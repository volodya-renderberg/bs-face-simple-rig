#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Описание содержимого файла hook.py

Данный файл содержит изменяемые процедуры сборки лицевого рига.

"""

import maya.cmds as cmds


def pre_bild(config):
    """
    Действия выполняемые до сборки рига.

    :param config:
    :return: None
    """
    pass


def post_bild(config):
    """Действия выполняемые после создания управления на бленды.
    
    :param config:
    :return: None
    """
    pass