#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Данный файл содержит конфигурацию сборки лицевого рига.
"""

CHARACTER="Frog_Girl"
"""str: имя персонажа"""

ALGORITHM = 'connect_bs_only'
"""str: название алгоритма сборки. """

BS_NAME = 'faceBlendShapes'
"""str: имя главной блендшейп ноды, которое может расширятся за счёт постфиксов."""

CTRL_ROOT_OB = 'FACE_CONTROLS'
"""str: имя рута объекта блендшейповых контролов."""

CTRL_FILE = 'face_controls_tmp.ma'
"""str: имя файла сконтролами, расположение в face_meta."""

DRIVING_OB = 'blendShapesDriverObject'
"""str: имя ``DRIVING`` ноды.

* **DRIVING** нода - пустая группа, содержащая атрибуты, одноимённые всем блендшейпам.
* Создаётся в :func:`bs_facial_rig.hook.pre_bild`
* Управление блендшейпами от контролов передаётся именно на атрибуты этой ноды.
* Блендшейпы сеток различных лодов, получают коннект от **Action** ноды.
"""

FACE_MATH_NODE = 'facial_rig_math_nodes'
"""str: Имя ассет ноды, в которую помещаются все ассеты содержащие математические ноды, 
Нода создаётся в :func:`bs_face_simple.hook.pre_bild`.
"""

MESHES = {
    "head_lod0_mesh": BS_NAME,
    # "head_geo_lod1": "%s_lod1" %  BS_NAME,
    "teeth_lod0_mesh": "%s_teeth" %  BS_NAME,
    # "eyebrow_geo": "%s_eyebrow" %  BS_NAME,
    # "lushes_geo": "%s_lushes" %  BS_NAME,
    # "moustoche_geo": "%s_moustoche" %  BS_NAME,
    # "eye_L_geo": "%s_l_eye" %  BS_NAME,
    # "eye_R_geo": "%s_r_eye" %  BS_NAME,
    # "eyeedge_geo_lod1": "%s_eyeedge" %  BS_NAME,
    # "eyelashes_geo_lod1": "%s_eyelashes" %  BS_NAME,
}
"""dict: сетки с блендами принимающими управление. ключ - имя сетки, значение - название бленда. """

EYE_GEO = {
    "l_eye": "eyeLeft_lod0_mesh",
    "r_eye": "eyeRight_lod0_mesh",
}
"""dict: сетки глазных яблок для автогенерации управления глаз в тестовых сборках для проверки блендов."""

JD = {
    "jawOpen": {
        "object": "jaw_ctrl",
        "attr": "ty",
        "value": -1,
    },
    "jawLeft": {
        "object": "jaw_ctrl",
        "attr": "tx",
        "value": 1,
    },
    "jawRight": {
        "object": "jaw_ctrl",
        "attr": "tx",
        "value": -1,
    },
    "jawForward": {
        "object": "jaw_ctrl",
        "attr": "tz",
        "value": 1,
    },
    "JawBack": {
        "object": "jaw_ctrl",
        "attr": "tz",
        "value": -1,
    },
}
"""
dict: параметры установки значений для контрола челюсти. 
"""

LOOK = {
    'down_r':{
        "object": "eye_R0_eye_jnt",
        "attr": "rx",
        "zero": '0.0',
        "value": '39.766',
    },
    'down_l':{
        "object": "eye_L0_eye_jnt",
        "attr": "rx",
        "zero": '0.0',
        "value": '39.766',
    },
    'up_r':{
        "object": "eye_R0_eye_jnt",
        "attr": "rx",
        "zero": '0.0',
        "value": '-29.674',
    },
    'up_l':{
        "object": "eye_L0_eye_jnt",
        "attr": "rx",
        "zero": '0.0',
        "value": '-29.511',
    },
    'in_r':{
        "object": "eye_R0_eye_jnt",
        "attr": "ry",
        "zero": '0.0',
        "value": '37.504',
    },
    'in_l':{
        "object": "eye_L0_eye_jnt",
        "attr": "ry",
        "zero": '0.0',
        "value": '-37.573',
    },
    'out_r':{
        "object": "eye_R0_eye_jnt",
        "attr": "ry",
        "zero": '0.0',
        "value": '-42.206',
    },
    'out_l':{
        "object": "eye_L0_eye_jnt",
        "attr": "ry",
        "zero": '0.0',
        "value": '42.1',
    },
}
"""
dict: параметры установки значений для авто век по направлению взгляда. 
"""

DRIVER_CONFIG = {
    # EYE PUPILS
    "eye_l_Pupil_Constrict": ("driver", ("eye_L_pupil_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "eye_r_Pupil_Constrict": ("driver", ("eye_R_pupil_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "eye_l_Pupil_Dilate": ("driver", ("eye_L_pupil_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "eye_r_Pupil_Dilate": ("driver", ("eye_R_pupil_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # CHEEK
    "CheekPuff": ("min", [
        ("cheekPuff_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("cheekPuff_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "CheekPuffLeft": ("side", {
        "this": ("cheekPuff_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        "another":("cheekPuff_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    }),
    "CheekPuffRight": ("side", {
        "this": ("cheekPuff_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        "another":("cheekPuff_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    }),
    "CheekSuck": ("min", [
        ("cheekPuff_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("cheekPuff_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    ]),
    "CheekSuckLeft": ("side", {
        "this": ("cheekPuff_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        "another":("cheekPuff_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    }),
    "CheekSuckRight": ("side", {
        "this": ("cheekPuff_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        "another":("cheekPuff_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    }),
    # "CheekSquintLeft": ("driver", ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # "CheekSquintRight": ("driver", ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # "EyeSquintLeft": ("driver", ("squint_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # "EyeSquintRight": ("driver", ("squint_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "CheekSquintLeft": ("multiply", [
        ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("squint_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("lid_L_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    "CheekSquintRight": ("multiply", [
        ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("squint_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("lid_R_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    "EyeSquintLeft": ("multiply", [
        ("cheek_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("squint_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lid_L_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    "EyeSquintRight": ("multiply", [
        ("cheek_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("squint_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lid_R_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    # 
    "CheekSquintSmileLeft_fix": ("multiply", [
        ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("smile_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "CheekSquintSmileRight_fix": ("multiply", [
        ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("smile_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "CheekSquintSharpPullLeft_fix": ("multiply", [
        ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("sharp_pull_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "CheekSquintSharpPullRight_fix": ("multiply", [
        ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("sharp_pull_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    ##
    # "EyeCheekSquintLeft_fix": ("multiply", [
    #     (DRIVING_OB, "EyeSquintLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintLeft", {"0.0": 0.0, "1.0": 1.0})
    # ]),
    # "EyeCheekSquintRight_fix": ("multiply", [
    #     (DRIVING_OB, "EyeSquintRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintRight", {"0.0": 0.0, "1.0": 1.0})
    # ]),
    "EyeCheekSquintLeft_u": ("multiply", [
        ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("squint_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lid_L_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    "EyeCheekSquintRight_u": ("multiply", [
        ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("squint_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lid_R_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    ##
    # "EyeBlinkCSquintLeft_fix": ("multiply", [
    #     (DRIVING_OB, "EyeBlinkLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeSquintLeft", {"0.0": 1.0, "1.0": 0.0}),
    # ]),
    # "EyeBlinkCSquintRight_fix": ("multiply", [
    #     (DRIVING_OB, "EyeBlinkRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeSquintRight", {"0.0": 1.0, "1.0": 0.0}),
    # ]),
    "EyeBlinkCSquintLeft_u": ("multiply", [
        ("lid_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("squint_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0})
    ]),
    "EyeBlinkCSquintRight_u": ("multiply", [
        ("lid_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("squint_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0})
    ]),
    ##
    # "EyeBlinkESquintLeft_fix": ("multiply", [
    #     (DRIVING_OB, "EyeBlinkLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeSquintLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintLeft", {"0.0": 1.0, "1.0": 0.0})
    # ]),
    # "EyeBlinkESquintRight_fix": ("multiply", [
    #     (DRIVING_OB, "EyeBlinkRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeSquintRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintRight", {"0.0": 1.0, "1.0": 0.0})
    # ]),
    "EyeBlinkESquintLeft_u": ("multiply", [
        ("lid_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("squint_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("cheek_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "EyeBlinkESquintRight_u": ("multiply", [
        ("lid_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("squint_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("cheek_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    ##
    # "EyeBlinkFSquintLeft_fix": ("multiply", [
    #     (DRIVING_OB, "EyeBlinkLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeSquintLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintLeft", {"0.0": 0.0, "1.0": 1.0})
    # ]),
    # "EyeBlinkFSquintRight_fix": ("multiply", [
    #     (DRIVING_OB, "EyeBlinkRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeSquintRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "CheekSquintRight", {"0.0": 0.0, "1.0": 1.0})
    # ]),
    "EyeBlinkFSquintLeft_u": ("multiply", [
        ("lid_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("squint_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "EyeBlinkFSquintRight_u": ("multiply", [
        ("lid_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("squint_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "EyeBlinkSquinLeft_fix": ("expression", {
    #     "strings": [
    #         'float $rez;',
    #         'float $driver;',
    #         'float $blink=lid_L_ctrl.ty;',
    #         'float $sguint=squint_L_ctrl.ty;',
    #         'float $cheek=cheek_L_ctrl.ty;',
    #         'if ($sguint > $cheek) $driver=$sguint;',
    #         'else $driver=$cheek;',
    #         'if ($blink>0) $rez=0;'
    #         'else $rez=$driver * $blink;',
    #         'current_target = $rez * -1;'
    #     ]
    # }),
    # "EyeBlinkSquinRight_fix": ("expression", {
    #     "strings": [
    #         'float $rez;',
    #         'float $driver;',
    #         'float $blink=lid_R_ctrl.ty;',
    #         'float $sguint=squint_R_ctrl.ty;',
    #         'float $cheek=cheek_R_ctrl.ty;',
    #         'if ($sguint > $cheek) $driver=$sguint;',
    #         'else $driver=$cheek;',
    #         'if ($blink>0) $rez=0;'
    #         'else $rez=$driver * $blink;',
    #         'current_target = $rez * -1;'
    #     ]
    # }),
    "EyeWideLeft": ("driver", ("lid_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "EyeWideRight": ("driver", ("lid_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # "EyeBlinkLeft": ("driver", ("lid_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    # "EyeBlinkRight": ("driver", ("lid_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    # "EyeCompressLeft_fix": ("multiply", [
    #     # (DRIVING_OB, "EyeBlinkLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     ("lid_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    #     # (DRIVING_OB, "EyeSquintLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     ("squint_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    #     # (DRIVING_OB, "CheekSquintLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     ("cheek_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    #     # (DRIVING_OB, "BrowDownLeftInner", {"0.0": 0.0, "1.0": 1.0}),
    #     ("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    #     # (DRIVING_OB, "BrowDownLeftOuter", {"0.0": 0.0, "1.0": 1.0}),
    #     ("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    #     # (DRIVING_OB, "BrowLateralLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     # (DRIVING_OB, "NoseSneerLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     ("nose_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    # ]),
    # "EyeCompressRight_fix": ("multiply", [
    #     # (DRIVING_OB, "EyeBlinkRight", {"0.0": 0.0, "1.0": 1.0}),
    #     ("lid_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    #     # (DRIVING_OB, "EyeSquintRight", {"0.0": 0.0, "1.0": 1.0}),
    #     ("squint_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    #     # (DRIVING_OB, "CheekSquintRight", {"0.0": 0.0, "1.0": 1.0}),
    #     ("cheek_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    #     # (DRIVING_OB, "BrowDownRightInner", {"0.0": 0.0, "1.0": 1.0}),
    #     ("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    #     # (DRIVING_OB, "BrowDownRightOuter", {"0.0": 0.0, "1.0": 1.0}),
    #     ("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    #     # (DRIVING_OB, "BrowLateralRight", {"0.0": 0.0, "1.0": 1.0}),
    #     # (DRIVING_OB, "NoseSneerRight", {"0.0": 0.0, "1.0": 1.0}),
    #     ("nose_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    # ]),
    # "EyeCompress_fix": ("min", [
    #     (DRIVING_OB, "EyeCompressLeft_fix", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "EyeCompressRight_fix", {"0.0": 0.0, "1.0": 1.0}),
    # ]),
    "EyeBlinkLeft": ("multiply", [
        ("lid_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("squint_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("cheek_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "EyeBlinkRight": ("multiply", [
        ("lid_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("squint_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("cheek_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "EyeBlinkLeft_fix": ("multiply", [
        # (DRIVING_OB, "EyeBlinkLeft", {"0.0": 0.0, "0.5": 1.0, "1.0": 0.0}),
        ("lid_L_ctrl", "ty", {"0.0": 0.0, "-0.5": 1.0, "-1.0": 0.0}),
        (DRIVING_OB, "EyeLookDownLeft", {"0.0": 1.0, "1.0": 0.0}),
        # (DRIVING_OB, "EyeLookUpLeft", {"0.0": 1.0, "1.0": 0.0}),
        ]),
    "EyeBlinkRight_fix": ("multiply", [
        # (DRIVING_OB, "EyeBlinkRight", {"0.0": 0.0, "0.5": 1.0, "1.0": 0.0}),
        ("lid_R_ctrl", "ty", {"0.0": 0.0, "-0.5": 1.0, "-1.0": 0.0}),
        (DRIVING_OB, "EyeLookDownRight", {"0.0": 1.0, "1.0": 0.0}),
        # (DRIVING_OB, "EyeLookUpRight", {"0.0": 1.0, "1.0": 0.0}),
        ]),
    "EyeAutolidL_driver": ("multiply",[
        ("lid_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("lid_L_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    "EyeAutolidR_driver": ("multiply",[
        ("lid_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("lid_R_ctrl", "ty", {"0.0": 1.0, "-1.0": 0.0}),
    ]),
    "EyeLookDownLeft": ("multiply", [
        (DRIVING_OB, "EyeAutolidL_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['down_l']["object"], LOOK['down_l']["attr"], { LOOK['down_l']["zero"]: 0.0,  LOOK['down_l']["value"]: 1.0}),
    ]),
    "EyeLookDownRight": ("multiply", [
        (DRIVING_OB, "EyeAutolidR_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['down_r']["object"], LOOK['down_r']["attr"], { LOOK['down_r']["zero"]: 0.0,  LOOK['down_r']["value"]: 1.0}),
    ]),
    "EyeLookUpLeft": ("multiply", [
        (DRIVING_OB, "EyeAutolidL_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['up_l']["object"], LOOK['up_l']["attr"], { LOOK['up_l']["zero"]: 0.0,  LOOK['up_l']["value"]: 1.0}),
    ]),
    "EyeLookUpRight": ("multiply", [
        (DRIVING_OB, "EyeAutolidR_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['up_r']["object"], LOOK['up_r']["attr"], { LOOK['up_r']["zero"]: 0.0,  LOOK['up_r']["value"]: 1.0}),
    ]),
    "EyeLookInLeft": ("multiply", [
        (DRIVING_OB, "EyeAutolidL_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['in_l']["object"], LOOK['in_l']["attr"], { LOOK['in_l']["zero"]: 0.0,  LOOK['in_l']["value"]: 1.0}),
    ]),
    "EyeLookInRight": ("multiply", [
        (DRIVING_OB, "EyeAutolidR_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['in_r']["object"], LOOK['in_r']["attr"], { LOOK['in_r']["zero"]: 0.0,  LOOK['in_r']["value"]: 1.0}),
    ]),
    "EyeLookOutLeft": ("multiply", [
        (DRIVING_OB, "EyeAutolidL_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['out_l']["object"], LOOK['out_l']["attr"], { LOOK['out_l']["zero"]: 0.0,  LOOK['out_l']["value"]: 1.0}),
    ]),
    "EyeLookOutRight": ("multiply", [
        (DRIVING_OB, "EyeAutolidR_driver", {"0.0": 0.0, "1.0": 1.0}),
        (LOOK['out_r']["object"], LOOK['out_r']["attr"], { LOOK['out_r']["zero"]: 0.0,  LOOK['out_r']["value"]: 1.0}),
    ]),
    # BROW
    # -- BROW DOWN
    "BrowDownLeft": ("min", [
        ("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    ]),
    "BrowDownRight": ("min", [
        ("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        ("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    ]),
    "BrowDownLeftOuter": ("side", {
        "this": ("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        "another":("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    }),
    "BrowDownRightOuter": ("side", {
        "this": ("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        "another":("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    }),
    "BrowDownLeftInner": ("side", {
        "this": ("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        "another":("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    }),
    "BrowDownRightInner": ("side", {
        "this": ("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
        "another":("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    }),
    # -- BROW UP
    "BrowUpLeft": ("min", [
        ("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "BrowUpRight": ("min", [
        ("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "BrowUpLeftOuter": ("side", {
        "this": ("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        "another":("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    }),
    "BrowUpRightOuter": ("side", {
        "this": ("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        "another":("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    }),
    "BrowUpLeftInner": ("side", {
        "this": ("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        "another":("brow_outer_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    }),
    "BrowUpRightInner": ("side", {
        "this": ("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        "another":("brow_outer_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    }),
    # -- BROW Lateral
    "BrowLateralLeft": ("driver", ("brow_lateral_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "BrowLateralRight": ("driver", ("brow_lateral_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # -- BROW FIX
    "BrowDownLateralLeft_fix": ("multiply", [
        (DRIVING_OB, "BrowLateralLeft", {"0.0": 0.0, "1.0": 1.0}),
        ("brow_inner_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    ]),
    "BrowDownLateralRight_fix": ("multiply", [
        (DRIVING_OB, "BrowLateralRight", {"0.0": 0.0, "1.0": 1.0}),
        ("brow_inner_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0}),
    ]),
    # JAW
    "JawForward": ("driver", (JD["jawForward"]["object"], JD["jawForward"]["attr"], {"0.0": 0.0, "%s" % JD["jawForward"]["value"] : 1.0})),
    "JawBack": ("driver", (JD["JawBack"]["object"], JD["JawBack"]["attr"], {"0.0": 0.0, "%s" % JD["JawBack"]["value"] : 1.0})),
    "JawOpen": ("driver", (JD["jawOpen"]["object"], JD["jawOpen"]["attr"], {"0.0": 0.0, "%s" % JD["jawOpen"]["value"] : 1.0})),
    "JawLeft": ("driver", (JD["jawLeft"]["object"], JD["jawLeft"]["attr"], {"0.0": 0.0, "%s" % JD["jawLeft"]["value"] : 1.0})),
    "JawRight": ("driver", (JD["jawRight"]["object"], JD["jawRight"]["attr"], {"0.0": 0.0, "%s" % JD["jawRight"]["value"] : 1.0})),
    "JawOpenFunnel_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFunnel", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenPucker_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        (DRIVING_OB, "MouthFunnel", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenPuckerFunnel_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFunnel", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenMouthCloseFunnel_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFunnel", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenMouthClosePucker_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFunnel", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenMouthClosePuckerFunnel_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFunnel", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthLeft": ("driver", ("mouth_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "MouthRight": ("driver", ("mouth_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthUp": ("driver", ("mouth_ctrl", "tx", {"0.0": 0.0, "1.0": 1.0})),
    "MouthDown": ("driver", ("mouth_ctrl", "tx", {"0.0": 0.0, "-1.0": 1.0})),
    "MouthClose_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        # (DRIVING_OB, "JawLeft", {"0.0": 1.0, "1.0": 0.0}),
        # (DRIVING_OB, "JawRight", {"0.0": 1.0, "1.0": 0.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthCloseJawOpenLeft_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawLeft", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthCloseJawOpenRight_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawRight", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthCloseJawLeft_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 1.0, "1.0": 0.0}),
        (DRIVING_OB, "JawLeft", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthCloseJawRight_fix": ("multiply",[
        (DRIVING_OB, "JawOpen", {"0.0": 1.0, "1.0": 0.0}),
        (DRIVING_OB, "JawRight", {"0.0": 0.0, "1.0": 1.0}),
        ("mouth_close_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLeftPress_fix": ("multiply",[
    #     (DRIVING_OB, "MouthLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "LipPressLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "LipPressRight", {"0.0": 0.0, "1.0": 1.0}),
    # ]),
    # "MouthRightPress_fix": ("multiply",[
    #     (DRIVING_OB, "MouthRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "LipPressLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "LipPressRight", {"0.0": 0.0, "1.0": 1.0}),
    # ]),
    "MouthLeftPucker_fix": ("multiply",[
        (DRIVING_OB, "MouthLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthRightPucker_fix": ("multiply",[
        (DRIVING_OB, "MouthRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthOClosedUpper": ("multiply",[
    #     ("mouthFunnel_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    #     ("mouth_upper_O_close_ctrl", "ty", {"0.0": 0.0, "1.0": 0.5}),
    # ]),
    # "MouthOClosedLower": ("multiply",[
    #     ("mouthFunnel_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    #     ("mouth_lower_O_close_ctrl", "ty", {"0.0": 0.0, "1.0": 0.5}),
    # ]),
    "MouthDimpleLeft": ("multiply",[
        ("dimple_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("smile_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("stretch_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0})
    ]),
    "MouthDimpleRight": ("multiply",[
        ("dimple_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("smile_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
        ("stretch_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0})
    ]),
    "MouthSmileLeft": ("driver",
        ("smile_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_L_ctrl", "tx", {"0.0": 0.0, "1.0": 1.0}),
    ),
    "MouthSmileRight": ("driver",
        ("smile_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_R_ctrl", "tx", {"0.0": 0.0, "-1.0": 1.0}),
    ),
    "MouthSharpPullLeft": ("driver",
        ("sharp_pull_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_L_ctrl", "tx", {"0.0": 0.0, "1.0": 1.0}),
    ),
    "MouthSharpPullRight": ("driver",
        ("sharp_pull_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_R_ctrl", "tx", {"0.0": 0.0, "-1.0": 1.0}),
    ),
    "MouthStretchLeft": ("driver",
        ("stretch_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_L_ctrl", "tx", {"0.0": 0.0, "1.0": 1.0}),
    ),
    "MouthStretchRight": ("driver",
        ("stretch_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_R_ctrl", "tx", {"0.0": 0.0, "-1.0": 1.0}),
    ),
    "MouthFrownLeft": ("driver",
        ("frown_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_L_ctrl", "tx", {"0.0": 1.0, "1.0": 0.0}),
    ),
    "MouthFrownRight": ("driver",
        ("frown_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
        # ("lips_R_ctrl", "tx", {"0.0": 1.0, "-1.0": 0.0}),
    ),
    # "MouthPress": ("driver", ("mouthPress_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthPressLeft": ("driver", ("mouthPress_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthPressRight": ("driver", ("mouthPress_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "LipPressLeft": ("driver", ("lipPress_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "LipPressRight": ("driver", ("lipPress_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "LipCornerSharpenULeft":["driver", ("lip_upper_corner_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})],
    "LipCornerSharpenURight":["driver", ("lip_upper_corner_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})],
    "LipCornerSharpenDLeft":["driver", ("lip_lower_corner_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})],
    "LipCornerSharpenDRight":["driver", ("lip_lower_corner_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})],
    # chin raise
    "MouthUpperUpLeft": ("multiply", [
        ("lip_upper_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lip_chin_raise_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "MouthUpperUpRight": ("multiply", [
        ("lip_upper_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lip_chin_raise_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "ChinRaiseLeft": ("driver",
        ("lip_chin_raise_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
    ),
    "ChinRaiseRight": ("driver",
        ("lip_chin_raise_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
    ),
    "ChinCompressLeft": ("driver",
        ("chin_compress_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
    ),
    "ChinCompressRight": ("driver",
        ("chin_compress_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})
    ),
    #
    "MouthLowerDownLeft": ("multiply", [
        ("lip_lower_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lip_chin_raise_L_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "MouthLowerDownRight": ("multiply", [
        ("lip_lower_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
        ("lip_chin_raise_R_ctrl", "ty", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    # "JawOpenSmileLeft_fix":("JawOpenSmileLeft_u", "JawOpen", "MouthSmileLeft"),
    "JawOpenSmileLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenSmileRight_fix":("JawOpenSmileRight_u", "JawOpen", "MouthSmileRight"),
    "JawOpenSmileRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenSharpPullLeft_fix":("JawOpenSharpPullLeft_u", "JawOpen", "MouthSharpPullLeft"),
    "JawOpenSharpPullLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenSharpPullRight_fix":("JawOpenSharpPullRight_u", "JawOpen", "MouthSharpPullRight"),
    "JawOpenSharpPullRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenDimpleLeft_fix":("JawOpenDimpleLeft_u", "JawOpen", "MouthDimpleLeft"),
    "JawOpenDimpleLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthDimpleLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenDimpleRight_fix":("JawOpenDimpleRight_u", "JawOpen", "MouthDimpleRight"),
    "JawOpenDimpleRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthDimpleRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenStretchLeft_fix":("JawOpenStretchLeft_u", "JawOpen", "MouthStretchLeft"),
    "JawOpenStretchLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthStretchLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenStretchRight_fix":("JawOpenStretchRight_u", "JawOpen", "MouthStretchRight"),
    "JawOpenStretchRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthStretchRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenFrownLeft_fix":("JawOpenFrownLeft_u", "JawOpen", "MouthFrownLeft"),
    "JawOpenFrownLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFrownLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "JawOpenFrownRight_fix":("JawOpenFrownRight_u", "JawOpen", "MouthFrownRight"),
    "JawOpenFrownRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFrownRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # Open Smile LowerDown
    "JawOpenMouthLowerDownLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthLowerDownLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenMouthLowerDownRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthLowerDownRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    "JawOpenSmileMLowerDownLeft_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthLowerDownLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "JawOpenSmileMLowerDownRight_fix": ("multiply", [
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthLowerDownRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "LipsTowards": ("driver", ("lips_towards_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthFunnel": ("driver", ("mouthFunnel_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthPucker": ("driver", ("mouthPucker_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthPuckerFunnel_fix": ("multiply", [
        (DRIVING_OB, "MouthFunnel", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawOpen", {"0.0": 1.0, "1.0": 0.0}),
    ]),
    # "MouthPuckerTowards_fix":("MouthPuckerTowards_u", "LipsTowards", "MouthPucker"),
    "MouthPuckerTowards_fix": ("multiply", [
        (DRIVING_OB, "LipsTowards", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthPucker", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthShrugUpperLeft": ("driver", ("lip_upper_roll_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthShrugUpperRight": ("driver", ("lip_upper_roll_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthShrugLowerLeft": ("driver", ("lip_lower_roll_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "MouthShrugLowerRight": ("driver", ("lip_lower_roll_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    #
    "MouthRollUpperLeft": ("driver", ("lip_upper_roll_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "MouthRollUpperRight": ("driver", ("lip_upper_roll_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "MouthRollLowerLeft": ("driver", ("lip_lower_roll_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "MouthRollLowerRight": ("driver", ("lip_lower_roll_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # "MouthRollJawOpen_fix": ("multiply", [
    #     (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "MouthRollUpperLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "MouthRollUpperRight", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "MouthRollLowerLeft", {"0.0": 0.0, "1.0": 1.0}),
    #     (DRIVING_OB, "MouthRollLowerRight", {"0.0": 0.0, "1.0": 1.0}),
    # ]),
    #
    "MouthRollUpperLeftJawOpen_fix": ("multiply", [
        (DRIVING_OB, "MouthRollUpperLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthRollUpperRightJawOpen_fix": ("multiply", [
        (DRIVING_OB, "MouthRollUpperRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthRollLowerLeftJawOpen_fix": ("multiply", [
        (DRIVING_OB, "MouthRollLowerLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "MouthRollLowerRightJawOpen_fix": ("multiply", [
        (DRIVING_OB, "MouthRollLowerRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "JawOpen", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # 
    "SmileSneerLeft_fix": ("multiply", [
        (DRIVING_OB, "NoseSneerLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "SmileSneerRight_fix": ("multiply", [
        (DRIVING_OB, "NoseSneerRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "SharpPullSneerLeft_fix": ("multiply", [
        (DRIVING_OB, "NoseSneerLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    "SharpPullSneerRight_fix": ("multiply", [
        (DRIVING_OB, "NoseSneerRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # NOSE
    "NoseSneerLeft": ("driver",
        ("nose_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ),
    "NoseSneerRight": ("driver", 
        ("nose_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0}),
    ),
    "NoseDepressLeft": ("driver", ("nose_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "NoseDepressRight": ("driver", ("nose_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "NostrilDilateLeft": ("driver", ("nostril_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "NostrilDilateRight": ("driver", ("nostril_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "NostrilCompressLeft": ("driver", ("nostril_L_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "NostrilCompressRight": ("driver", ("nostril_R_ctrl", "ty", {"0.0": 0.0, "-1.0": 1.0})),
    "NasolabialLeft": ("driver", ("nasolabial_L_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    "NasolabialRight": ("driver", ("nasolabial_R_ctrl", "ty", {"0.0": 0.0, "1.0": 1.0})),
    # "NasolabialSmileLeft_fix":("NasolabialSmileLeft_u", "NasolabialLeft", "MouthSmileLeft"),
    "NasolabialSmileLeft_fix": ("multiply", [
        (DRIVING_OB, "NasolabialLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialSmileRight_fix":("NasolabialSmileRight_u", "NasolabialRight", "MouthSmileRight"),
    "NasolabialSmileRight_fix": ("multiply", [
        (DRIVING_OB, "NasolabialRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialSharpPullLeft_fix":("NasolabialSharpPullLeft_u", "NasolabialLeft", "MouthSharpPullLeft"),
    "NasolabialSharpPullLeft_fix": ("multiply", [
        (DRIVING_OB, "NasolabialLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialSharpPullRight_fix":("NasolabialSharpPullRight_u", "NasolabialRight", "MouthSharpPullRight"),
    "NasolabialSharpPullRight_fix": ("multiply", [
        (DRIVING_OB, "NasolabialRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialDimpleLeft_fix":("NasolabialDimpleLeft_u", "NasolabialLeft", "MouthDimpleLeft"),
    "NasolabialDimpleLeft_fix": ("multiply", [
        (DRIVING_OB, "NasolabialLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthDimpleLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialDimpleRight_fix":("NasolabialDimpleRight_u", "NasolabialRight", "MouthDimpleRight"),
    "NasolabialDimpleRight_fix": ("multiply", [
        (DRIVING_OB, "NasolabialRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthDimpleRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialStretchLeft_fix":("NasolabialStretchLeft_u", "NasolabialLeft", "MouthStretchLeft"),
    "NasolabialStretchLeft_fix": ("multiply", [
        (DRIVING_OB, "NasolabialLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthStretchLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialStretchRight_fix":("NasolabialStretchRight_u", "NasolabialRight", "MouthStretchRight"),
    "NasolabialStretchRight_fix": ("multiply", [
        (DRIVING_OB, "NasolabialRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthStretchRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialFrownLeft_fix":("NasolabialFrownLeft_u", "NasolabialLeft", "MouthFrownLeft"),
    "NasolabialFrownLeft_fix": ("multiply", [
        (DRIVING_OB, "NasolabialLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFrownLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "NasolabialFrownRight_fix":("NasolabialFrownRight_u", "NasolabialRight", "MouthFrownRight"),
    "NasolabialFrownRight_fix": ("multiply", [
        (DRIVING_OB, "NasolabialRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthFrownRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthUpperUpSneerLeft_fix":("MouthUpperUpSneerLeft_u", "MouthUpperUpLeft", "NoseSneerLeft"),
    "MouthUpperUpSneerLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthUpperUpLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "NoseSneerLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthUpperUpSneerRight_fix":("MouthUpperUpSneerRight_u", "MouthUpperUpRight", "NoseSneerRight"),
    "MouthUpperUpSneerRight_fix": ("multiply", [
        (DRIVING_OB, "MouthUpperUpRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "NoseSneerRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthUpperUpSharpPullLeft_fix":("MouthUpperUpSharpPullLeft_u", "MouthUpperUpLeft", "MouthSharpPullLeft"),
    "MouthUpperUpSharpPullLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthUpperUpLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthUpperUpSharpPullRight_fix":("MouthUpperUpSharpPullRight_u", "MouthUpperUpRight", "MouthSharpPullRight"),
    "MouthUpperUpSharpPullRight_fix": ("multiply", [
        (DRIVING_OB, "MouthUpperUpRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLowerDownSmileLeft_fix":("MouthLowerDownSmileLeft_u", "MouthLowerDownLeft", "MouthSmileLeft"),
    "MouthLowerDownSmileLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthLowerDownLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLowerDownSmileRight_fix":("MouthLowerDownSmileRight_u", "MouthLowerDownRight", "MouthSmileRight"),
    "MouthLowerDownSmileRight_fix": ("multiply", [
        (DRIVING_OB, "MouthLowerDownRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLowerDownSharpPullLeft_fix":("MouthLowerDownSharpPullLeft_u", "MouthLowerDownLeft", "MouthSharpPullLeft"),
    "MouthLowerDownSharpPullLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthLowerDownLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLowerDownSharpPullRight_fix":("MouthLowerDownSharpPullRight_u", "MouthLowerDownRight", "MouthSharpPullRight"),
    "MouthLowerDownSharpPullRight_fix": ("multiply", [
        (DRIVING_OB, "MouthLowerDownRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthStretchSmileLeft_fix":("MouthStretchSmileLeft_u", "MouthStretchLeft", "MouthSmileLeft"),
    "MouthStretchSmileLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthStretchLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthStretchSmileRight_fix":("MouthStretchSmileRight_u", "MouthStretchRight", "MouthSmileRight"),
    "MouthStretchSmileRight_fix": ("multiply", [
        (DRIVING_OB, "MouthStretchRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthStretchSharpPullLeft_fix":("MouthStretchSharpPullLeft_u", "MouthStretchLeft", "MouthSharpPullLeft"),
    "MouthStretchSharpPullLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthStretchLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthStretchSharpPullRight_fix":("MouthStretchSharpPullRight_u", "MouthStretchRight", "MouthSharpPullRight"),
    "MouthStretchSharpPullRight_fix": ("multiply", [
        (DRIVING_OB, "MouthStretchRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSharpPullRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLowerDownStretchLeft_fix":("MouthLowerDownStretchLeft_u", "MouthStretchLeft", "MouthLowerDownLeft"),
    "MouthLowerDownStretchLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthStretchLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthLowerDownLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthLowerDownStretchRight_fix":("MouthLowerDownStretchRight_u", "MouthStretchRight", "MouthLowerDownRight"),
    "MouthLowerDownStretchRight_fix": ("multiply", [
        (DRIVING_OB, "MouthStretchRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthLowerDownRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthUpperUpSmileLeft_fix":("MouthUpperUpSmileLeft_u", "MouthUpperUpLeft", "MouthSmileLeft"),
    "MouthUpperUpSmileLeft_fix": ("multiply", [
        (DRIVING_OB, "MouthUpperUpLeft", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileLeft", {"0.0": 0.0, "1.0": 1.0}),
    ]),
    # "MouthUpperUpSmileRight_fix":("MouthUpperUpSmileRight_u", "MouthUpperUpRight", "MouthSmileRight"),
    "MouthUpperUpSmileRight_fix": ("multiply", [
        (DRIVING_OB, "MouthUpperUpRight", {"0.0": 0.0, "1.0": 1.0}),
        (DRIVING_OB, "MouthSmileRight", {"0.0": 0.0, "1.0": 1.0}),
    ]),
}
"""dict: словарь конфигурации логики управления блендами мимики на объект :attr:`DRIVING_OB`.

* Управление назначается на одноимённые названиям блендшейпов атрибуты объекта :attr:`DRIVING_OB`
* Уже от этих атрибутов делается коннект на блендшейпы всех лодов и сеток.
* :ref:`coding-page`
"""

FACIAL_MOCAP_RIG_CTRLS = [
    "brow_inner_L_ctrl",
    "brow_inner_R_ctrl",
    "brow_lateral_L_ctrl",
    "brow_lateral_R_ctrl",
    "brow_outer_L_ctrl",
    "brow_outer_R_ctrl",
    "cheekPuff_L_ctrl",
    "cheekPuff_R_ctrl",
    "cheek_L_ctrl",
    "cheek_R_ctrl",
    "chin_compress_L_ctrl",
    "chin_compress_R_ctrl",
    "dimple_L_ctrl",
    "dimple_R_ctrl",
    "frown_L_ctrl",
    "frown_R_ctrl",
    "jaw_ctrl",
    "lid_L_ctrl",
    "lid_R_ctrl",
    "lipPress_L_ctrl",
    "lipPress_R_ctrl",
    "lip_chin_raise_L_ctrl",
    "lip_chin_raise_R_ctrl",
    "lip_lower_L_ctrl",
    "lip_lower_R_ctrl",
    "lip_lower_roll_L_ctrl",
    "lip_lower_roll_R_ctrl",
    "lip_upper_L_ctrl",
    "lip_upper_R_ctrl",
    "lip_upper_roll_L_ctrl",
    "lip_upper_roll_R_ctrl",
    "lips_towards_ctrl",
    "mouthFunnel_ctrl",
    "mouthPress_L_ctrl",
    "mouthPress_R_ctrl",
    "mouthPucker_ctrl",
    "mouth_close_ctrl",
    "mouth_ctrl",
    "nasolabial_L_ctrl",
    "nasolabial_R_ctrl",
    "nose_L_ctrl",
    "nose_R_ctrl",
    "nostril_L_ctrl",
    "nostril_R_ctrl",
    "sharp_pull_L_ctrl",
    "sharp_pull_R_ctrl",
    "smile_L_ctrl",
    "smile_R_ctrl",
    "squint_L_ctrl",
    "squint_R_ctrl",
    "stretch_L_ctrl",
    "stretch_R_ctrl",
    #
    'eye_R0_ik_ctl',
    'eye_L0_ik_ctl',
    'eyeslook_C0_ctl',
    'jawUp_C0_ctl',
    'jawDw_C0_ctl',
    #
    'lip_upper_corner_L_ctrl',
    'lip_upper_corner_R_ctrl',
    'lip_lower_corner_L_ctrl',
    'lip_lower_corner_R_ctrl',
]
"""list: перечень лицевых контролов, которые должны добавлятся в сет :attr:`Tentaculo_processors.settings.FACIAL_MOCAP_RIG_CTRLS_SET` """

LIVE_LINK_CODES={
    "brow_lateral_L_ctrl|ty|1.0": "BrowLateralLeft", # - нет кода livelink
    "brow_lateral_R_ctrl|ty|1.0": "BrowLateralRight", # - нет кода livelink
    "brow_outer_L_ctrl|ty|-1.0": "BrowDownLeftOuter", # BrowDownLeft
    "brow_inner_L_ctrl|ty|-1.0": "BrowDownLeftInner", # BrowDownLeft
    "brow_outer_R_ctrl|ty|-1.0": "BrowDownRightOuter", # BrowDownRight
    "brow_inner_R_ctrl|ty|-1.0": "BrowDownRightInner", # BrowDownRight
    "brow_inner_L_ctrl|ty|1.0": "BrowUpLeftInner", # BrowInnerUp
    "brow_inner_R_ctrl|ty|1.0": "BrowUpRightInner", # BrowInnerUp
    "brow_outer_L_ctrl|ty|1.0": "BrowUpLeftOuter", # BrowOuterUpLeft
    "brow_outer_R_ctrl|ty|1.0": "BrowUpRightOuter", # BrowOuterUpRight
    "cheekPuff_L_ctrl|ty|1.0": "CheekPuff",
    "cheekPuff_R_ctrl|ty|1.0": "CheekPuff",
    "cheek_L_ctrl|ty|1.0": "CheekSquintLeft",
    "cheek_R_ctrl|ty|1.0": "CheekSquintRight",
    "lid_L_ctrl|ty|-1.0": "EyeBlinkLeft",
    "lid_R_ctrl|ty|-1.0": "EyeBlinkRight",
    "%s|%s|%s" % (LOOK['down_l']["object"], LOOK['down_l']["attr"], LOOK['down_l']["value"]): "EyeLookDownLeft",
    "%s|%s|%s" % (LOOK['down_r']["object"], LOOK['down_r']["attr"], LOOK['down_r']["value"]): "EyeLookDownRight",
    "%s|%s|%s" % (LOOK['in_l']["object"], LOOK['in_l']["attr"], LOOK['in_l']["value"]): "EyeLookInLeft",
    "%s|%s|%s" % (LOOK['in_r']["object"], LOOK['in_r']["attr"], LOOK['in_r']["value"]): "EyeLookInRight",
    "%s|%s|%s" % (LOOK['out_l']["object"], LOOK['out_l']["attr"], LOOK['out_l']["value"]): "EyeLookOutLeft",
    "%s|%s|%s" % (LOOK['out_r']["object"], LOOK['out_r']["attr"], LOOK['out_r']["value"]): "EyeLookOutRight",
    "%s|%s|%s" % (LOOK['up_l']["object"], LOOK['up_l']["attr"], LOOK['up_l']["value"]): "EyeLookUpLeft",
    "%s|%s|%s" % (LOOK['up_r']["object"], LOOK['up_r']["attr"], LOOK['up_r']["value"]): "EyeLookUpRight",
    "squint_L_ctrl|ty|1.0": "EyeSquintLeft",
    "squint_R_ctrl|ty|1.0": "EyeSquintRight",
    "lid_L_ctrl|ty|1.0": "EyeWideLeft",
    "lid_R_ctrl|ty|1.0": "EyeWideRight",
    # "": "HeadPitch",
    # "": "HeadRoll",
    # "": "HeadYaw",
    "%s|%s|%s" % (JD["jawForward"]["object"], JD["jawForward"]["attr"], JD["jawForward"]["value"]): "JawForward",
    "%s|%s|%s" % (JD["jawLeft"]["object"], JD["jawLeft"]["attr"], JD["jawLeft"]["value"]): "JawLeft",
    "%s|%s|%s" % (JD["jawOpen"]["object"], JD["jawOpen"]["attr"], JD["jawOpen"]["value"]): "JawOpen",
    "%s|%s|%s" % (JD["jawRight"]["object"], JD["jawRight"]["attr"], JD["jawRight"]["value"]): "JawRight",
    # "": "LeftEyePitch",
    # "": "LeftEyeRoll",
    # "": "LeftEyeYaw",
    "mouth_close_ctrl|ty|1.0": "MouthClose_fix",
    "dimple_L_ctrl|ty|1.0": "MouthDimpleLeft",
    "dimple_R_ctrl|ty|1.0": "MouthDimpleRight",
    "frown_L_ctrl|ty|1.0": "MouthFrownLeft",
    "frown_R_ctrl|ty|1.0": "MouthFrownRight",
    "mouthFunnel_ctrl|ty|1.0": "MouthFunnel",
    "mouth_ctrl|ty|-1.0": "MouthLeft",
    "lip_lower_L_ctrl|ty|1.0": "MouthLowerDownLeft",
    "lip_lower_R_ctrl|ty|1.0": "MouthLowerDownRight",
    "mouthPress_L_ctrl|ty|1.0": "MouthPressLeft",
    "mouthPress_R_ctrl|ty|1.0": "MouthPressRight",
    "mouthPucker_ctrl|ty|1.0": "MouthPucker",
    "mouth_ctrl|ty|1.0": "MouthRight",
    "lip_lower_roll_L_ctrl|ty|1.0": "MouthRollLower",
    "lip_lower_roll_R_ctrl|ty|1.0": "MouthRollLower",
    "lip_upper_roll_L_ctrl|ty|-1.0": "MouthRollUpper",
    "lip_upper_roll_R_ctrl|ty|-1.0": "MouthRollUpper",
    "lip_lower_roll_L_ctrl|ty|-1.0": "MouthShrugLower",
    "lip_lower_roll_R_ctrl|ty|-1.0": "MouthShrugLower",
    "lip_upper_roll_L_ctrl|ty|1.0": "MouthShrugUpper",
    "lip_upper_roll_R_ctrl|ty|1.0": "MouthShrugUpper",
    "smile_L_ctrl|ty|1.0": "MouthSmileLeft",
    "smile_R_ctrl|ty|1.0": "MouthSmileRight",
    "stretch_L_ctrl|ty|1.0": "MouthStretchLeft",
    "stretch_R_ctrl|ty|1.0": "MouthStretchRight",
    "lip_upper_L_ctrl|ty|1.0": "MouthUpperUpLeft",
    "lip_upper_R_ctrl|ty|1.0": "MouthUpperUpRight",
    "nose_L_ctrl|ty|1.0": "NoseSneerLeft",
    "nose_R_ctrl|ty|1.0": "NoseSneerRight",
    "sharp_pull_L_ctrl|ty|1.0": "MouthSharpPullLeft",
    "sharp_pull_R_ctrl|ty|1.0": "MouthSharpPullRight",
    # "": "RightEyePitch",
    # "": "RightEyeRoll",
    # "": "RightEyeYaw",
    # "": "TongueOut",
}
"""dict: связь контролов с позами лойв линка. структура ключа: 'control_name|attr_name|value', значение - код лайвлинка, который используется вместо данного контрола."""

MATCHING_LIVE_LINK_CODES={
    "BrowDownLeftOuter": "BrowDownLeft",
    "BrowDownLeftInner": "BrowDownLeft",
    "BrowDownRightOuter": "BrowDownRight",
    "BrowDownRightInner": "BrowDownRight",
    "BrowUpLeftInner": "BrowInnerUp",
    "BrowUpRightInner": "BrowInnerUp",
    "BrowUpLeftOuter": "BrowOuterUpLeft",
    "BrowUpRightOuter": "BrowOuterUpRight",
    "MouthClose": "MouthClose_fix"
}
"""dict: ключи - лайвлинк код, значения - название блендшейпа mfa."""