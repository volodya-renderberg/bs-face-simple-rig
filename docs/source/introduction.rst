.. _introduction-page:

Introduction
============

.. note:: Риг на блендшейпах

Конфигурирование
------------------

.. note:: Всё необходимое для конфигурирования в директории ``face_meta``, \
    добавление в ``sys.path`` пути до этой диретории определяется в модуле :ref:`pipeline_connect-page`

* face_meta директория:
    * :ref:`config-page`
    * :ref:`hook-page`
    * :ref:`face_controls_tmp-page`

Наборы блендшейпов
--------------------

* :ref:`base_forms-page`
* :ref:`base_union_forms-page`