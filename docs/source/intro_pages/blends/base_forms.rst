.. _base_forms-page:

Базовые формы
===============

.. note:: Формы в основном соответствующие ``FACS`` кодам, и совпадающие по неймингу с кодами ``LIVE_LINK``.

`Facs wiki <https://ru.wikipedia.org/wiki/Система_кодирования_лицевых_движений>`_


.. _BrowDownLeft:

BrowDownLeft|BrowDownRight
---------------------------

Опускание брови общее.

``facs: 4 - Brow lowerer``

---------------

.. figure:: ../../img/gif/brow_down.gif
    :scale: 100 %
    :align: center


.. _BrowDownLeftInner:

BrowDownLeftInner|BrowDownRightInner
-------------------------------------

Опускание внутренней области брови.

``facs: 42 - Inner eyebrow lowerer``

---------------


.. _BrowDownLeftOuter:

BrowDownLeftOuter|BrowDownRightOuter
-------------------------------------

Опускание внешней области брови.

``facs: _``

---------------


.. _BrowLateralLeft:

BrowLateralLeft|BrowLateralRight
---------------------------------

Сведение бровей к центру, мышца гордеца.

``facs: 44 - Eyebrow gatherer``

---------------

.. figure:: ../../img/gif/BrowLateral.gif
    :scale: 75 %
    :align: center


.. _BrowUpLeft:

BrowUpLeft|BrowUpRight
-----------------------

Общий подъём брови

``facs: _``

---------------

.. figure:: ../../img/gif/BrowUp.gif
    :scale: 75 %
    :align: center


.. _BrowUpLeftInner:

BrowUpLeftInner|BrowUpRightInner
---------------------------------

Подъём внутренней части брови

``facs: 1 - Inner brow raiser``

---------------

.. figure:: ../../img/gif/BrowUpInner.gif
    :scale: 75 %
    :align: center


.. _BrowUpLeftOuter:

BrowUpLeftOuter|BrowUpRightOuter
---------------------------------

Подъём внешней части брови

``facs: 2 - Outer brow raiser``

---------------

.. figure:: ../../img/gif/BrowUpOuter.gif
    :scale: 75 %
    :align: center


.. _CheekPuff:

CheekPuff
----------

Надувание щёк

``facs: 34 - Cheek puff``

---------------

.. figure:: ../../img/gif/CheekPuff.gif
    :scale: 75 %
    :align: center


.. _CheekPuffLeft:

CheekPuffLeft|CheekPuffRight
-----------------------------

Надувание щёки с одной стороны

``facs: 34 - Cheek puff``

---------------


.. _CheekSquintLeft:

CheekSquintLeft|CheekSquintRight
---------------------------------

Подниматель щеки `(круговая мышца глаза) <https://ru.wikipedia.org/wiki/Круговая_мышца_глаза>`_

``facs: 6 - Cheek raiser``

---------------

.. figure:: ../../img/gif/CheekSquintLeft.gif
    :scale: 75 %
    :align: center


.. _CheekSuck:

CheekSuck
----------

Втягивание щёк

``facs: 35 - Cheek suck``

---------------

.. figure:: ../../img/gif/CheekSuck.gif
    :scale: 75 %
    :align: center


.. _CheekSuckLeft:

CheekSuckLeft|CheekSuckRight
-----------------------------

Втягивание щеки с одной стороны

``facs: 35 - Cheek suck``

---------------


.. _ChinCompressLeft:

ChinCompressLeft|ChinCompressRight
-----------------------------------

Подниматель подбородка `(Подбородочная мышца) <https://ru.wikipedia.org/wiki/Подбородочная_мышца>`_

``facs: 17 - Chin raiser``

---------------

.. figure:: ../../img/gif/ChinCompress.gif
    :scale: 75 %
    :align: center


.. _ChinRaiseLeft:

ChinRaiseLeft|ChinRaiseRight
-----------------------------

Пожимание губ

``facs: _``

---------------

.. figure:: ../../img/gif/ChinRaise.gif
    :scale: 75 %
    :align: center


.. _EyeBlinkLeft:

EyeBlinkLeft|EyeBlinkRight
---------------------------

Моргание

``facs: 45 Blink``

---------------

.. figure:: ../../img/gif/EyeBlink.gif
    :scale: 75 %
    :align: center


.. _EyeLookDownLeft:

EyeLookDownLeft|EyeLookDownRight
---------------------------------

Взгляд вниз

``facs: 64 Eyes down``

---------------

.. figure:: ../../img/gif/EyeLookDown.gif
    :scale: 75 %
    :align: center


.. _EyeLookInLeft:

EyeLookInLeft|EyeLookInRight
-----------------------------

Глаза к носу

``facs: 61/62 Eyes left|right``

---------------

.. figure:: ../../img/gif/EyeLookIn.gif
    :scale: 75 %
    :align: center


.. _EyeLookOutLeft:

EyeLookOutLeft|EyeLookOutRight
-------------------------------

Глаза в стороны

``facs: 61/62 Eyes left|right``

---------------

.. figure:: ../../img/gif/EyeLookOut.gif
    :scale: 75 %
    :align: center


.. _EyeLookUpLeft:

EyeLookUpLeft|EyeLookUpRight
-----------------------------

Взгляд вверх

``facs: 63 Eyes up``

---------------

.. figure:: ../../img/gif/EyeLookUp.gif
    :scale: 75 %
    :align: center


.. _EyeSquintLeft:

EyeSquintLeft|EyeSquintRight
-----------------------------

Натягивание нижнего века

``facs: 7 Lid tightener``

---------------

.. figure:: ../../img/gif/EyeSquint.gif
    :scale: 75 %
    :align: center


.. _EyeWideLeft:

EyeWideLeft|EyeWideRight
-------------------------

Максимальное раскрытие век

``facs: 5 Upper lid raiser + ``

---------------

.. figure:: ../../img/gif/EyeWideLeft.gif
    :scale: 75 %
    :align: center


.. _JawBack:

JawBack
--------

Челюсть назад

``facs: _ ``

---------------


.. _JawForward:

JawForward
-----------

Челюсть вперёд

``facs: 29 Jaw thrust ``

---------------

.. _JawLeft:

JawLeft
--------

Челюсть влево

``facs: 30 Jaw sideways ``

---------------

.. figure:: ../../img/gif/JawLeft.gif
    :scale: 75 %
    :align: center


.. _JawOpen:

JawOpen
--------

Открытый рот

``facs: 26 	Jaw drop ``

---------------

.. figure:: ../../img/gif/JawOpen.gif
    :scale: 75 %
    :align: center


.. _JawRight:

JawRight
---------

Челюсть вправо, аналогично :ref:`JawLeft`

``facs: 30 Jaw sideways ``

---------------


.. _LipCornerSharpenDLeft:

LipCornerSharpenDLeft|LipCornerSharpenDRight
---------------------------------------------

Слипание уголков губ (нижняя губа)

``facs: _ ``

---------------

.. figure:: ../../img/gif/LipCornerSharpenDLeft.gif
    :scale: 75 %
    :align: center


.. _LipCornerSharpenULeft:

LipCornerSharpenULeft|LipCornerSharpenURight
---------------------------------------------

Слипание уголков губ (верхняя губа)

``facs: _ ``

---------------

.. figure:: ../../img/gif/LipCornerSharpenULeft.gif
    :scale: 75 %
    :align: center


.. _LipPressLeft:

LipPressLeft|LipPressRight
---------------------------

Сжимание губ.

``facs: _ ``

---------------

.. figure:: ../../img/gif/LipPressLeft.gif
    :scale: 75 %
    :align: center


.. _LipsTowards:

LipsTowards
------------

Губы вперёд

``facs: 8 Lips toward each other ``

---------------

.. figure:: ../../img/gif/LipsTowards.gif
    :scale: 75 %
    :align: center


.. _MouthDimpleLeft:

MouthDimpleLeft|MouthDimpleRight
---------------------------------

Натягивание губ в стороны

``facs: 20 Lip stretcher ``

---------------

.. figure:: ../../img/gif/MouthDimpleLeft.gif
    :scale: 75 %
    :align: center


.. _MouthDown:

MouthDown
----------

Губы вниз

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthDown.gif
    :scale: 75 %
    :align: center


.. _MouthFrownLeft:

MouthFrownLeft|MouthFrownRight
-------------------------------

Опускание уголков губ

``facs: 15 	Lip corner depressor ``

---------------

.. figure:: ../../img/gif/MouthFrownLeft.gif
    :scale: 75 %
    :align: center


.. _MouthFunnel:

MouthFunnel
------------

ГУбы воронкой

``facs: 22 Lip funneler ``

---------------

.. figure:: ../../img/gif/MouthFunnel.gif
    :scale: 75 %
    :align: center


.. _MouthLeft:

MouthLeft|MouthRight
---------------------

Губы в сторону

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthLeft.gif
    :scale: 75 %
    :align: center


.. _MouthLowerDownLeft:

MouthLowerDownLeft|MouthLowerDownRight
---------------------------------------

Опускание ни жней губы

``facs: 16 Lower lip depressor ``

---------------

.. figure:: ../../img/gif/MouthLowerDownLeft.gif
    :scale: 75 %
    :align: center


.. _MouthPressLeft:

MouthPressLeft|MouthPressRight
-------------------------------

Рот сжат

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthPressLeft.gif
    :scale: 75 %
    :align: center


.. _MouthPucker:

MouthPucker
------------

Губы пучком

``facs: 18 Lip pucker ``

---------------

.. figure:: ../../img/gif/MouthPucker.gif
    :scale: 75 %
    :align: center


.. _MouthRollLowerLeft:

MouthRollLowerLeft|MouthRollLowerRight
---------------------------------------

Подкручивание нижней губы внутрь

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthRollLowerLeft.gif
    :scale: 75 %
    :align: center


.. _MouthRollUpperLeft:

MouthRollUpperLeft|MouthRollUpperRight
---------------------------------------

Подкручивание верхней губы внутрь

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthRollUpperLeft.gif
    :scale: 75 %
    :align: center


.. _MouthSharpPullLeft:

MouthSharpPullLeft|MouthSharpPullRight
---------------------------------------

Острый подниматель уголка губы

``facs: 13 Sharp lip puller ``

---------------

.. figure:: ../../img/gif/MouthSharpPullLeft.gif
    :scale: 75 %
    :align: center


.. _MouthShrugLowerLeft:

MouthShrugLowerLeft|MouthShrugLowerRight
-----------------------------------------

Выкручивание нижней губы наружу

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthShrugLowerLeft.gif
    :scale: 75 %
    :align: center


.. _MouthShrugUpperLeft:

MouthShrugUpperLeft|MouthShrugUpperRight
-----------------------------------------

Выкручивание верхней губы наружу

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthShrugUpperLeft.gif
    :scale: 75 %
    :align: center


.. _MouthSmileLeft:

MouthSmileLeft|MouthSmileRight
-------------------------------

Улыбка

``facs: 12 	Lip corner puller ``

---------------

.. figure:: ../../img/gif/MouthSmileLeft.gif
    :scale: 75 %
    :align: center


.. _MouthStretchLeft:

MouthStretchLeft|MouthStretchRight
-----------------------------------

Натягивание нтжней губы вниз через шею

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthStretchLeft.gif
    :scale: 75 %
    :align: center


.. _MouthUp:

MouthUp
--------

Рот вверх

``facs: _ ``

---------------

.. figure:: ../../img/gif/MouthUp.gif
    :scale: 75 %
    :align: center


.. _MouthUpperUpLeft:

MouthUpperUpLeft|MouthUpperUpRight
-----------------------------------

Верхняя губа вверх, агрессия.

``facs: 10 Upper lip raiser ``

---------------

.. figure:: ../../img/gif/MouthUpperUpLeft.gif
    :scale: 75 %
    :align: center

.. _NasolabialLeft:

NasolabialLeft|NasolabialRight
-------------------------------

Создание носогубной складки

``facs: 11 Nasolabial deepener ``

---------------

.. figure:: ../../img/gif/NasolabialLeft.gif
    :scale: 75 %
    :align: center


.. _NoseDepressLeft:

NoseDepressLeft|NoseDepressRight
---------------------------------

Ноздря вниз

``facs: _ ``

---------------

.. figure:: ../../img/gif/NoseDepressLeft.gif
    :scale: 75 %
    :align: center


.. _NoseSneerLeft:

NoseSneerLeft|NoseSneerRight
-----------------------------

Сморщивание носа

``facs: 9 Nose wrinkler ``

---------------

.. figure:: ../../img/gif/NoseSneerLeft.gif
    :scale: 75 %
    :align: center


.. _NostrilCompressLeft:

NostrilCompressLeft|NostrilCompressRight
-----------------------------------------

Суживатель ноздрей

``facs: 39	Nostril compressor ``

---------------

.. figure:: ../../img/gif/NostrilCompressLeft.gif
    :scale: 75 %
    :align: center

.. _NostrilDilateLeft:

NostrilDilateLeft|NostrilDilateRight
-------------------------------------

Расширение ноздрей

``facs: 38	Nostril dilator ``

---------------

.. figure:: ../../img/gif/NostrilDilateLeft.gif
    :scale: 75 %
    :align: center


.. _eye_l_Pupil_Constrict:

eye_l_Pupil_Constrict|eye_r_Pupil_Constrict
--------------------------------------------

Зрачки сужены

``facs: _ ``

---------------

.. _eye_l_Pupil_Dilate:

eye_l_Pupil_Dilate|eye_r_Pupil_Dilate
--------------------------------------

Зрачки расширены

``facs: _ ``

---------------