.. _base_union_forms-page:

Базовые объединённые формы
==============================

.. note:: Финальные формы смешивания какого либо набора базовых форм. Редактируются на моделлинге.


CheekSquintSharpPullLeft_u|CheekSquintSharpPullRight_u
------------------------------------------------------
Смешивание:
    - :ref:`CheekSquintLeft`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/CheekSquintSharpPullLeft_u.gif
    :scale: 75 %
    :align: center




CheekSquintSmileLeft_u|CheekSquintSmileRight_u
----------------------------------------------
Смешивание:
    - :ref:`CheekSquintLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/CheekSquintSmileLeft_u.gif
    :scale: 75 %
    :align: center




EyeBlinkCSquintLeft_u|EyeBlinkCSquintRight_u
--------------------------------------------
Смешивание:
    - :ref:`EyeBlinkLeft`
    - :ref:`CheekSquintLeft`

---------------

.. figure:: ../../img/gif/EyeBlinkCSquintLeft_u.gif
    :scale: 75 %
    :align: center




EyeBlinkESquintLeft_u|EyeBlinkESquintRight_u
--------------------------------------------
Смешивание:
    - :ref:`EyeBlinkLeft`
    - :ref:`EyeSquintLeft`

---------------

.. figure:: ../../img/gif/EyeBlinkESquintLeft_u.gif
    :scale: 75 %
    :align: center




EyeBlinkFSquintLeft_u|EyeBlinkFSquintRight_u
--------------------------------------------
Смешивание:
    - :ref:`EyeBlinkLeft`
    - :ref:`EyeSquintLeft`
    - :ref:`CheekSquintLeft`

---------------

.. figure:: ../../img/gif/EyeBlinkFSquintLeft_u.gif
    :scale: 75 %
    :align: center




EyeCheekSquintLeft_u|EyeCheekSquintRight_u
------------------------------------------
Смешивание:
    - :ref:`EyeSquintLeft`
    - :ref:`CheekSquintLeft`

---------------

.. figure:: ../../img/gif/EyeCheekSquintLeft_u.gif
    :scale: 75 %
    :align: center



.. _EyeCompressLeft_u:

EyeCompressLeft_u|EyeCompressRight_u
------------------------------------

.. note:: Пока не используется

Смешивание:
    - :ref:``

---------------

.. figure:: ../../img/gif/EyeCompressLeft_u.gif
    :scale: 75 %
    :align: center



EyeCompress_u
-------------

.. note:: Пока не используется

Объединённая форма для :ref:`EyeCompressLeft`

Смешивание:
    - :ref:``

---------------

.. figure:: ../../img/gif/EyeCompress_u.gif
    :scale: 75 %
    :align: center




JawOpenDimpleLeft_u|JawOpenDimpleRight_u
----------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthDimpleLeft`

---------------

.. figure:: ../../img/gif/JawOpenDimpleLeft_u.gif
    :scale: 75 %
    :align: center




JawOpenFrownLeft_u|JawOpenFrownRight_u
--------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthFrownLeft`

---------------

.. figure:: ../../img/gif/JawOpenFrownLeft_u.gif
    :scale: 75 %
    :align: center




JawOpenFunnel_u
---------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthFunnel`

---------------

.. figure:: ../../img/gif/JawOpenFunnel_u.gif
    :scale: 75 %
    :align: center




JawOpenMouthCloseFunnel_u
-------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthFunnel`
    - :ref:`MouthClose_u`

---------------

.. figure:: ../../img/gif/JawOpenMouthCloseFunnel_u.gif
    :scale: 75 %
    :align: center




JawOpenMouthClosePuckerFunnel_u
-------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthFunnel`
    - :ref:`MouthPucker`
    - :ref:`MouthClose_u`

---------------

.. figure:: ../../img/gif/JawOpenMouthClosePuckerFunnel_u.gif
    :scale: 75 %
    :align: center




JawOpenMouthClosePucker_u
-------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthPucker`
    - :ref:`MouthClose_u`

---------------

.. figure:: ../../img/gif/JawOpenMouthClosePucker_u.gif
    :scale: 75 %
    :align: center




JawOpenMouthLowerDownLeft_u|JawOpenMouthLowerDownRight_u
--------------------------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthLowerDownLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/JawOpenMouthLowerDownLeft_u.gif
    :scale: 75 %
    :align: center




JawOpenPuckerFunnel_u
---------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthFunnel`
    - :ref:`MouthPucker`

---------------

.. figure:: ../../img/gif/JawOpenPuckerFunnel_u.gif
    :scale: 75 %
    :align: center




JawOpenPucker_u
---------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthPucker`

---------------

.. figure:: ../../img/gif/JawOpenPucker_u.gif
    :scale: 75 %
    :align: center




JawOpenSharpPullLeft_u|JawOpenSharpPullRight_u
----------------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/JawOpenSharpPullLeft_u.gif
    :scale: 75 %
    :align: center




JawOpenSmileLeft_u|JawOpenSmileRight_u
--------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/JawOpenSmileLeft_u.gif
    :scale: 75 %
    :align: center




JawOpenSmileMLowerDownLeft_u|JawOpenSmileMLowerDownRight_u
----------------------------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthSmileLeft`
    - :ref:`MouthLowerDownLeft`

---------------

.. figure:: ../../img/gif/JawOpenSmileMLowerDownLeft_u.gif
    :scale: 75 %
    :align: center



JawOpenStretchLeft_u|JawOpenStretchRight_u
------------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthStretchLeft`

---------------

.. figure:: ../../img/gif/JawOpenStretchLeft_u.gif
    :scale: 75 %
    :align: center




MouthCloseJawLeft_u|MouthCloseJawRight_u
-----------------------------------------
Смешивание:
    - :ref:`JawLeft`
    - :ref:`MouthClose_u`

---------------

.. figure:: ../../img/gif/MouthCloseJawLeft_u.gif
    :scale: 75 %
    :align: center




MouthCloseJawOpenLeft_u|MouthCloseJawOpenRight_u
------------------------------------------------
Смешивание:
    - :ref:`JawOpen`
    - :ref:`JawLeft`
    - :ref:`MouthClose_u`

---------------

.. figure:: ../../img/gif/MouthCloseJawOpenLeft_u.gif
    :scale: 75 %
    :align: center



.. _MouthClose_u:

MouthClose_u
------------
Смешивание:
    - :ref:`JawOpen`
    - сомкнутые губы

---------------

.. figure:: ../../img/gif/MouthClose_u.gif
    :scale: 75 %
    :align: center




MouthLeftPucker_u|MouthRightPucker_u
--------------------------------------
Смешивание:
    - :ref:`MouthLeft`
    - :ref:`MouthPucker`

---------------

.. figure:: ../../img/gif/MouthLeftPucker_u.gif
    :scale: 75 %
    :align: center




MouthLowerDownSharpPullLeft_u|MouthLowerDownSharpPullRight_u
------------------------------------------------------------
Смешивание:
    - :ref:`MouthLowerDownLeft`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/MouthLowerDownSharpPullLeft_u.gif
    :scale: 75 %
    :align: center




MouthLowerDownSmileLeft_u|MouthLowerDownSmileRight_u
----------------------------------------------------
Смешивание:
    - :ref:`MouthLowerDownLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/MouthLowerDownSmileLeft_u.gif
    :scale: 75 %
    :align: center




MouthLowerDownStretchLeft_u|MouthLowerDownStretchRight_u
--------------------------------------------------------
Смешивание:
    - :ref:`MouthStretchLeft`
    - :ref:`MouthLowerDownLeft`

---------------

.. figure:: ../../img/gif/MouthLowerDownStretchLeft_u.gif
    :scale: 75 %
    :align: center




MouthPuckerFunnel_u
-------------------
Смешивание:
    - :ref:`MouthFunnel`
    - :ref:`MouthPucker`

---------------

.. figure:: ../../img/gif/MouthPuckerFunnel_u.gif
    :scale: 75 %
    :align: center




MouthPuckerTowards_u
--------------------
Смешивание:
    - :ref:`LipsTowards`
    - :ref:`MouthPucker`

---------------

.. figure:: ../../img/gif/MouthPuckerTowards_u.gif
    :scale: 75 %
    :align: center




MouthRollJawOpen_u
------------------

.. note:: Пока не используется

Смешивание:
    - :ref:`JawOpen`
    - :ref:`MouthRollUpperLeft` (оба бленда)
    - :ref:`MouthRollLowerLeft` (оба бленда)
    
---------------

.. figure:: ../../img/gif/MouthRollJawOpen_u.gif
    :scale: 75 %
    :align: center




MouthRollLowerLeftJawOpen_u|MouthRollLowerRightJawOpen_u
--------------------------------------------------------
Смешивание:
    - :ref:`MouthRollLowerLeft` (подворачивания на обе стороны по отдельности)
    - :ref:`JawOpen`

---------------

.. figure:: ../../img/gif/MouthRollLowerLeftJawOpen_u.gif
    :scale: 75 %
    :align: center




MouthRollUpperLeftJawOpen_u|MouthRollUpperRightJawOpen_u
--------------------------------------------------------
Смешивание:
    - :ref:`MouthRollUpperLeft` (подворачивания на обе стороны по отдельности)
    - :ref:`JawOpen`

---------------

.. figure:: ../../img/gif/MouthRollUpperLeftJawOpen_u.gif
    :scale: 75 %
    :align: center




MouthStretchSharpPullLeft_u|MouthStretchSharpPullRight_u
--------------------------------------------------------
Смешивание:
    - :ref:`MouthStretchLeft`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/MouthStretchSharpPullLeft_u.gif
    :scale: 75 %
    :align: center




MouthStretchSmileLeft_u|MouthStretchSmileRight_u
------------------------------------------------
Смешивание:
    - :ref:`MouthStretchLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/MouthStretchSmileLeft_u.gif
    :scale: 75 %
    :align: center




MouthUpperUpSharpPullLeft_u|MouthUpperUpSharpPullRight_u
--------------------------------------------------------
Смешивание:
    - :ref:`MouthUpperUpLeft`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/MouthUpperUpSharpPullLeft_u.gif
    :scale: 75 %
    :align: center




MouthUpperUpSmileLeft_u|MouthUpperUpSmileRight_u
------------------------------------------------
Смешивание:
    - :ref:`MouthUpperUpLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/MouthUpperUpSmileLeft_u.gif
    :scale: 75 %
    :align: center




MouthUpperUpSneerLeft_u|MouthUpperUpSneerRight_u
------------------------------------------------
Смешивание:
    - :ref:`MouthUpperUpLeft`
    - :ref:`NoseSneerLeft`

---------------

.. figure:: ../../img/gif/MouthUpperUpSneerLeft_u.gif
    :scale: 75 %
    :align: center




NasolabialDimpleLeft_u|NasolabialDimpleRight_u
----------------------------------------------
Смешивание:
    - :ref:`NasolabialLeft`
    - :ref:`MouthDimpleLeft`

---------------

.. figure:: ../../img/gif/NasolabialDimpleLeft_u.gif
    :scale: 75 %
    :align: center




NasolabialFrownLeft_u|NasolabialFrownRight_u
--------------------------------------------
Смешивание:
    - :ref:`NasolabialLeft`
    - :ref:`MouthFrownLeft`

---------------

.. figure:: ../../img/gif/NasolabialFrownLeft_u.gif
    :scale: 75 %
    :align: center




NasolabialSharpPullLeft_u|NasolabialSharpPullRight_u
----------------------------------------------------
Смешивание:
    - :ref:`NasolabialLeft`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/NasolabialSharpPullLeft_u.gif
    :scale: 75 %
    :align: center




NasolabialSmileLeft_u|NasolabialSmileRight_u
--------------------------------------------
Смешивание:
    - :ref:`NasolabialLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/NasolabialSmileLeft_u.gif
    :scale: 75 %
    :align: center




NasolabialStretchLeft_u|NasolabialStretchRight_u
------------------------------------------------
Смешивание:
    - :ref:`NasolabialLeft`
    - :ref:`MouthStretchLeft`

---------------

.. figure:: ../../img/gif/NasolabialStretchLeft_u.gif
    :scale: 75 %
    :align: center




SharpPullSneerLeft_u|SharpPullSneerRight_u
------------------------------------------
Смешивание:
    - :ref:`NoseSneerLeft`
    - :ref:`MouthSharpPullLeft`

---------------

.. figure:: ../../img/gif/SharpPullSneerLeft_u.gif
    :scale: 75 %
    :align: center




SmileSneerLeft_u|SmileSneerRight_u
----------------------------------
Смешивание:
    - :ref:`NoseSneerLeft`
    - :ref:`MouthSmileLeft`

---------------

.. figure:: ../../img/gif/SmileSneerLeft_u.gif
    :scale: 75 %
    :align: center




