.. templates-page:

Menu / Templates
----------------

Import Facial Blends Joints
===========================

* Импорт базовых костей лицевого рига.
* Настраивается на персонажа *тюнерами*. Включение видимости *тюнеров* атрибут ``face_character_grp.tuners_display``.
* Рут группа ``face_character_grp`` - содержит ``notes`` (просмотр через **Atribut Editor**) с описанием настроек.

.. _import_ctrl_template_link:

Import template of controls
===========================

Импорт шаблона лицевых контролов.