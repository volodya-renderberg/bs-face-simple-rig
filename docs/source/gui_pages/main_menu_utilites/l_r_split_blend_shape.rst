.. _l_r_split_blend_shape-page:

L/R Split Blend Shape
======================

.. figure:: ../../img/l_r_split_blend_shape.png
    :scale: 100 %
    :align: center
    :alt: main gui

.. note:: Требуется установленный блендер, добавленный п переменную среды ``path``

Neutral Mesh:
--------------
- Поле ввода имени нейтральной сетики
- ``<<`` - добавление в поле имени выделенного объекта

Centrtal Split
---------------

- Распилка Центрального бленда на ``L`` / ``R``
    - для выделенных сеток
- Добавление постфикса ``Left`` / ``Right``

Brows Split
---------------

- Распиловка левых/правых блендов на ``Inner`` / ``Outer``
    - для выделенных сеток
- Добавление соответсвующих постфиксов ``Inner`` / ``Outer``

.. _l_r_split_blend_shape-results:

Results
----------

- Открывает системную темп директорию
    - использовать фильтр ``split``
- Можно импортить ``fbx`` руками

.. figure:: ../../img/l_r_split_blend_shape_result.png
    :scale: 100 %
    :align: center
    :alt: main gui

.. _l_r_split_blend_shape-factors:

Коэффициэнты K, W
-------------------
- ``k`` - множитель умножаемый на все весовые значения - пока всегда ``1``
- ``w`` - ширина области смуса распиловки - проверять в бленд файле ``.../tmp/split_l_r_work.blend``
    - объект ``SPLIT_RESULT_BASE``
    - режим ``weight paint mode``

.. figure:: ../../img/l_r_split_blend_shape_w_test.png
    :scale: 100 %
    :align: center
    :alt: main gui