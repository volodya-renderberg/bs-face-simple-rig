.. tools-page:

Menu / Tools
------------

Edit Blend Shapes
=================

«Вытаскивание» блендшейпов для редактирования в **maya**.

Окно
~~~~

.. figure:: ../img/edit_bs_gui.png
    :scale: 100 %
    :align: center
    :alt: main gui

Функционал
~~~~~~~~~~

* меню **Names Of Sides** - присутствуют стандартные варианты обозначения правой и левой сторон.
* меню **Select Blend Node** - содержит перечень бленд нод данной сцены. При выборе бленд ноды отобразится список её таргетов с кнопками:
    * кнопка **Extract** - создаст из данных таргета сетку соответствующую данному таргету и подключённую в бленд ноду, тоесть радактируя её мы редактируем данный бленд шейп.
    .. note:: Все созданные бленды помещаются под локатор ``bs_meshes_edit_root``, его можно отодвинуть в сторону от головы, для удобства работы.
    * кнопка **Remove Selected Vertices** - убирает из действия бленд ноды выбранные вершины.
    * Нижняя полка кнопок для работы с выделенными объектами и экспорта-импорта.
        * **Extract Selected** - достанет блендшейп, значение веса которого равно 1, если таких будет больше одного, будет ошибка. Сделано чтобы не вчитываться в списки. Двигаем за контрол, жмём кнопку и бленд в работе.
        * **Mirror Select** - создаёт зеркальный блендшейп по выделенной сетке. Для этого надо выделить объект блендшейпа, правый или левый не важно, и нажать на кнопку, будет создана зеркальная сетка с подключением.
        * **Export All** - делает экспорт всех форм (кроме нейтральной) в указаную директорию, в файл - ``bs_face_simple_Blend_Shapes.mb``
        * **Import All** - делает импорт и применение форм из указанного файла, после применения импортнутые формы удаляются.
        * **Remove Select Vertices From BS** - убирает из действия бленд ноды выбранные вершины.

.. _make_correct_by_bs:
        
Make Correct By BS
==================

Создание корректирующих блендшейпов относительно других блендшейпов, и создание новых блендшейпов из комбинации существующих.

* данные берутся из :attr:`bs_face_simple.lists.CORRECTS` и :attr:`bs_face_simple.lists.MAKE_SHAPES`.
* Условия:
    * Загруженные бленды от моделлеров находятся в одной иерархии.
    * Выделен родительский объект блендов.
    * В сцене находится нейтральная сетка, в мире, названа ``Neutral``.

.. _export_bs_mesh_to_source_split:

Export BS Meshes to Source/Split
================================

«Раскладывание» сеток блендов по **source/split** диреториям.

* Нужно для того случая, когда надо разложить бленды ``obj`` файлами по папкам **source** и **split** для резки на право - лево в гудини.
* Что как раскладывать, определяется по спискам из :mod:`bs_face_simple.lists.TO_SPLIT_MESHES` и :mod:`bs_face_simple.lists.TO_SOURSE_MESHES`.

Окно
~~~~

.. figure:: ../img/to_source_split_gui.png
    :scale: 100 %
    :align: center
    :alt: main gui

Функционал
~~~~~~~~~~

* Исходное состояние - бленды отдельными мешами в мая файле.
* Определяем пути к **source** и **split** директориям (**Source Folder,  Split Folder**). Данная **split** директория указывается как ``source`` в :attr:`bs_face_simple.lists.MULTY_DATA`.
* Выделяем экспортируемые сетки и жмём **Export Selected Meshes**.
* Если выбрать фильтр в меню **select The Filter** - то будут экспортироваться только сетки из этого списка.
    * фильтр определяется в :mod:`bs_face_simple.lists.SOURCE_SPLIT_FILTER`
* **Tools/Test Missing** - проверка присутствия в сцене сеток блендов. Имена отсутствующих блендов будут напечатаны в терминале, так же будут сохранены в текстовый файл ``/tmp/bs_missing.txt``, который будет открыт по завершению скрипта.

.. _make_correct_by_skin:

Make Correct By Skin
====================

Создание корректирующих блендшейпов относительно скин_кластера.

Окно
~~~~

.. figure:: ../img/correct_by_skin_gui.png
    :scale: 50 %
    :align: center
    :alt: main gui

Функционал
~~~~~~~~~~

* Условия:
    * Сцена лицевого рига (скиновая часть до создания блендов).
    .. attention:: Убедится, что скиновые сетки не имеют дополнительных деформаторов, только скинкластер!! иначе взрыв!. 
    * Новые бленды разложены про **source** и **split** директориям.
* Меню:
    * **Select The Lod** - надо выбрать лод для которого будет создаваться коррект. Список лодов берётся из :mod:`bs_face_simple.lists.MULTY_DATA`.
    * **Select The Method** - выбор метода, которым будет создаваться коррект. По умолчанию ``cvshapeinverter method`` метод (предпочтительно).
* Чек боксы:
    * **Auto Export Correct Shape** - сразу делается экспорт коррект формы в нужную директорию.
    * **Auto Export Correct Shape** - удаляется созданная коррект форма, после создания и экспорта.
* Список кнопок на каждый бленд (данные из :mod:`bs_face_simple.lists.POSES_DATA`):
    * **Имя бленда** - создаётся коррект на данный бленд: выставляется поза, импортится бленд, применяется **shape_invert**
    * **set pose** - только выставляется поза, без создания корректа.
* кнопка **All** - создаётся коррект на все бленды.
* кнопка **set Neutral pose** - постановка в нейтральную позу.

Make Zscript
============

.. attention:: Возможно не будет использовано

Генерация скрипта ``Zscripts`` (файл *[character name]_export_bs.txt*) для экспорта блендов отдельными *.obj* файлами в директорию ``C:\bs_export\`` запускается в **ZBrush** на **windows**

Reexport from Zbrush
====================

.. attention:: Возможно не будет использовано

Экспорт блендов отдельными обжами в указанную директорию из сетки полученной fbx экспортом из браша. Для исполнения скрипта надо выбрать меш содержащий бленд.

import Obj from folder
======================

.. attention:: Возможно не будет использовано

Импорт блендов в майский файл из указанной директории, содержащей обжи блендов. (недоработано)

Remove Face Rig
===============

удаление лицевого рига из боди рига. (``hook.remove_face_rig(lists)``)


.. _apply_face_rig:

Apply Face Rig
==============

импорт и подсоединение лицевого рига в боди риг, из запаблишенного файла :attr:`bs_face_simple.lists.FACE_RIG_TO_BODY_FILE` . (``hook.apply_face_rig_from_file(lists)``)

.. _apply_face_rig_from_file:

Apply Face Rig From file
========================

импорт (откроет файлбраузер для выбора файла) и подсоединение лицевого рига в боди риг. (``hook.apply_face_rig_from_file(lists)``)
