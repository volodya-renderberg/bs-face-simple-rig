.. _coding-page:

Схема кодирования подключения блендов
=====================================

Применение
----------

Кодирование задаётся словарём, где ключ - название атрибута блендшейп ноды, а значение - схема подключения. 

Используются два словаря: 

* :mod:`bs_face_simple.lists.ACTIONS`
    * Управление назначается на одноимённые названиям блендшейпов атрибуты объекта :mod:`bs_face_simple.lists.ACTION_OB`
* :mod:`bs_face_simple.lists.LIST_OF_SHAPES`
    * Управление непосредствено на атрибуты блендшейп ноды.
    * Правильнее всего использовать `Тип соединения "connect"`_ на атрибуты объекта :mod:`bs_face_simple.lists.ACTION_OB`


Тип соединения "connect"
------------------------

Простой коннект.

Структура записи:
::
    {
    ...
    'имя атрибута блендшейп ноды': ('connect', 'имя объекта отдающего коннект', 'название атрибута'),
    ...
    }
    
Пример:
::
    {
    ...
    'eyeLookDownLeft':('connect', ACTIONS_OB, 'eyeLookDownLeft'),
    ...
    }

.. _driven-key-frame:

Тип соединения "drivenKeyframe"
-------------------------------

Создаётся :obj:`drivenKeyframe` с произвольным количеством анимационных ключей.

Структура записи:
::
    {
    ...
    'имя атрибута блендшейп ноды': ('driver', ['имя управляющего объекта', 'название атрибута', '''словарь анимационных ключей''' {'0.0':0.0, '-0.2':1.0, '-0.45':0.0}]),
    ...
    }
    
Пример:
::
    {
    ...
    'lidCompressLeft': ('driver', ['CTRL_eye_press_L', 'ty', {'0.0':0.0, '1.0': 1.0}]),
    ...
    }


.. _multiply-coding:

Тип соединения "multiply"
-------------------------

Перемножение значений от нескольких :obj:`drivenKeyframe`. Если укзан один котреж, то \
сработает как :ref:`driven-key-frame`

Создаётся несколько :obj:`drivenKeyframe` нод, выходы которых перемножаются :obj:`MultiplyDivide` нодами.

.. figure:: img/bs_face_multiply_connect.png
    :scale: 100 %
    :align: center
    :alt: Три множителя
    
Структура записи:
::
    {
    ...
    'имя атрибута блендшейп ноды': ('multiply', [
        ('имя управляющего объекта 1', 'название атрибута', '''словарь анимационных ключей''' {'0.0':0.0, '-0.2':1.0, '-0.45':0.0}), 
        ('имя управляющего объекта 2', 'название атрибута', '''словарь анимационных ключей''' {'0.0':0.0, '1.0':1.0}), # включающий фактор
        ('имя управляющего объекта 3', 'название атрибута', '''словарь анимационных ключей''' {'0.0':1.0, '1.0':0.0}), # выключающий фактор
        ]),
    ...
    }
    
В качестве имён и значений можно использовать константы и переменные из :mod:`bs_face_simple.lists`

Пример: 
::
    {
    ...
    'eyeLookInRight':('multiply', [
        (EYE_LOOK_DRIVER_R, HORIZONTAL_AXIS, {'0.0':0.0, EYE_LIMITS[1][0]:1.0}),
        (EYE_LOOK_DRIVER_R, VERTICAL_AXIS, {'0.0':1.0, EYE_LIMITS[0][0]:0.0}),
        ('CTRL_eye_R', 'autolid', {'0.0':0.0, '1.0':1.0}),
        ('CTRL_up_eye_lid_R', 'ty', {'0.0':1.0, '0.8':0.0}),
        ('CTRL_eye_press_R', 'ty', {'0.0': 1.0, '1.0': 0.0}),
        ]),
    ...
    }

.. _multiply-min-coding:

Тип соединения "multiply_min"
-----------------------------

.. attention:: Реализовано только для Blender.

Структура записи аналогична :ref:`multiply-coding` только тип соединения указывать ``multiply_min``.

Результат вычислений будет аналогичен:
::
    1*min(var1,var2,...varN)

.. _multiply-max-coding:

Тип соединения "multiply_max"
-----------------------------

.. attention:: Реализовано только для Blender.

Структура записи аналогична :ref:`multiply-coding` только тип соединения указывать ``multiply_max``.

Результат вычислений будет аналогичен:
::
    1*max(var1,var2,...varN)


Тип соединения "expression"
---------------------------

Для управления блендшейп атрибутом создаётся ``expression`` нода.

Структура записи:
::
    {
    ...
    'имя атрибута блендшейп ноды':('expression',
            {'strings':
                [
                'строка 1', # каждая строка завершается ``;``
                'строка 2',
                'строка 3',
                ...
                current_target = ...; # в скрипте 'current_target' будет заменено на управляемый атрибут.
                ],
            },
        ),
    ...
    }

Пример:
::
    {
    ...
    'jawLeft':('expression',
            {'strings':
                [
                'float $x = CTRL_jaw.mouthClose;',
                'float $y = CTRL_jaw.ty;',
                'float $factor;',
                'if(CTRL_jaw.tx <= 0) $factor=0;',
                'else $factor = CTRL_jaw.tx;',
                '$y1 = -0.2;',
                '$z1 = $x;',
                '$y2 = -0.04575*$x-0.21994;',
                '$z2 = 0;',
                'if($y < -0.2) $z = ($y2*$z1-$y1*$z2-$y*($z1-$z2))/($y2-$y1);',
                'else $z = (0.04 + 0.2*$y - 0.2*$x*$y)*25;',
                '$z= clamp(0, 1.0, $z);',
                'current_target = (1-$z)*$factor;',
                ],
            },
        ),
    ...
    }


Тип соединения "switch"
-----------------------

Переключение между двумя разными алгоритмами нодой ``blendTwoAttr``

Пример:
::
    'lipShrug':('switch', 
        {
            'switcher': ('CTRL_mouth', 'press', {'0.0':0.0, '1.0': 1.0}), # дрейвин кей на бленд атрибут.
            'input[0]': ('multiply', (                                    # любой алгоритм
                ('CTRL_mouth','ty', {'0.5': 0.0, '1.0': 1.0}),
                ('CTRL_mouth','tz', {'0.0': 1.0, '1.0': 0.0}),
                ('CTRL_jaw', 'ty', {'%s' % jawDrop:0.0, '0.0':1.0})
                )),
            'input[1]': ('multiply', (                                    # любой алгоритм
                ('CTRL_mouth','ty', {'0.0': 0.0, '1.0': 1.0}),
                ('CTRL_mouth','tz', {'0.0': 1.0, '1.0': 0.0}),
                ('CTRL_jaw', 'ty', {'%s' % jawDrop:0.0, '0.0':1.0})
                )),
            }
        ),
