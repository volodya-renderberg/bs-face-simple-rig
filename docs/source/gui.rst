.. _gui-manual-page:

GUI Manual
==========

Команда запуска
---------------

    .. code-block:: python
        :linenos:
        
        import sys
        from importlib import reload

        path = "path_to/bs-face-simple-rig"
        if not path in sys.path:sys.path.append(path)

        from bs_facial_rig import ui
        reload(ui)
        ui.run()
      
Запускаемое окно
----------------

.. _main_gui:

.. figure:: img/main_gui.png
    :scale: 100 %
    :align: center
    :alt: main gui

.. _menu_tools_link:    
    
Меню
----

.. toctree::
    :maxdepth: 1
    
    gui_pages/utilities

Бленды
-------

.. toctree::
    :maxdepth: 1
    
    gui_pages/blends/checking_blend_shapes
    gui_pages/blends/appends_blend_shapes


Сборка
-------

Контролы
~~~~~~~~~~

.. toctree::
    :maxdepth: 1

    gui_pages/ctrls/import_rig_template
    gui_pages/ctrls/set_position
    gui_pages/ctrls/surf_ctrls

Управление
~~~~~~~~~~~

.. toctree::
    :maxdepth: 1

    gui_pages/build/pre_build
    gui_pages/build/clear_driving
    gui_pages/build/full_clear
    gui_pages/build/driving_blend_shapes
