import tempfile
import sys
import os
import traceback
import json

import bpy

try: # for docs
    with open(sys.argv[4]) as f:
        data = json.load(f)
except:
     data = [0,1,2,3,4,5,6,7,8,9] # for docs

print('-'*100)
print(data)
fbx_path = data[0]
print(fbx_path)
k = float(data[1])
print(k)
w = float(data[2])
print(w)
ob = data[3]
print(ob)
base = data[4]
print(base)
split_type = data[5]
print(split_type)
print('-'*100)

def _rename_shape_key_from_auto_key(ob, name):
    """
    Переименовывает первый бленд начинающийся с ``Key`` на новое имя
    """
    for bs in ob.data.shape_keys.key_blocks:
        if bs.name.startswith('Key'):
            bs.name = name

def main(base, target, split_type):
    """
    Doc
    """
    try:
        dir = tempfile.gettempdir()
        save_path = os.path.join(dir, 'split_l_r_work.blend')
        export_path = os.path.join(dir, 'SPLIT_L_R_RESULT.fbx')
        # bpy.ops.wm.save_as_mainfile(filepath=save_path)

        # -- import fbx
        try:
            bpy.ops.import_scene.fbx(
                filepath=fbx_path,
                # directory=os.path.dirname(fbx_path)
            )
        except:
            print(traceback.format_exc())

        # -- objects
        base = bpy.data.objects[base]
        target = bpy.data.objects[target]

        # -- vertex_group
        make_vertex_groups(bpy.context, base, split_type)

        # -- bs
        for ob in bpy.data.objects:
            ob.select_set(False)

        target.select_set(True)
        base.select_set(True)
        bpy.context.view_layer.objects.active = base

        target_name = target.name
        for g in base.vertex_groups:
            print('-'*50)
            print(g.name)
            bs_name = f"{target_name}{g.name.split('_')[1]}"
            target.name = bs_name
            r = bpy.ops.object.join_shapes()
            if not r == {'FINISHED'}:
                print(f"Blend Shape {bs_name} No Maked!!!")
                continue
            else:
                print(f"Blend Shape {bs_name} Maked!!!")
            shape_key = base.data.shape_keys.key_blocks[bs_name]
            shape_key.vertex_group=g.name
            shape_key.value=1
            bpy.ops.object.shape_key_add(from_mix=True)
            base.shape_key_remove(shape_key)
            _rename_shape_key_from_auto_key(base, bs_name)

        # -- export
        for ob in bpy.data.objects:
            ob.select_set(False)
        base.select_set(True)
        bpy.context.view_layer.objects.active = base
        base.name = 'SPLIT_RESULT_BASE'
        bpy.ops.export_scene.fbx(filepath=export_path, use_selection=True)
    except Exception as e:
        print(traceback.format_exc())
        bpy.ops.wm.save_as_mainfile(filepath=save_path)
        raise Exception(e)

    # -- fin
    bpy.ops.wm.save_as_mainfile(filepath=save_path)


def _get_weight(x, min, max):
        value= (x-min)/(max-min)
        return value


def make_vertex_groups(context, ob, split_type, action='split'):
    """
    Создание вертекс групп на сетке для распилки
    """
   
    if split_type=="CENTRAL":
            l_group_name=f'{action}_Left'
            r_group_name=f'{action}_Right'
            left_border = w/2
            right_border = -left_border
    elif split_type=="L_BROW":
            l_group_name=f'{action}_Outer'
            r_group_name=f'{action}_Inner'
            left_border = w
            right_border = 0
    elif split_type=="R_BROW":
            l_group_name=f'{action}_Inner'
            r_group_name=f'{action}_Outer'
            left_border = 0
            right_border = -w

    # l_group_name=f'{action}_Left'
    # r_group_name=f'{action}_Right'

    # (2) vertex groups exists
    if not l_group_name in ob.vertex_groups:
        l_v_group=ob.vertex_groups.new(name=l_group_name)
    else:
        l_v_group=ob.vertex_groups[l_group_name]
    # --
    if not r_group_name in ob.vertex_groups:
        r_v_group=ob.vertex_groups.new(name=r_group_name)
    else:
        r_v_group=ob.vertex_groups[r_group_name]

    # (3) weights
    # left_border = w/2
    # right_border = -left_border
    # -- l
    for v in ob.data.vertices:
        if v.co[0]*k>left_border:
            l_v_group.add([v.index], 1.0, 'REPLACE')
            r_v_group.add([v.index], 0.0, 'REPLACE')
        elif v.co[0]*k<right_border:
            l_v_group.add([v.index], 0.0, 'REPLACE')
            r_v_group.add([v.index], 1.0, 'REPLACE')
        else:
            value=_get_weight(v.co[0]*k, right_border, left_border)
            l_v_group.add([v.index], value, 'REPLACE')
            r_v_group.add([v.index], 1-value, 'REPLACE')


try: # for docs
    main(base, ob, split_type)
except:
    pass
