#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import tempfile
import logging
import traceback
import subprocess
import json

# -- logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
file_handler = logging.FileHandler(os.path.join(os.path.expanduser('~'), 'face_split_bs.log'))
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

def fbx_export_settings():
    mel.eval("FBXExportUpAxis y")
    mel.eval("FBXExportAnimationOnly -v false;")
    mel.eval("FBXExportConstraints -v false;")
    mel.eval("FBXExportSkeletonDefinitions -v false;")
    # mel.eval("FBXExportCameras -v false;")
    mel.eval("FBXExportLights -v false;")
    mel.eval("FBXExportInputConnections -v false;")

def split_l_r(k, w, neutral, split_type="CENTRAL"):
    """
    Экспорт выделенной сетки в fbx. Запуск фонового процесса распилки в блендер.
    """
    logger.info('%s - %s' % (k,w))
    ob = mc.ls(sl=True)[0]
    mc.select(ob)
    mc.select(neutral, add=True)

    fbx_path = os.path.join(tempfile.gettempdir(), 'to_split_l_r.fbx').replace('\\', '/')
    # logger.info(fbx_path)

    fbx_export_settings()
    mel.eval('FBXExport -f "{}" -s'.format(fbx_path))

    split_path = os.path.join(os.path.dirname(__file__), "split_bs_action.py").replace('\\', '/')
    bat_path = os.path.join(os.path.dirname(__file__), "split.bat").replace('\\', '/')
    data_path = os.path.join(tempfile.gettempdir(), "split_data.json").replace('\\', '/')
    with open(data_path, "w") as f:
        json.dump([fbx_path, k, w, ob, neutral, split_type], f, sort_keys=True, indent=4), 
    com = 'start {0} {1} {2}'.format(bat_path, split_path, data_path)
    
    subprocess.call(com, shell=True)


def take_result():
    dir = tempfile.gettempdir()
    result_path = os.path.join(dir, 'SPLIT_L_R_RESULT.fbx')
    try:
        mel.eval('FBXImport -f "{}"'.format(result_path))
    except:
        os.system('start %tmp%')
    

def run():
    """Gui"""
    try:
        # window
        window = pm.window(t='L/R Split Blend Shape')

        pm.columnLayout(adj=True)

        pm.rowLayout(nc=3, adj=True)
        pm.text('Neutral Mesh:')
        neutral = pm.textField()
        neutral_btn = pm.button(l='<<')
        pm.setParent('..')

        pm.text('Central Split')
        
        pm.rowLayout(nc=4, adj=True)
        pm.text('K:')
        k = pm.floatField(v=1)
        pm.text('Width:')
        w = pm.floatField(v=5)
        pm.setParent('..')

        # pm.rowLayout(nc=2, adj=True)
        split_btn = pm.button(l='L/R Split')
        # take_btn = pm.button(l='Take Result')
        # pm.setParent('..')

        pm.text('Brows Split')

        pm.rowLayout(nc=4, adj=True)
        pm.text('K:')
        bk = pm.floatField(v=1)
        pm.text('Width:')
        bw = pm.floatField(v=5)
        pm.setParent('..')

        pm.rowLayout(nc=2, adj=True)
        split_l_brow_btn = pm.button(l='Split L Brow')
        split_r_brow_btn = pm.button(l='Split R Brow')
        pm.setParent('..')

        pm.text('Results')

        take_btn = pm.button(l='Take Result')

        pm.setParent('..')

        def set_neutral():
            ob = mc.ls(sl=True)[0]
            mc.textField(neutral, e=True, text=ob)

        def get_run(k, w, neutral):
            kv = pm.floatField(k, q=True, v=True)
            wv = pm.floatField(w, q=True, v=True)
            nv = pm.textField(neutral, q=True, text=True)
            split_l_r(kv,wv, nv)

        def get_run_brow(k, w, neutral, split_type):
            kv = pm.floatField(k, q=True, v=True)
            wv = pm.floatField(w, q=True, v=True)
            nv = pm.textField(neutral, q=True, text=True)
            split_l_r(kv,wv, nv, split_type)


        split_btn.setCommand(pm.Callback(get_run, k, w, neutral))
        split_l_brow_btn.setCommand(pm.Callback(get_run_brow, bk, bw, neutral, "L_BROW"))
        split_r_brow_btn.setCommand(pm.Callback(get_run_brow, bk, bw, neutral, "R_BROW"))
        neutral_btn.setCommand(pm.Callback(set_neutral))
        take_btn.setCommand(pm.Callback(take_result))

        # mc.showWindow(window)
        window.show()
        pm.window(window, edit=True, wh=[350, 200])
    except:
        logger.error(traceback.format_exc())