# -*- coding: utf-8 -*-

"""Хеш тесты файлов. """

import hashlib
import os
import shutil

def md5(file_name):
    """
    Возвращает md5 хеш файла.

    :parm file_name: file path
    :type file_name: str
    :return: str
    """
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def match_md5(file1, file2):
    """сверка двух файлов по md5 хешу.

    Parameters
    -----------
    file1 : str
        file path
    file2 : str
        file path
   
    Returns
    ---------
    bool
        True - совпадение.
    """
    if md5(file1)==md5(file2):
        return True


def copy_source_to_target_with_hash_test(source_path, target_path):
    """
    Копирование файла с проверкой при наличии по пути назначения по хешу.
    Если файл существует, но хеш отличается, файл всё равно будет перезаписан.

    Parameters
    -----------
    source_path : str
        source_path
    target_path : str
        target_path

    Returns
    --------
    None
    """
    if not os.path.exists(os.path.dirname(target_path)):
        os.makedirs(os.path.dirname(target_path))
    
    if not os.path.exists(target_path):
        shutil.copy(source_path, target_path)
    elif  not match_md5(source_path, target_path):
        os.remove(target_path)
        shutil.copy(source_path, target_path)