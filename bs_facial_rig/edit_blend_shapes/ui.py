#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Зависимости: config.LIVE_LINK_CODES
"""

import logging
import traceback
import subprocess
import json
import os

# -- logger
logger = logging.getLogger(__name__)
# logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
file_handler = logging.FileHandler(os.path.join(os.path.expanduser('~'), 'face_edit_bs.log'))
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

import pymel.core as pm
import maya.cmds as mc
import webbrowser

import settings

# maya не видит settings, поэтому прямая ссылка
def help_tutorial(*args):
    '''help '''
    path = settings.HELP.convert_u_fix_help
    webbrowser.open(path)


def _select_to_field(mesh_field):
    """
    Добавление имени выбранного объекта в текстовое поле.
    """
    ob = mc.ls(sl=True)[0]
    mc.textField(mesh_field, e=True, text=ob)


def _conf_line_to_string(conf_line):
    """Преобразует элементарную строку конфига в ``str`` для сверки с ``LIVE_LINK_CODES``"""
    key = None
    for k, v in conf_line[2].items():
        if v == 1:
            key = k
    string = "%s|%s|%s" % (conf_line[0], conf_line[1], key)
    return string


def _on_off_test(condition):
    """Тест: является ли строка конфига включающей или нет.
    
    Parameters
    ----------
    condition : list tuple
        элементарная строка условия в конфиге типа: (ob, attr, {keys})

    Returns
    --------
    bool
    """
    if "1.0" in condition[2]:
        max = "1.0"
    elif "-1.0" in condition[2]:
        max = "-1.0"
    if condition[2][max] == 0:
        return False
    else:
        return True


def _test_condition(config, conf_line, conf_item):
    """
    Проверяет выполнение условия включения бленда, задаваемого строкой конфига (от контрола)
    * если тестируемой строке соответствует бленд (через LIVE_LINK_CODES) и все остальные значения ``conf_item``\
    удовлетворяют условию включения этого бленда - то этот бленд участвует в активации бленда соответствующего ``conf_item``

    Parameters
    -----------
    config : config.py
        config
    conf_line : list tuple
        тестируемая элементарная строка конфига (условия не от бленда)
    conf_item : tuple
        конфига бленда, содержащий тестируемую строку.

    Returns
    -------
    bool
    """
    print("_test_condition()")
    conf_string = _conf_line_to_string(conf_line)
    if not conf_string in config.LIVE_LINK_CODES:
        return
    
    # -- conf_items
    conf_items = []
    if conf_item[0] == "driver":
        conf_items = [conf_item[1]]
    elif conf_item[0] == "multiply":
        conf_items = conf_item[1]
    
    # -- test items
    test_blend = config.LIVE_LINK_CODES[conf_string]
    test_conf_item = config.DRIVER_CONFIG[test_blend]
    test_items = []
    if test_conf_item[0] == "driver":
        test_items = [test_conf_item[1]]
    elif test_conf_item[0] == "multiply":
        test_items = test_conf_item[1]

    # -- количество включающих строк
    on_lines = 0
    for it in test_items:
        if _on_off_test(it):
            on_lines += 1
    
    # # -- Проверка на количество включающих строк
    # # количество строк в текущем конфиге не может быть меньше чем в тестируемом
    # # в противном случае не сможет быть выполнено условие включения. 
    # if len(conf_items) < len(test_items):
    #     return
    
    # -- Проверка на условие включения
    tru = 0
    for c_item in conf_items:
        for t_item in test_items:
            if c_item[0]==t_item[0] and c_item[1]==t_item[1]:
                print("-"*10)
                print(c_item)
                print(t_item)
                if _on_off_test(c_item) and _on_off_test(c_item) == _on_off_test(t_item):
                    tru+=1
                    print("True")
    if tru == on_lines:
        return True


def _get_activating_keys(config, blend_shape):
    """
    Даёт словарь соответствия влияющих блендов и их весов.

    Parameters
    ----------
    config : py.module
        config
    blend_shape : str
        имя блендшейпа для которого идёт поиск списка активации.
    
    Returns
    --------
    dict
        {bs_name: weight, ...}
    """
    # r_data=[]
    r_data = {}

    if not config.DRIVER_CONFIG.get(blend_shape):
        return

    conf = config.DRIVER_CONFIG[blend_shape]
    print(conf)

    if conf[0] == "driver":
        print(json.dumps(conf[1], sort_keys=True, indent=4))
        if conf[1][0] != config.DRIVING_OB:
            # config.LIVE_LINK_CODES
            return
        else:
            pass
    elif conf[0] == "multiply" or conf[0] == "min":
        for item in conf[1]:
            print("test item: ", item)
            if "1.0" in item[2]:
                max = "1.0"
            elif "-1.0" in item[2]:
                max = "-1.0"
            if item[2][max] == 0:
                continue
            if item[0] == config.DRIVING_OB:
                if item[1].endswith("_driver"):
                    continue
                else:
                    # r_data.append((item[1], item[2][max]))
                    r_data[item[1]] = item[2][max]
            else:
                # key = None
                # for k, v in item[2].items():
                #     if v == 1:
                #         key = k
                # string = "%s|%s|%s" % (item[0], item[1], key)
                # if string in config.LIVE_LINK_CODES:
                #     if config.LIVE_LINK_CODES[string] in config.DRIVER_CONFIG:
                #         if config.LIVE_LINK_CODES[string] != blend_shape:
                #             # r_data.append((config.LIVE_LINK_CODES[string], item[2][max]))
                #             r_data[config.LIVE_LINK_CODES[string]] = item[2][max]
                activate = _test_condition(config, item, conf)
                string = _conf_line_to_string(item)
                if activate and config.LIVE_LINK_CODES[string] != blend_shape:
                    r_data[config.LIVE_LINK_CODES[string]] = item[2][max]
                else:
                    continue

    return r_data


def _short(name):
    """Не полное имя объекта"""
    return name.split('|')[-1:][0]


def _long_prefix(name):
    """Возвращает префикс иерархии полного имени объекта"""
    h = name.split('|')
    return '|'.join(h[:-1])


def _long(prefix, name):
    """
    Возвращает полное имя

    Parameters
    ----------
    prefix: str
        префикс полного имени - иерархия, результат функции :func:`_long_prefix`
    name: str
        короткое имя объекта
    """
    return "|".join((prefix, name))


def _parent(ob):
    """
    Возвращает родителя
    """
    p = mc.listRelatives(ob, p=True, f=True)
    if p:
        return p[0]


def _fix_name(ob: str):
    """Возвращает _fix имя для бленда"""
    if not ob.endswith("_u") and not ob.endswith("_fix"):
        print("wrong object")
        return
    if ob.endswith("_u"):
        fix_name = ob[:-2] + "_fix"
        return fix_name
    return ob


def _print_data(config, ob=None, action="CONFIG"):
    """Печатает инфу по управлению данным блендом.
    
    Parameters
    -----------
    config : object
        модуль конфига
    ob: str optional
        имя объекта

    """
    if ob is None:
        try:
            ob = mc.ls(sl=True)[0]
            print(ob)
        except:
            print("No selected object")
            return
        
    fix_name = _fix_name(ob)
    if fix_name is None:
        return

    if _short(fix_name) in config.DRIVER_CONFIG:
        if action == "CONFIG":
            print(json.dumps(config.DRIVER_CONFIG[_short(fix_name)], sort_keys=True, indent=4))
            
        elif action == "ACTIVATION":
            activation = _get_activating_keys(config, _short(fix_name))
            print(json.dumps(activation, sort_keys=True, indent=4))
    else:
        print("%s - not in Config" % fix_name)


def _make_correct(n, union, activation, hierarhy):
    """Создание корректриующей формы, методом вычитания.
    
    Parameters
    ----------
    n : str
        нейтральная сетка
    union : str
        union сетка
    activations : dict
        результат выполнения функции :func:`_get_activating_keys`
    hierarhy : str
        результат выполнения метода :func:`_long_prefix`
    """
    print("-"*10 + " _make_correct()")
    print(union)
    print(activation)
    mc.select(d=True)
    for item in activation.keys():
        mc.select(_long(hierarhy, item), add=True)
    mc.select(_long(hierarhy, _short(union)), add=True)
    mc.select(n, add=True)
    # mc.select(list(activation.keys()), add=True)
    bs = pm.blendShape()[0]
    for k, v in activation.items():
        eval('bs.%s.set(%s)' % (k, -v))
    eval('bs.%s.set(%s)' % (_short(union), 1))


def _convert_u_to_fix(config, ob, neutral, dr={"b": True, "d": False}, new=True):
    """
    Конвертирование объединённых форм в корректирующие для выделенной сетки.
    Работает с дочерними объектами выделенной группы.

    Fix = U - N - sum(BS)

    Parameters
    -----------
    config: module
        config
    ob : str
        имя ``_u`` сетки
    dr: dict
        управляющие параметры
        * b: bool - запекать бленды или нет
        * d: bool - удалять исходный _fix или нет (переименовывается + '_old')
    """
    def copy_ob(ob, new_name, p):
        copy = mc.duplicate(ob, name="COPY")
        try:
            mc.parent(copy, p)
        except:
            pass
        copy = mc.rename(copy, _short(fix_name))
        print("COPY: " + _short(copy))
        print("COPY LONG: " + copy)
        return copy
    
    print("DR: %s" % dr)

    if not ob.endswith("_u"):
        logger.info("No Working! %s" % ob)
        return
    fix_name = ob[:-2] + "_fix"
    hierarhy = _long_prefix(ob)
    logger.info("HIERARCHY: " + hierarhy)
    p = _parent(ob)
    if not _short(fix_name) in config.DRIVER_CONFIG:
        return "No Working!"
    logger.info(ob)
    # --
    logger.info("CONFIG:")
    activation = _get_activating_keys(config, _short(fix_name))
    logger.info(activation)
    # -- fix_ob
    # fix_ob = mc.duplicate(ob)
    # fix_ob = mc.rename(fix_ob, _short(fix_name))

    if new:
        # -- COPY
        # copy = copy_ob(ob, fix_name, p)
        copy = copy_ob(neutral, fix_name, p)
        # -- bs
        _make_correct(copy, ob, activation, hierarhy)
    else:
        # -- COPY
        copy = copy_ob(neutral, fix_name, p)
        # copy = mc.duplicate(neutral, name="COPY")
        # try:
        #     mc.parent(copy, p)
        # except:
        #     pass
        # copy = mc.rename(copy, _short(fix_name))
        # print("COPY: " + _short(copy))
        # print("COPY LONG: " + copy)
        
        # -- bs1
        mc.select(d=True)
        for k in activation.keys():
            mc.select(_long(hierarhy, k), add=True)
        mc.select(_long(hierarhy, _short(copy)), add=True)
        bs = pm.blendShape()[0]
        for k, v in activation.items():
            eval('bs.%s.set(%s)' % (k, v))
        mc.delete(_long(hierarhy, _short(copy)), ch=True)

        # -- bs2
        mc.select([neutral, ob], add=True)
        mc.select(_long(hierarhy, _short(copy)), add=True)
        bs = pm.blendShape()[0]
        for item in [neutral, _short(ob)]:
            eval('bs.%s.set(%s)' % (item, 1))

    # -- finishing
    # -- -- remove old fix
    if dr.get("d"):
        mc.delete(ob)
    else:
        mc.rename(ob, _short(ob) + '_old')
    # -- -- bake bs
    if dr.get("b"):
        mc.delete(_long(hierarhy, _short(copy)), ch=True)
    else:
        pass

    logger.info("Ok! %s" % ob)
    return


def _convert_fix_to_u(config, ob, neutral, dr={"b":True, "d":False}):
    """
    Конвертирование корректирующих форм в объединённые для выделенной сетки.
    Работает с дочерними объектами выделенной группы.

    U = N + sum(BS) + Fix

    Parameters
    -----------
    dr: dict
        управляющие параметры
        * b: bool - запекать бленды или нет
        * d: bool - удалять исходный _fix или нет (переименовывается + '_old')
    """
    if not ob.endswith("_fix") or not _short(ob) in config.DRIVER_CONFIG:
        return "No Working!"
    u_name = _short(ob)[:-4] + "_u"
    hierarhy = _long_prefix(ob)
    # --
    print("OB: " + ob)
    print("HIERARHY: " + hierarhy)
    print("U_NAME: " + u_name)
    # --
    activation = _get_activating_keys(config, _short(ob))
    print(activation)
    # -- u_ob
    p = _parent(ob)
    u_ob = mc.duplicate(neutral)
    try:
        mc.parent(u_ob, p)
    except:
        print(traceback.format_exc())
    u_ob = mc.rename(u_ob, u_name)
    print("U_OB: " + u_ob)

    # -- bs
    mc.select(d=True)
    for k in activation:
        mc.select(_long(hierarhy, k), add=True)
    mc.select(ob, add=True)
    mc.select(_long(hierarhy, _short(u_ob)), add=True)
    # -- --
    bs = pm.blendShape()[0]
    activation[_short(ob)] = 1.0
    print("ACTIVATION: " + str(activation))
    for k,v in activation.items():
        eval('bs.%s.set(%s)' % (k, v))

    # -- finishing
    # -- -- remove old fix
    if dr.get("d"):
        mc.delete(ob)
    else:
        mc.rename(ob, _short(ob) + '_old')
    # -- -- bake bs
    if dr.get("b"):
        mc.delete(_long(hierarhy, _short(u_ob)), ch=True)
    else:
        pass

    return "Ok!"


CALLER = {"fix_to_u": _convert_fix_to_u, "u_to_fix": _convert_u_to_fix}


def convert_selected(config, mesh_field, action, del_, bake_):
    """
    action: str
        значение из ["fix_to_u", "u_to_fix"]
    """
    ob = mc.ls(sl=True, l=True)[0]
    neutral = mc.textField(mesh_field, q=True, text=True)
    dr = {
        "d": mc.checkBox(del_, q=True, v=True),
        "b": mc.checkBox(bake_, q=True, v=True),
    }
    CALLER[action](config, ob, neutral, dr=dr)


def convert_all(config, mesh_field, action, del_, bake_):
    """
    Конвертирование объединённых форм в корректирующие.
    Работает с дочерними объектами выделенной группы.

    action: str
        значение из ["fix_to_u", "u_to_fix"]
    """
    neutral = mc.textField(mesh_field, q=True, text=True)
    dr = {
        "d": mc.checkBox(del_, q=True, v=True),
        "b": mc.checkBox(bake_, q=True, v=True),
    }
    for ob in mc.listRelatives(c=True, f=True):
        # print(ob)
        CALLER[action](config, ob, neutral, dr=dr)


def run(config):
    """Gui"""
    try:
        # window
        window = pm.window(t='1Convert Blend Shapes')

        menuBar = pm.menuBarLayout()
        pm.menu(l='Help')
        pm.menuItem(l='Manual', c=help_tutorial)

        pm.columnLayout(adj=True)
        del_ = pm.checkBox(l="del source", v=True)
        bake_ = pm.checkBox(l="bake bs", v=True)

        pm.rowLayout(nc=2, adj=True)
        pm.button(l="Print Config", c=pm.Callback(_print_data, config))
        pm.button(l="Print Activation", c=pm.Callback(_print_data, config, action="ACTIVATION"))
        pm.setParent('..')

        pm.rowLayout(nc=3, adj=True)
        pm.text('Mesh:', align='right')
        mesh_field = pm.textField()
        pm.button(l='<<', c=pm.Callback(_select_to_field, mesh_field))
        pm.setParent('..')

        pm.text('Convert:', align='left')
        pm.rowLayout(nc=3, adj=True)
        pm.text('_fix -> _u:', align='left')
        pm.button(l='all',
                  c=pm.Callback(convert_all, config, mesh_field, "fix_to_u",
                                del_, bake_))
        pm.button(l='selected',
                  c=pm.Callback(convert_selected, config, mesh_field,
                                "fix_to_u", del_, bake_))
        pm.setParent('..')
        pm.rowLayout(nc=3, adj=True)
        pm.text('_u -> _fix:', align='left')
        pm.button(l='all',
                  c=pm.Callback(convert_all, config, mesh_field, "u_to_fix",
                                del_, bake_))
        pm.button(l='selected',
                  c=pm.Callback(convert_selected, config, mesh_field,
                                "u_to_fix", del_, bake_))
        pm.setParent('..')

        pm.setParent('..')

        window.show()
        pm.window(window, edit=True, wh=[350, 200])
    except:
        logger.error(traceback.format_exc())
