#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Описание содержимого файла hook.py

Данный файл содержит изменяемые процедуры сборки лицевого рига.

"""

import json
import os
import sys
import traceback

import pymel.core as pm
import maya.mel as mel
import maya.cmds as mc

import cvshapeinverter

try:
    from importlib import reload
except:
    pass

import facial_utils as utils
try:
    reload(utils)
except:
    pass
import facial_utils as utils

#from bs_face_simple import on_mesh_ctrls

try:
    wr_path=utils.get_path('USER_WORKDIR')
    if wr_path and not wr_path in sys.path:
        sys.path.append(wr_path)
except:
    pass # для доки

try:
    from face_meta import hook as hook_local
    reload(hook_local) # иначе валит документацию.
    from face_meta import hook as hook_local
except:
    pass

def __wire_no_off(config, off=True):
    pass
    for wire_ob in pm.ls(type='wire'):
        wire_attr = pm.PyNode('%s.envelope' % wire_ob)
        if off:
            wire_attr.set(0)
        else:
            wire_attr.set(1)
    if hasattr(config, "WIRE_ATTR_LIST"):
        for item in config.WIRE_ATTR_LIST:
            if not pm.objExists(item[0]):
                continue
            object_ = pm.PyNode(item[0])
            if off:
                object_.set(item[1][0])
            else:
                object_.set(item[1][1])
    if off:
        var = 'disabled'
    else:
        var = 'enabled'
    pm.confirmDialog(t = 'message', m = 'Wire is %s!' % var)


def pre_bild(config):
    """Действия запускаемые перед сборкой лицевого рига.
    
    по окончании запускает :func:`local_data.face_meta.hook.pre_bild()`
    """

    print("global PREBILD")
       
    # # wire off
    # __wire_no_off(config)

    # create math assets nodes
    if hasattr(config, config.FACE_MATH_NODES) and pm.objExists(config.FACE_MATH_NODES):
        pm.delete(config.FACE_MATH_NODES)
    parent_asset=pm.createNode('container', n=config.FACE_MATH_NODES)
    # # --
    # child_assets = [
    #     config.BROW_AND_LIDS_ASSET,
    #     config.JAW_SIDES_DRIVER_ASSET
    # ]
    # childs=[]
    # for asset_name in child_assets:
    #     if pm.objExists(asset_name):
    #         pm.delete(asset_name)
    #     asset=pm.createNode('container', n=asset_name)
    #     childs.append(asset)
    # # --
    # pm.container(parent_asset, e=1, an=childs)

    # # create ik_fk controls of eye
    # if config.MAKE_EYE_IK_CONTROL:
    #     utils.make_eye_ik_control(config)
    
    # make Actions
    # # (1)
    # if pm.objExists(config.ACTION_OB):
    #     pm.delete(config.ACTION_OB)
    # action_ob = pm.group(empty=True, name = config.ACTION_OB)
    #
    # # (2)
    # for name in config.ACTIONS:
    #     utils.add_attr(action_ob, name)
    
    # # make Dependence
    # for name in sorted(config.ACTIONS.keys()):
    #     pass
    #     fn(name, config.ACTIONS, action_ob)

    hook_local.pre_bild(config)

# действия запускаемые после сборки лицевого рига, "допилка"
def post_bild(config):
    """Действия запускаемые после сборки лицевого рига.
    
    Содержит блоки кода ``обязательных`` и ``не обязательных действий``.

    по окончании запускает :func:`local_data.face_meta.hook.post_bild()`
    """
    # 
    PARENT_FOR_DRIVING_OB = 'setup'
    if pm.objExists(PARENT_FOR_DRIVING_OB) and pm.objExists(config.DRIVING_OB):
        try:
            pm.parent(config.DRIVING_OB, PARENT_FOR_DRIVING_OB)
        except:
            pass
    
    ### Общее не зависимое от наличия кластерного рига.
    if hasattr(config, 'POST_BILD_CONNECT'):
        for output_name, input_name in config.POST_BILD_CONNECT:
            pass
            input_attr = pm.PyNode(input_name)
            output_attr = pm.PyNode(output_name)
            output_attr >> input_attr

    # POST_BILD_OTHER_CONNECT
    if hasattr(config, 'POST_BILD_OTHER_CONNECT'):
        for key in config.POST_BILD_OTHER_CONNECT:
            for i in range(0, config.POST_BILD_OTHER_CONNECT[key]['num']):
                ob = pm.PyNode(config.POST_BILD_OTHER_CONNECT[key]['driven'][0])
                ob_attr = config.POST_BILD_OTHER_CONNECT[key]['driven'][1]
                ctrl = pm.PyNode(config.POST_BILD_OTHER_CONNECT[key]['driver'][0])
                ctrl_attr = config.POST_BILD_OTHER_CONNECT[key]['driver'][1]
                dvalue = config.POST_BILD_OTHER_CONNECT[key]['driver'][2][i]
                value = config.POST_BILD_OTHER_CONNECT[key]['driven'][2][i]
                com = "pm.setDrivenKeyframe(ob.%s, cd=ctrl.%s, dv = %f, v = %f, itt = 'linear', ott = 'linear')" % (ob_attr, ctrl_attr, dvalue, value)
                eval(com)
                print('$'*100)
                print(key, com)

    # POST_BILD_REORDER_DEFORMERS
    if hasattr(config, 'POST_BILD_REORDER_DEFORMERS'):
        for key in config.POST_BILD_REORDER_DEFORMERS:
            if pm.objExists(key) and pm.objExists(config.POST_BILD_REORDER_DEFORMERS[key][0]) and pm.objExists(config.POST_BILD_REORDER_DEFORMERS[key][1]):
                mesh = pm.PyNode(key)
                pm.reorderDeformers(config.POST_BILD_REORDER_DEFORMERS[key][0], config.POST_BILD_REORDER_DEFORMERS[key][1], mesh)

    # POST_BILD_PARENTS
    if hasattr(config, 'POST_BILD_PARENTS'):
        for child, parent in config.POST_BILD_PARENTS.items():
            if pm.objExists(child) and pm.objExists(parent):
                pm.parent(child, parent)

    # POST_BILD_PARENT_CONSTRAINTS
    if hasattr(config, 'POST_BILD_PARENT_CONSTRAINTS'):
        for child, parent in config.POST_BILD_PARENT_CONSTRAINTS.items():
            if pm.objExists(child) and pm.objExists(parent):
                pm.select(parent, child)
                pm.parentConstraint(mo=True)

    # POST_BILD_TO_MESH_LAYER
    if hasattr(config, 'POST_BILD_TO_MESH_LAYER'):
        if not pm.objExists('face_meshes'):
            pm.createDisplayLayer(n = 'face_meshes', nr=True)
        for ob in config.POST_BILD_TO_MESH_LAYER:
            try:
                pm.editDisplayLayerMembers('face_meshes', ob)
            except Exception as e:
                print("Exception: in hook.postbild - POST_BILD_TO_MESH_LAYER - %s" % e)

    # POST_BILD_TO_RIG_LAYER
    if hasattr(config, 'POST_BILD_TO_RIG_LAYER'):
        if not pm.objExists('face_rig'):
            pm.createDisplayLayer(n = 'face_rig', nr=True)
        for ob in config.POST_BILD_TO_RIG_LAYER:
            try:
                pm.editDisplayLayerMembers('face_rig', ob)
            except Exception as e:
                print("Exception: in hook.postbild - POST_BILD_TO_MESH_LAYER - %s" % e)
    
    # POST_BILD_HIDE_OBJECTS
    if hasattr(config, 'POST_BILD_HIDE_OBJECTS'):
        try:
            pm.hide(config.POST_BILD_HIDE_OBJECTS)
        except Exception as e:
            print("Exception: in hook.postbild - POST_BILD_HIDE_OBJECTS - %s" % e)

    # POST_BILD_HIDE_SHAPE_OBJECTS
    if hasattr(config, 'POST_BILD_HIDE_SHAPE_OBJECTS'):
        for ob in config.POST_BILD_HIDE_SHAPE_OBJECTS:
            try:
                ob=pm.PyNode(ob)
                shape = ob.getShape()
                pm.hide(shape)
            except Exception as e:
                print("Exception: in hook.postbild - POST_BILD_HIDE_SHAPE_OBJECTS - %s" % e)

    # SET MIN MAX
    if hasattr(config, 'POST_BILD_SET_MIN_MAX_ATTR'):
        for name in config.POST_BILD_SET_MIN_MAX_ATTR:
            try:
                attr = pm.PyNode(name)
                attr.setMin(config.POST_BILD_SET_MIN_MAX_ATTR[name][0])
                attr.setMax(config.POST_BILD_SET_MIN_MAX_ATTR[name][1])
            except Exception as e:
                print("in POST_BILD_SET_MIN_MAX_ATTR (exception1): %s" % e)
                try:
                    ob, attr = name.split('.')
                    ldata = {
                        'at':attr,
                        'min': config.POST_BILD_SET_MIN_MAX_ATTR[name][0],
                        'max': config.POST_BILD_SET_MIN_MAX_ATTR[name][1],
                    }
                    eval("pm.transformLimits(ob, %(at)s = (%(min)s, %(max)s), e%(at)s = (1, 1))" % ldata)
                except Exception as e:
                    print("in POST_BILD_SET_MIN_MAX_ATTR (exception1): %s" % e)

    # генерация Сурф контролов из файла
    # if with_surf:
    #     utils.make_surf_ctrls_from_file_data(config)
    
                
    # # позиционирование остальных контролов(глаза, веки) по данным из файла
    # pm.select(['neutralPose_CTRL_eye_L', 'neutralPose_CTRL_eye_R'], r=True)
    # pm.group(n='root_eyes')
    
    if hasattr(config, "OTHER_CTRL_DATA_JSON"):
        path = os.path.join(utils.get_path('USER_WORKDIR'), config.LOCAL_DIR, config.OTHER_CTRL_DATA_JSON)
        if os.path.exists(path):
            with open(path, 'r') as f:
                data = json.load(f)
            if data:
                for name in data:
                    if not pm.objExists(name):
                        continue
                    ob = pm.PyNode(name)
                    for i in [('tx',0),('ty',1),('tz',2),('rx',0),('ry',1),('rz',2),('sx',0),('sy',1),('sz',2)]:
                        try:
                            exec('ob.%s.set(data[name]["%s"][%s])' % (i[0], i[0][0], str(i[1])))
                        except:
                            pass
    
    # POST_BILD_REMOVED_OBJECTS
    if hasattr(config, 'POST_BILD_REMOVED_OBJECTS'):
        for name in config.POST_BILD_REMOVED_OBJECTS:
            if pm.objExists(name):
                pm.delete(name)

    # POST_BILD_SET_ATTR
    if hasattr(config, 'POST_BILD_SET_ATTR'):
        for name, data in config.POST_BILD_SET_ATTR.items():
            if pm.objExists(name):
                ob=pm.PyNode(name)
                exec('ob.%s.set(%s)' % data)


def remove_face_rig(config, confirm=False):
    """Удаление лицевого рига из рига тела. """
    
    pass
    # confirm
    if confirm:
        ask = pm.confirmDialog( title='Confirm', message='Are you sure?', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
        if ask == 'No':
            return
    
    # remove objects
    for ob_name in config.APPLY_REMOVE_OBJECTS:
        if pm.objExists(ob_name):
            try:
                print('*** delete: %s' % ob_name)
                pm.delete(ob_name)
            except Exception as e:
                print(e)
                continue

    # remove blendshapes
    name = config.APPLY_BS_PREFIX
    for ob in pm.ls(type='blendShape'):
        if ob.name().startswith(name):
            print(ob.name())
            try:
                print('delete: %s' % ob.name())
                pm.delete(ob)
            except:
                continue
        else:
            print('not found: %s' % ob.name())

    mel.eval ('MLdeleteUnused;')

def apply_face_rig_from_file(config, file_path):
    """Импорт из файла и подключение лицевого рига в риг тела. """
    
    # remove
    remove_face_rig(config, confirm=False)
    
    # import file
    if not file_path:
        #work = utils.get_path('USER_WORKDIR')
        #file_path = os.path.join(os.path.dirname(os.path.dirname(work)), 'rigs/face/%s' % config.FACE_RIG_TO_BODY_FILE)
        base = os.environ['JOB']
        file_path = os.path.join(base, 'creatures/rigs/face/%s' % config.FACE_RIG_TO_BODY_FILE)
        if not os.path.exists(file_path):
            pm.confirmDialog(m='No exists file: "%s"' % file_path)
            return
    pm.importFile(file_path)
    
    #make BS
    for key in config.APPLY_CONNECT_DATA:
        if pm.objExists(key[0]) and pm.objExists(key[1]):
            print('### %s' % key[0])
            source = pm.PyNode(key[0])
            target = pm.PyNode(key[1])
            skin_clusters = pm.listConnections(target.getShape(), t='skinCluster', s=1)
            pm.select(source, r=True)
            pm.select(target, add=True)
            bs = pm.blendShape(name = config.APPLY_BS_PREFIX, tc = False)[0]
            exec('bs.%s.set(1)' % key[0])
            #skin_clusters = pm.listConnections(target.getShape(), t='skinCluster', s=1)
            if skin_clusters:
                pm.reorderDeformers(skin_clusters[0].name(), bs.name(), target)
            else:
                print('*** Not Skincluster in %s' % target.getShape().name())
        else:
            print('*** Not Found %s or %s' % (key[0], key[1]))

    # parent
    if hasattr(config, APPLY_PARENT_LIST):
        for key in config.APPLY_PARENT_LIST:
            if pm.objExists(key[0]) and pm.objExists(key[1]):
                pm.parent(key[0], key[1])
            
    # parent constaraints
    if hasattr(config, APPLY_PARENT_CONSTRAINT_LIST):
        for key in config.APPLY_PARENT_CONSTRAINT_LIST:
            #if pm.objExists(key[0]) and pm.objExists(key[1]):
                if isinstance(key[0], str):
                    pm.parentConstraint(key[0], key[1], mo=True, name='%s_parentConstraint' % key[1].split('|')[-1])
                else:
                    com = 'pm.parentConstraint('
                    for item in key[0]:
                        com = '%s "%s",' % (com, item)
                    com = com + '"%s", mo=True, name="%s_parentConstraint")' % (key[1], key[1].split('|')[-1])
                    eval(com)
            
    # connect attributes
    if hasattr(config, APPLY_CONNECT_ATTR_DATA):
        for item in config.APPLY_CONNECT_ATTR_DATA:
            utils.add_attr(item[0].split('.')[0], item[0].split('.')[1])
            source = pm.PyNode(item[0])
            target = pm.PyNode(item[1])
            
            pm.setAttr(target, lock=0)
            source >> target
            
    utils.make_eye_ik_spaces(config)

    # добавление локаторов отслеживающих положение глаз в пространстве.
    # def set_eye_drivers():
    if hasattr(config, 'APPLY_HEAD_BONE'):
        groups=list()
        root_grp=pm.group(n='root_eye_drivers_grp', empty=True)
        for driver in [config.EYE_LOOK_DRIVER_R, config.EYE_LOOK_DRIVER_L]:
            # make
            loc_name = '%s_loc' % driver
            loc = pm.spaceLocator(name=loc_name)
            grp_name = '%s_grp' % loc_name
            grp=pm.group(n=grp_name)
            groups.append(grp)
            # position
            pct=pm.parentConstraint(driver, grp)
            pm.delete(pct)
            # lock attributes
            pm.setAttr(loc.t, lock=True)
            pm.setAttr(grp.t, lock=True)
            pm.setAttr(grp.r, lock=True)
            pm.setAttr(grp.s, lock=True)
            # connect attrs
            drv = pm.PyNode(driver)
            drv.r >> loc.r
        pm.parent(groups, root_grp)
        pm.parent(root_grp, config.APPLY_HEAD_BONE)
        pm.setAttr(root_grp.t, lock=True)
        pm.setAttr(root_grp.r, lock=True)
        pm.setAttr(root_grp.s, lock=True)
    
    # if hasattr(config, APPLY_HEAD_BONE):
    #     set_eye_drivers()
    
    pm.confirmDialog(m='The Facial rig added!', title='Facial Simple',)
    
    
    print('*** apply face rig from %s' % file_path)


def _to_float(stroke):
    """
    Превращение строки в вещественное число, замена запятой на точку
    """
    return float(stroke.replace(',', '.'))


def control_emulate(config, delta_z=True):
    """
    Создание элементов управления при отсутствии рига, запуск в задаче блендшейпов.
    
    * Эмуляция управления поворотами глаз при отсутствии объектов в config.LOOK
    
    .. note:: Требуется наличие config.EYE_GEO
    """
    print('-'*100)
    print('EMULATE')
    print(config.LOOK)

    GROUP = "EYE_EMULATE"
    ROOT_JOINT = "root_jnt"

    try: 
        if hasattr(config, "LOOK") and hasattr(config, "EYE_GEO"):
            eye_roots = dict(l_eye=[], r_eye=[])
            eye_objects = [mc.objExists(ROOT_JOINT)]
            for key,item in config.LOOK.items():
                eye_objects.append(mc.objExists(item["object"]))
                if not mc.objExists(item["object"]):
                    if '_l' in item["object"].lower():
                        item["side"]='l'
                        item["direction"]=key.split('_')[0]
                        eye_roots['l_eye'].append(item)
                    elif '_r' in item["object"].lower():
                        item["side"]='r'
                        item["direction"]=key.split('_')[0]
                        eye_roots['r_eye'].append(item)

            if all(eye_objects):
                return
            
            # -- 
            if not mc.objExists(GROUP):
                root_group = mc.group(name=GROUP, empty=True)
            else:
                root_group = GROUP
            # --
            mc.select(d=True)
            if not mc.objExists(ROOT_JOINT):
                root_joint = mc.joint(name=ROOT_JOINT, p=(0,0,0))
            else:
                root_joint = ROOT_JOINT
            mc.parent(root_joint, root_group)

            for key,item in eye_roots.items():
                root_name = "%s_root" % item[0]["object"]
                ctl_name = "%s_control" % item[0]["object"]
                obj_name = item[0]["object"]
                cluster_name = "%s_cluster" % item[0]["object"]
                joint_name = "%s_def_joint" % item[0]["object"]
                # cluster
                mc.select(config.EYE_GEO[key])
                cluster = mc.cluster(name = cluster_name)
                joint = mc.joint(name = joint_name)
                mc.select(cluster)
                mc.select(joint, add=True)
                pc = mc.pointConstraint()
                mc.delete(pc)
                # -- delta Z
                if delta_z:
                    bb = mc.exactWorldBoundingBox(config.EYE_GEO[key])
                    z = bb[5] - bb[2]
                    x = bb[3] - bb[0]
                    d = z - x
                    mc.move(0, 0, -d/2, config.EYE_GEO[key], ws=True, r=True)
                #-- end
                mc.parent(joint, root_joint)
                # --
                mc.skinCluster(config.EYE_GEO[key], joint)
                # --
                if not mc.objExists(obj_name):
                    # obj
                    obj = mc.spaceLocator(name=obj_name)[0]
                    mc.parent(obj, root_group)
                    mc.select(joint)
                    mc.select(obj, add=True)
                    pc = mc.pointConstraint()
                    mc.delete(pc)

                    # ctl
                    ctl = mc.spaceLocator(name=ctl_name)[0]
                    mc.select(ctl)
                    root = mc.group(name=root_name)
                    mc.select(obj)
                    mc.select(root, add=True)
                    pc = mc.pointConstraint()
                    mc.delete(pc)
                    mc.move(0,0,20, root, r=True)

                    mc.parent(root, root_group)

                    print('+'*100)
                    for it in item:
                        print(it)
                        mc.setAttr('%s.%s' % (obj, it['attr']), _to_float(it['zero']))

                        lim=2

                        if it["direction"]=='up':
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'ty', 0, _to_float(it['zero']))
                            print(com)
                            eval(com)
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'ty', lim, _to_float(it['value']))
                            print(com)
                            eval(com)
                        if it["direction"]=='down':
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'ty', 0, _to_float(it['zero']))
                            print(com)
                            eval(com)
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'ty', -lim, _to_float(it['value']))
                            print(com)
                            eval(com)
                        if it["direction"]=='in':
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'tx', 0, _to_float(it['zero']))
                            print(com)
                            eval(com)
                            if it["side"] == 'r':
                                v = lim
                            else:
                                v = -lim
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'tx', v, _to_float(it['value']))
                            print(com)
                            eval(com)
                        if it["direction"]=='out':
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'tx', 0, _to_float(it['zero']))
                            print(com)
                            eval(com)
                            if it["side"] == 'r':
                                v = -lim
                            else:
                                v = lim
                            com = 'pm.setDrivenKeyframe(\'%s.%s\', cd=\'%s.%s\', dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                            obj, it["attr"], ctl, 'tx', v, _to_float(it['value']))
                            print(com)
                            eval(com)

                    mc.select(obj)
                    mc.select(joint, add=True)
                    mc.parentConstraint(mo=True)
                    mc.delete(cluster)
    except:
        print('~>'*100)
        print(traceback.format_exc())
        print('<~'*100)


def create_inverted(config, blends=["jawOpen", "jawLeft", "jawRight", "jawForward"]):
    """
    Создание инверт форм.
    
    TODO: Привязать к конфигу - вызывать в :func:`pre_bild`
    """
    #mesh_root=cmds.ls(sl=True)[0]
        
    def _create(orig_mesh, config, blends):
        """Doc """
        for b in blends:
            if cmds.objExists("%s_inverted" % b):
                continue
            parent=cmds.listRelatives(b, parent=True)[0]
            if not b in config.JD:
                cmds.warning(u"Нет данных по %s" % b)
                continue
            cmds.setAttr("%s.%s" % (config.JD[b]["object"], config.JD[b]["attr"]), config.JD[b]["value"])
            
            cmds.select(orig_mesh, r=True)
            cmds.select(b, add=True)
            invert_mesh=cvshapeinverter.invert()
            cmds.parent(invert_mesh, parent)
            
            cmds.setAttr("%s.%s" % (config.JD[b]["object"], config.JD[b]["attr"]), 0)
            
    # window
    window = pm.window(t='Import data from Incoming tasks:')
    # layout
    layout = pm.columnLayout(adj=True)
    
    for k in config.MESHES.keys():
        b=pm.button(l=k)
        b.setCommand(pm.Callback(_create, k, config, blends))
    pm.setParent('..')
    window.show()