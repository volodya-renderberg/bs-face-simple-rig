# -*- coding: utf-8 -*-

"""
Методы для применения анимации LiveLinks
"""


def get_csv(csv_path):
    """
    Читает файл по переданному пути, возвращает словарь: ключи - коды лайвлинка, значения - список значений этого кода

    Parameters
    -----------
    csv_path : str
        path

    Returns
    --------
    dict
        {code: [values], ...}
    """


def bake_animation(name_space: str,
                   codes: dict,
                   config,
                   start_frame=1) -> None:
    """
    Запекает лайвлинк анимацию на лицевые контролы персонажа, учитывая его конфиг.

    Args:
        name_space (str): нейм спейс персонажа
        codes (dict): результат выполнения метода :func:`get_csv`
        config: модуль config текущего персонажа.
        start_frame (int): кадр с которога начинается запекание анимации.
    Returns:
        None: ничего
    """
