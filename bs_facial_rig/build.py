#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Сборка рига.
"""

import traceback
import os
import json
import logging
import uuid

try:
    from importlib import reload
except:
    pass

import pymel.core as pm
import maya.cmds as mc

import facial_utils as utils, hook
try: # for: make html
    reload(utils)
    reload(hook)
except:
    pass

# def __make_dependens(key, current_dict, bs):
def _make_dependens(config, key, current_dict, bs):
    def _temp_empty():
        """
        Returns
        -------
        PyNode
        """
        TMP_EMPTY = "e_%s" % str(uuid.uuid4()).split('-')[0]
        if mc.objExists(TMP_EMPTY):
            mc.delete(TMP_EMPTY)
        tmp_empty = pm.group(n=TMP_EMPTY, em=True)

        return tmp_empty

    def make_driver(ob, attr, driver, driver_attr, values):
        """
        Parameters
        ----------
        ob: str
            имя управляемого объекта.
        attr: str
            имя атрибута управляемого объекта.
        driver: PyNode
            имя управляющего объекта
        driver_attr: str
            имя управляющего атрибута
        values: dict
            ключи - строковое представление абцисс, значения - ордината для ``keyframe``.
        """
        try:
            utils.add_attr(driver, driver_attr)  # добавление атрибута на контрол в случае его отсутствия.
            for key_ in values:
                value_ = str(values[key_])
                if isinstance(key_, str):
                    key_ = float(key_.replace(',', '.'))
                com = 'pm.setDrivenKeyframe(ob.%s, cd=driver.%s, dv = %s, v = %s, itt = \'linear\', ott = \'linear\')' % (
                attr, driver_attr, key_, value_)
                try:
                    eval(com)
                except Exception as e:
                    print(key, e, com)
        except:
            print(traceback.format_exc())

    def make_multiply(ob, attr, blocks):
        """
        Parameters
        ----------
        ob: str
            имя управляемого объекта.
        attr: str
            имя атрибута управляемого объекта.
        blocks: tuple
            (driver_name, driver_attr_name, {'value': value, ...})
        """
        try:
            asset=pm.PyNode(config.FACE_MATH_NODE)
            m_nodes=[]

            if len(blocks) == 1:
                driver = pm.PyNode(blocks[0][0])
                driver_attr = blocks[0][1]
                data = blocks[0][2]
                make_driver(ob, attr, driver, driver_attr, data)
            if len(blocks) > 1:
                # inputs = ['input1X', 'input2X']
                input1 = False
                for i in range(1, len(blocks)):
                    mp_name = '%s_multiplyDivide' % key
                    mp = pm.shadingNode('multiplyDivide', n=mp_name, au=True)
                    m_nodes.append(mp)
                    # input 1
                    if i == 1:
                        driver = pm.PyNode(blocks[i - 1][0])
                        driver_attr = blocks[i - 1][1]
                        data = blocks[i - 1][2]
                        make_driver(mp, 'input1X', driver, driver_attr, data)
                    else:
                        input1 >> mp.input1X
                    # input 2
                    driver = pm.PyNode(blocks[i][0])
                    driver_attr = blocks[i][1]
                    data = blocks[i][2]
                    make_driver(mp, 'input2X', driver, driver_attr, data)

                    # fin
                    input1 = mp.outputX

                # to bs
                eval('input1 >> ob.%s' % attr)
            # multiply nodes to asset
            pm.container(asset, e=True, an=m_nodes)
        except:
            print(traceback.format_exc())

    def make_min(ob, attr, blocks):
        """
        Минимальное из ряда значений. Используется для центрального бленда при тройной схеме: Left:Middle:Right

        Parameters
        ----------
        ob: str
            имя управляемого объекта.
        attr: str
            имя атрибута управляемого объекта.
        blocks: tuple list
            ((driver_name, driver_attr_name, {'value': value, ...}),...)
        """
        try:
            asset=pm.PyNode(config.FACE_MATH_NODE)
            m_nodes=[]

            tmp_empty = _temp_empty()

            # -- to driver
            if len(blocks) == 1:
                driver = pm.PyNode(blocks[0][0])
                driver_attr = blocks[0][1]
                data = blocks[0][2]
                make_driver(ob, attr, driver, driver_attr, data)
            elif len(blocks) > 1:
                input1 = False
                for i in range(1, len(blocks)):
                    com = [
                        'float $l=%s.tx;' % tmp_empty.name(),
                        'float $r=%s.ty;' % tmp_empty.name(),
                        'float $d=0;',
                        'if ($l<=$r) $d=$l;',
                        'else if ($r<=$l) $d=$r;',
                        'current_target = $d;'
                    ]
                    # mp_name = '%s_multiplyDivide' % key
                    # mp = pm.shadingNode('multiplyDivide', n=mp_name, au=True)
                    mp = _make_expression(com, output_attr_name = "%s.tz" % tmp_empty.name())
                    m_nodes.append(mp)
                    # input 1
                    if i == 1:
                        driver = pm.PyNode(blocks[i - 1][0])
                        driver_attr = blocks[i - 1][1]
                        data = blocks[i - 1][2]
                        tmp_empty.tx // mp.input[0]
                        make_driver(mp, 'input[0]', driver, driver_attr, data)
                    else:
                        tmp_empty.tx // mp.input[0]
                        input1 >> mp.input[0]
                    # input 2
                    driver = pm.PyNode(blocks[i][0])
                    driver_attr = blocks[i][1]
                    data = blocks[i][2]
                    tmp_empty.ty // mp.input[1]
                    make_driver(mp, 'input[1]', driver, driver_attr, data)

                    # fin
                    input1 = mp.output[0]

                # to bs
                eval('input1 >> ob.%s' % attr)
            
            # -- finish
            pm.container(asset, e=True, an=m_nodes)
            # -- -- clear empty
            if mc.objExists(tmp_empty.name()):
                mc.delete(tmp_empty.name())
        except:
            print(traceback.format_exc())
            # raise Exception("ERROR!!!!!")

    def make_side(ob, attr, blocks):
        """
        Подключение одной из сторон при тройной схеме: Left:Middle:Right
        """
        try:
            asset=pm.PyNode(config.FACE_MATH_NODE)
            m_nodes=[]

            tmp_empty = _temp_empty()

            this_attr = "%s.%s" % (blocks["this"][0], blocks["this"][1])
            another_attr = "%s.%s" % (blocks["another"][0], blocks["another"][1])

            com = [
                'float $this=%s.tx;' % tmp_empty.name(),
                'float $other=%s.ty;' % tmp_empty.name(),
                'float $d=0;',
                'if ($this>$other) $d=$this-$other;',
                'else $d=0;',
                'current_target = $d',
            ]
            mp = _make_expression(com, output_attr_name = "%s.%s" % (ob, attr))
            m_nodes.append(mp)

            # -- input this
            driver = pm.PyNode(blocks["this"][0])
            driver_attr = blocks["this"][1]
            data = blocks["this"][2]
            tmp_empty.tx // mp.input[0]
            make_driver(mp, 'input[0]', driver, driver_attr, data)

            # -- input another
            driver = pm.PyNode(blocks["another"][0])
            driver_attr = blocks["another"][1]
            data = blocks["another"][2]
            tmp_empty.ty // mp.input[1]
            make_driver(mp, 'input[1]', driver, driver_attr, data)

            # -- finish
            pm.container(asset, e=True, an=m_nodes)

            # -- -- clear empty
            if mc.objExists(tmp_empty.name()):
                mc.delete(tmp_empty.name())
        except:
            print(traceback.format_exc())
            raise Exception("ERROR!!!!!")

    def _make_expression(strings, output_attr_name=None):
        """
        Создание ноды Expression из строк

        Parameters
        ----------
        strings: list str optional
            список строк из которых формируется нода.
        output_attr_name : str optional
            атрибут назначения результата экспрешена

        Return
        -------
        pymel.core.PyNode
        """
        print('>'*100)
        # -- com
        
        if isinstance(strings, list) or isinstance(strings, tuple):
            com = ''
            for string in strings:
                com = com + '\n' + string
        else:
            com = strings
        
        if output_attr_name:
            com = com.replace('current_target', output_attr_name)

        # -- node
        if not output_attr_name:
            output_attr_name = str(uuid.uuid4()).split('-')[0]
        node = pm.createNode('expression', n='%s_expression' % output_attr_name.replace('.', '_'))
        node.setExpression(com)

        # -- end
        return node

    def make_expression(config, ob, attr, block):
        """
        Parameters
        -------------
        ob: str
            имя управляемого объекта.
        attr: str
            имя атрибута управляемого объекта.
        block: dict
            содержит ключ 'strings' - в котором все строки экспершена.
        """
        try:
            asset = pm.PyNode(config.FACE_MATH_NODE)
            m_nodes = []

            if isinstance(ob, str):
                ob = pm.PyNode(ob)
            # com = ''
            # for string in block['strings']:
            #     com = com + '\n' + string
            # com = com.replace('current_target', '%s.%s' % (ob.name(), attr))
            # # expression
            # node = pm.createNode('expression', n='%s_%s_expression' % (ob.name(), attr))
            # print(com)
            # node.setExpression(com)
            node = _make_expression(block['strings'], output_attr_name='%s.%s' % (ob.name(), attr))
            m_nodes.append(node)

            pm.container(asset, e=True, an=m_nodes)
        except:
            print(traceback.format_exc())

    if not current_dict[key]:
        return
    if current_dict[key][0] == 'driver':
        driver = pm.PyNode(current_dict[key][1][0])
        driver_attr = current_dict[key][1][1]
        data = current_dict[key][1][2]
        make_driver(bs, key, driver, driver_attr, data)

    elif current_dict[key][0] == 'multiply':
        make_multiply(bs, key, current_dict[key][1])
    elif current_dict[key][0] == 'min':
        make_min(bs, key, current_dict[key][1])
    elif current_dict[key][0] == 'side':
        make_side(bs, key, current_dict[key][1])
    elif current_dict[key][0] == 'switch':
        # make switcher ob
        sw_name = '%s_switch' % key
        sw = pm.shadingNode('blendTwoAttr', n=sw_name, au=True)
        driver = pm.PyNode(current_dict[key][1]['switcher'][0])
        driver_attr = current_dict[key][1]['switcher'][1]
        values = current_dict[key][1]['switcher'][2]
        make_driver(sw, 'attributesBlender', driver, driver_attr, values)
        eval('sw.output >> bs.%s' % key)

        # input 1
        _make_dependens(config, 'input[0]', current_dict[key][1], sw)

        # input 2
        _make_dependens(config, 'input[1]', current_dict[key][1], sw)
    elif current_dict[key][0] == 'expression':
        make_expression(config, bs, key, current_dict[key][1])
    elif current_dict[key][0] == 'connect':
        if pm.objExists(current_dict[key][1]):
            inOb = pm.PyNode(current_dict[key][1])
            eval('inOb.%s >> bs.%s' % (current_dict[key][2], key))


def _create_math_asset_node(config):
    """Создание ассета для математических нод"""
    if pm.objExists(config.FACE_MATH_NODE):
        pm.delete(lists.FACE_MATH_NODE)
    parent_asset = pm.createNode('container', n=config.FACE_MATH_NODE)


def _make_drivers(config):
    """
    Создаёт пустую группу c атрибутами по количеству блендшейпов ( :attr:`config.DRIVER_CONFIG` ) и управление на них.

    :param config:
    :return:
    """

    # 1 - создать пустую группу
    # 2 - создание атрибутов на action_ob
    # 3 - создание зависимостей

    # (0)
    _create_math_asset_node(config)

    # (1)
    if pm.objExists(config.DRIVING_OB):
        pm.delete(config.DRIVING_OB)
    action_ob = pm.group(empty=True, name=config.DRIVING_OB)

    # (2)
    for name in config.DRIVER_CONFIG:
        utils.add_attr(action_ob, name)

    # (3)
    for name in sorted(config.DRIVER_CONFIG.keys()):
        pass
        _make_dependens(config, name, config.DRIVER_CONFIG, action_ob)

    return action_ob


def import_template_of_facial_controls(config, local_ctrl=True, clean=True):
    """Импорт темплейта контролов из face_meta"""
    ctrl_name = config.CTRL_ROOT_OB
    ctrl_file = config.CTRL_FILE
    # Clean
    if clean:
        clear_driving_blend_shaps(config, ctrls=True, surf=True)
    # CTRL
    if pm.objExists(ctrl_name):
        pm.warning("%s allready Exists" % ctrl_name)
        return (False)
    if local_ctrl:
        if utils.get_path('FACE_META', config=config):
            ctrl_path = os.path.join(utils.get_path('FACE_META', config=config), ctrl_file)
            if not os.path.exists(ctrl_path):
                print('#' * 5)
                print('Not exists of local path: %s' % ctrl_path)
                ctrl_path = os.path.join(os.path.dirname(__file__), ctrl_file)
        else:
            ctrl_path = os.path.join(os.path.dirname(__file__), ctrl_file)
    else:
        ctrl_path = os.path.join(os.path.dirname(__file__), ctrl_file)

    try:
        pm.importFile(ctrl_path)
    except:
        print(traceback.format_exc())
    return (True)


def clear_driving_blend_shaps(config, ctrls=False, surf=False):
    """Удаление управления бленд шейпами.
    
    Parameters
    ----------
    config : object
        config
    ctrls : bool optional
        Удалять или нет Рамку блендшейп контролов
    surf : bool optional
        Удалять или нет сурфейс контролы
    """
    if pm.objExists(config.FACE_MATH_NODE):
        pm.delete(config.FACE_MATH_NODE)
    if pm.objExists(config.DRIVING_OB):
        pm.delete(config.DRIVING_OB)
    
    if ctrls:
        if hasattr(config, "CTRL_ROOT_OB") and mc.objExists(config.CTRL_ROOT_OB):
            mc.delete(config.CTRL_ROOT_OB)

    if surf:
        for ob in [os.environ['SURF_ROOT'], os.environ['HIDDEN_GRP']]:
            if mc.objExists(ob):
                mc.delete(ob)

def connect_blend_shapes(config):
    """Создание управления от контролов на блендшейпы используя ``config``

    * контролы присутствуют, блендшейпы все созданы - только управление: создание или пересоздание.

    * создаёт для всех сеток из :attr:`config.MESHES`
    """

    # (0) Очистка управления
    clear_driving_blend_shaps(config)

    # (0_1) генерация недостающего управления при запусках не в риге.
    hook.control_emulate(config)

    # (1) управление на промежуточный объект DRIVING_OB
    action_ob=_make_drivers(config)

    not_found=list()

    # (2) коннекты от driving ноды на блендшейпы
    for ob_name, bs_name in config.MESHES.items():
        if not pm.objExists(bs_name):
            continue
        bs = pm.PyNode(bs_name)

        for key in config.DRIVER_CONFIG:
            # пропуск если в бленд ноде нет такого ключа.
            targets = []
            for target in bs.weight:
                num = target.item()
                name = pm.aliasAttr(bs.weight[num], query=True)
                targets.append(name)
            if not key in targets:
                not_found.append(key)
                print("%s - not found" % key)
                continue

            # _make_dependens(config, key, config.DRIVER_CONFIG, bs)
            print('********** action_ob.%s >> bs.%s ***********' % (key, key))
            eval('action_ob.%s >> bs.%s' % (key, key))

        print('-'*100)
        print("*** NOT FOUND BLEND SHAPES IN: %s ***" % bs_name)
        for item in not_found:
            print(item)
        print('-'*100)

    # (3) make ctrl set
    utils.ctrls_set(config.FACIAL_MOCAP_RIG_CTRLS)

    # (4) post_build
    hook.post_bild(config)

    # (4) finish
    pm.confirmDialog(m=config.ALGORITHM)

