#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Описание содержимого файла facial_utils.py """

import os
import sys
import json
import traceback
import logging
import shutil
import uuid

import pymel.core as pm
import maya.OpenMaya as om
import maya.cmds as mc

import pipeline_connect, hash_test, settings

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

def face_meta_to_local_dir(task_info):
    """
    Импортирует папку ``face_meta`` из директории персонажа в локал директорию текущей задачи.
    Требуется открытие задачи через тентакулу.
    """
    try:
        # --
        global_meta_path = pipeline_connect.get_global_face_meta()
        local_meta_path = pipeline_connect.get_local_face_meta()
        if os.path.samefile(global_meta_path, local_meta_path):
            return
        if os.path.exists(global_meta_path) and not os.path.exists(local_meta_path):
            shutil.copytree(global_meta_path, local_meta_path)
        elif os.path.exists(global_meta_path) and os.path.exists(local_meta_path):
            # print('@'*500)
            global_config=os.path.join(global_meta_path, 'config.py')
            local_config=os.path.join(local_meta_path, 'config.py')
            test = hash_test.match_md5(global_config, local_config)
            # print(test)
            if not test:
                try:
                    shutil.rmtree(local_meta_path)
                except:
                    num = str(uuid.uuid4()).split('-')[0]
                    shutil.move(local_meta_path, os.path.join(os.path.dirname(local_meta_path), '~face_meta_%s_to_delete' % num))
                shutil.copytree(global_meta_path, local_meta_path)
        if not local_meta_path in sys.path:
            sys.path.insert(0, os.path.dirname(local_meta_path))
            sys.path.append(os.path.dirname(local_meta_path))
        # # -- asset
        # asset_info = cerebro.task_info_by_task_id(task_info["task_parent_id"])
        # # -- path
        # asset_fvers=cerebro.task_versions_paths(asset_info)
        # task_fvers=cerebro.task_versions_paths(task_info)
        # if asset_fvers and task_fvers:
        #     # -- clean ~
        #     utils.clearing_temp_files(os.path.dirname(task_fvers.local_path()))
        #     #
        #     global_meta_path=os.path.join(os.path.dirname(asset_fvers.next_version_path()), "face_meta")
        #     logger.info(global_meta_path)
        #     local_meta_path=os.path.join(os.path.dirname(task_fvers.local_path()), "face_meta")
        #     logger.info(local_meta_path)
        #     if os.path.exists(global_meta_path) and not os.path.exists(local_meta_path):
        #         shutil.copytree(global_meta_path, local_meta_path)
        #     # test fo different config.py file
        #     elif os.path.exists(global_meta_path) and os.path.exists(local_meta_path):
        #         print('@'*500)
        #         global_config=os.path.join(global_meta_path, 'config.py')
        #         local_config=os.path.join(local_meta_path, 'config.py')
        #         test = hash_test.match_md5(global_config, local_config)
        #         print(test)
        #         if not test:
        #             try:
        #                 shutil.rmtree(local_meta_path)
        #             except:
        #                 num = str(uuid.uuid4()).split('-')[0]
        #                 shutil.move(local_meta_path, os.path.join(os.path.dirname(local_meta_path), '~face_meta_%s_to_delete' % num))
        #             shutil.copytree(global_meta_path, local_meta_path)
        #     if not local_meta_path in sys.path:
        #         sys.path.insert(0, os.path.dirname(local_meta_path))
        #         sys.path.append(os.path.dirname(local_meta_path))
    except:
        logger.error(traceback.format_exc())
        mc.confirmDialog(m=traceback.format_exc(), icn="warning")


def create_expression(strings, name='bs_face_simple_expression'):
    """
    Создаёт ноду ``expression``

    Parameters
    ----------
    strings: list
        Список строк экспрешена (заканчиваются символом ``;``).
    name : str, optional
        Имя создаваемой ноды.

    Returns
    -------
    expression
        Экспрешен.
    """
    # (1)
    com='//'
    for string in strings:
        com = com + '\n' + string
    # (2) expression
    node = pm.createNode('expression', n = name)
    node.setExpression(com)
    return node


def get_path(template, config=None):
    """Определние путей по шаблонам.

    .. attention:: Данную процедуру надо редактировать под студийный пайплайн.

    Parameters
    ----------
    template : str
        Шаблон, значение из [``USER_WORKDIR`` , ``FACE_META``]

    Returns
    -------
    None, str
        Шаблонный путь или *None*.
    """

    # CGF_TEMPLATES = {
    #     'FACE_META': 'creatures/rigs/face/bs_face_simple_local',
    #     'USER_WORKDIR': 'creatures/rigs/face',
    #     }
    # if not os.environ.get('JOB'):
    #     return None
    # #
    # base = os.environ['JOB']
    # return(os.path.join(base, TEMPLATES[template]))

    if os.environ.get('JOB'): # CGF
        CGF_TEMPLATES = {
            'FACE_META': 'creatures/rigs/face/bs_face_simple_local',
            'USER_WORKDIR': 'creatures/rigs/face',
            }
        return(pm.util.path.joinpath(os.environ.get('JOB'), CGF_TEMPLATES[template]))
    else: # MGF (поиск в ближайшем окружении)
        this_dir = pm.util.path.dirname(pm.sceneName())
        if template=="FACE_META":
            if config:
                return os.path.dirname(config.__file__)
            else:
                if not this_dir:
                    print("This file not saved!")
                    return
                if pm.util.path.dirs(this_dir, "*face_meta"):
                    return pm.util.path.joinpath(this_dir, "face_meta")
                else:
                    this_dir=pm.util.path.dirname(this_dir)
                    if pm.util.path.dirs(this_dir, "*face_meta"):
                        return pm.util.path.joinpath(this_dir, "face_meta")
                    else:
                        print("directory not found", template)
                        return
        elif template=="USER_WORKDIR":
            if not this_dir:
                print("This file not saved!")
                return
            return this_dir
        # if pm.util.path.dirs(this_dir, "*blends"):
        #     return this_dir
        # else:
        #     this_dir=pm.util.path.dirname(this_dir)
        #     if pm.util.path.dirs(this_dir, "*blends"):
        #         return this_dir
        #     else:
        #         raise Exception("directory not found", template)


if get_path('USER_WORKDIR'):
    sys.path.append(get_path('USER_WORKDIR'))


def connect_ctrls():
    """Для темплейта лицевых контролов, делает зависимые зеркальные трансформации рута \
    правого контрола по отношению к левому. Надо выбрать оба рута, в любом порядке(выбираем\
     контролы и стрелка вверх) и выполнить эту процедуру. \
     Имена рутов должны содержать на **_L_** и **_R_** соответственно."""
    objects = pm.ls(sl=True, type='transform')
    l_object = None
    r_object = None
    if len(objects) != 2:
        pm.warning('Select Blya!')
        return
    for ob in objects:
        if '_l_' in str(ob.name()).lower():
            l_object = ob
        if '_r_' in str(ob.name()).lower():
            r_object = ob
    l_object.ty >> r_object.ty
    l_object.tz >> r_object.tz
    l_object.rx >> r_object.rx

    l_object.sy >> r_object.sy
    l_object.sz >> r_object.sz
    l_object.sx >> r_object.sx

    #tx
    mp = pm.shadingNode('multiplyDivide', au=True)
    mp.input2X.set(-1)
    l_object.tx >> mp.input1X
    mp.outputX >> r_object.tx

    #ry
    mp = pm.shadingNode('multiplyDivide', au=True)
    mp.input2X.set(-1)
    l_object.ry >> mp.input1X
    mp.outputX >> r_object.ry

    #rz
    mp = pm.shadingNode('multiplyDivide', au=True)
    mp.input2X.set(-1)
    l_object.rz >> mp.input1X
    mp.outputX >> r_object.rz


def shape_invert(neutral, source_pose, deform_pose):
    """Создание корректа на сетку  под влиянием деформатора.

    Parameters
    ----------
    neutral : str, PyNode
        Нейтральная сетка.
    source_pose : str, PyNode
        Чистый блендшейп.
    deform_pose : str, PyNode
        Сетка под воздействием какого-либо деформатора, на которое состояние надо\
         сделать коррект форму.

    Returns
    -------
    PyNode
        Коррект форма
    """

    # 1 - str to PyNode
    # 2 - make correct

    # (1)
    if isinstance(neutral, str):
        neutral=pm.PyNode(neutral)
    if isinstance(source_pose, str):
        source_pose=pm.PyNode(source_pose)
    if isinstance(deform_pose, str):
        deform_pose=pm.PyNode(deform_pose)

    # (2)
    # --
    copy = pm.duplicate(deform_pose)[0]

    # --
    pm.select([neutral, source_pose], r=True)
    pm.select(copy, add=True)
    bs = pm.blendShape()[0]
    # --
    for item in [neutral.name(), source_pose.name()]:
        eval('bs.%s.set(%s)' % (item, 1))
    # --
    pm.delete(copy, ch=True)
    pm.rename(copy, "%s_inverted" % source_pose.name())

    return copy


def grp(ob, grp_name=False):
    if not grp_name:
        grp = pm.group(n = 'grp_%s' % ob.name(), empty=True)
    else:
        grp = pm.group(n = grp_name, empty=True)
    pm.select(ob, grp, r=True)
    pc = pm.parentConstraint(mo=0)
    pm.delete(pc)
    # parents
    parent_ob = pm.listRelatives(ob, parent=True)[0]
    if parent_ob:
        pm.parent(grp, parent_ob)
    pm.parent(ob, grp)
    return(grp)


def make_eye_ik_control(lists):
    for side in ['L','R']:
        driver_joint_name = getattr(lists, "EYE_LOOK_DRIVER_%s" % side)
        driver_joint = pm.PyNode(driver_joint_name)
        parent_ob = pm.listRelatives(driver_joint_name, p=True)[0]
        # IK
        ik_joint = pm.duplicate(driver_joint_name, n=getattr(lists, "EYE_LOOK_IK_DRIVER_%s" % side))[0]
        pm.parent(ik_joint, parent_ob)
        tail = pm.listRelatives(ik_joint, c=1)[0]
        pm.rename(tail, 'tail_%s' % ik_joint.name())
        grp(ik_joint, '%s_offset' % ik_joint.name())
        # FK
        fk_joint = pm.duplicate(driver_joint_name, n=getattr(lists, "EYE_LOOK_FK_DRIVER_%s" % side))[0]
        pm.parent(fk_joint, parent_ob)
        tail = pm.listRelatives(fk_joint, c=1)[0]
        pm.rename(tail, 'tail_%s' % fk_joint.name())
        # condition
        cond = pm.shadingNode('condition', n = 'eye_fk_ik_condition_%s' % side, au=True)
        ik_joint.rotate >> cond.colorIfFalse
        fk_joint.rotate >> cond.colorIfTrue
        cond.outColor.outColorR >> driver_joint.rx
        cond.outColor.outColorG >> driver_joint.ry
        cond.outColor.outColorB >> driver_joint.rz
        # middle to condition
        middle_ctrl = pm.PyNode(lists.IK_EYE_CTRL_M)
        middle_ctrl.ik >> cond.firstTerm
        # aim constraint
        pm.select(getattr(lists, 'IK_EYE_CTRL_%s' % side), ik_joint, r=True)
        pm.aimConstraint(o=[0,0,0], w=1, aim=[0,0,1], u=[0,1,0], wut='object', wuo=getattr(lists, 'UP_VECTOR_OBJECT_%s' % side))


def make_eye_ik_spaces(lists):
    # make eye_ik_world
    eye_ik_world = pm.spaceLocator(name = 'eye_ik_world')
    eye_ik_world.v.set(0)
    for attr_name in pm.listAttr(eye_ik_world, k=True):
        pm.setAttr('eye_ik_world.%s' % attr_name, lock=True)
    pm.parent(eye_ik_world, lists.RIG_NO_DEFORM_GRP)

    # # make transform
    # eye_ik_main = pm.spaceLocator(name = 'eye_ik_main')
    # eye_ik_transform.v.set(0)
    # for attr_name in pm.listAttr(eye_ik_main, k=True):
    #     pm.setAttr('eye_ik_main.%s' % attr_name, lock=True)
    # pm.parent(eye_ik_main, lists.BODY_RIG_MAIN_CTRL)

    head_ctrl = pm.PyNode(lists.BODY_RIG_HEAD_CTRL)

    # ik-fk attr create
    if not "eye_ik" in pm.listAttr(head_ctrl, k=True):
        pm.addAttr(head_ctrl, at='float', ln='eye_ik', dv=0, min=0, max=1)
        pm.setAttr('%s.eye_ik' % lists.BODY_RIG_HEAD_CTRL, keyable=True, channelBox=False)
    head_ctrl.eye_ik >> pm.PyNode(lists.IK_EYE_CTRL_M).ik
    pm.setAttr('%s.v' % lists.IK_EYE_CTRL_M, lock=False)
    head_ctrl.eye_ik >> pm.PyNode(lists.IK_EYE_CTRL_M).v

    # Enum attr create
    enum_values = 'main:pelvis:chest:world:'
    if not "eye_ik_spacing" in pm.listAttr(head_ctrl, k=True):
        pm.addAttr(head_ctrl, at='enum', ln='eye_ik_spacing', en=enum_values)
        pm.setAttr('%s.eye_ik_spacing' % lists.BODY_RIG_HEAD_CTRL, keyable=True, channelBox=False, lock=False)

    # Enum attributes connect
    head = pm.PyNode(lists.BODY_RIG_HEAD_CTRL)
    # -- unlock attrs
    eye_ik_root = pm.listRelatives(lists.IK_EYE_CTRL_M, p=True)[0]
    for attr_name in ('tx','ty','tz','rx','ry','rz'):
        pm.setAttr('%s.%s' % (eye_ik_root.name(), attr_name), lock=False)
    # -- parent constraint
    pm.select(lists.BODY_RIG_MAIN_CTRL, lists.BODY_RIG_PELVIS_OBJ, lists.BODY_RIG_CHEST_OBJ, eye_ik_world, eye_ik_root)
    prnt = pm.parentConstraint(mo=True)
    # -- main
    world = pm.shadingNode('condition', n = 'main_space_condition', au=True)
    world.secondTerm.set(0)
    head.eye_ik_spacing >> world.firstTerm
    world.colorIfTrueR.set(1)
    world.colorIfFalseR.set(0)
    world.outColorR >> prnt.w0
    # -- pelvis
    pelvis = pm.shadingNode('condition', n = 'pelvis_space_condition', au=True)
    pelvis.secondTerm.set(1)
    head.eye_ik_spacing >> pelvis.firstTerm
    pelvis.colorIfTrueR.set(1)
    pelvis.colorIfFalseR.set(0)
    pelvis.outColorR >> prnt.w1
    # -- chest
    chest = pm.shadingNode('condition', n = 'chest_space_condition', au=True)
    chest.secondTerm.set(2)
    head.eye_ik_spacing >> chest.firstTerm
    chest.colorIfTrueR.set(1)
    chest.colorIfFalseR.set(0)
    chest.outColorR >> prnt.w2
    # -- world
    world_g = pm.shadingNode('condition', n = 'world_space_condition', au=True)
    world_g.secondTerm.set(3)
    head.eye_ik_spacing >> world_g.firstTerm
    world_g.colorIfTrueR.set(1)
    world_g.colorIfFalseR.set(0)
    world_g.outColorR >> prnt.w3


def add_attr(ob, attr, min_value=-1.0, max_value=1.0):
    shotNames = [
        'tx',
        'ty',
        'tz',
        'rx',
        'ry',
        'rz',
        'sx',
        'sy',
        'sz',
    ]
    if attr in shotNames:
        return
    list_attr = pm.listAttr(ob)
    if not attr in list_attr:
        pm.addAttr(ob, ln=attr, min=min_value, max=max_value, k=True)


def _get_uv_of_selected_vertex():
    """
    Получает кортеж (u, v, mesh_name, index) для выделенной точки на сетке

    Returns
    -------
    tuple
        (u: float, v: float, mesh_name: str, vert_index: int)
    """
    vertex = pm.ls(sl=1)[0]
    vindex = vertex.index()
    map_vertex = pm.polyListComponentConversion(vertex, fromVertex=True, toUV=True)[0]
    map_index = pm.PyNode(map_vertex).index()

    mesh_shape_name = vertex.name().split('.')[0]
    mesh_name = pm.listRelatives(mesh_shape_name, parent=1)[0].name()

    List = om.MSelectionList()
    List.add(mesh_name)

    ListIter = om.MItSelectionList(List, om.MFn.kMesh)
    pathToShape = om.MDagPath()
    ListIter.getDagPath(pathToShape)
    #meshNode = pathToShape.fullPathName()

    mMesh = om.MFnMesh(pathToShape.node())

    uUtil = om.MScriptUtil()
    vUtil = om.MScriptUtil()

    uPtr = uUtil.asFloatPtr()
    vPtr = vUtil.asFloatPtr()

    mMesh.getUV(map_index, uPtr, vPtr)

    u = uUtil.getFloat(uPtr)
    v = vUtil.getFloat(vPtr)

    return u,v,mesh_name, vindex


def _ob_to_hiden_group(*objects, **kwargs):
    """
    Парент объектов в группу для скрытых объектов

    Parameters
    ----------
    objects : str[]
        имя или имена скрываемого объекта
    kwargs : dict
        ожидает параметры:
        * "lists" - объект содержащий параметр имени группы;
        * "group" - имя группы в которую перекладывается объект
    """
    # -- get name of group
    if "lists" in kwargs and hasattr(kwargs["lists"], "HIDDEN_GROUP_OF_SURF"):
        name_of_hiden_grp = kwargs["lists"].HIDDEN_GROUP_OF_SURF
    elif "group" in kwargs:
        name_of_hiden_grp = kwargs["group"]
    else:
        name_of_hiden_grp = os.environ['HIDDEN_GRP']

    # -- ob to group
    for ob in objects:
        if pm.objExists(name_of_hiden_grp):
            pm.parent(ob, name_of_hiden_grp)
        else:
            pm.select(ob, r=True)
            pm.group(n = name_of_hiden_grp)


def _create_follicle_from_selected_vertex():
    """Создаёт и возвращает фолликл созданный по выделенной вершине сетки

    Returns
    -------
    tuple
        (follicle_name, follicle_shape_name)
    """
    u,v,mesh_name, vindex = _get_uv_of_selected_vertex()
    prefix = '%s_v_%s_' % (mesh_name, str(vindex))
    mesh = pm.PyNode(mesh_name)
    shape = mesh.getShape()

    f_name = '%sfollicle' % prefix
    follicle = '%sShape' % f_name
    if not mc.objExists(f_name):
        pm.createNode('follicle', n=follicle)
    else:
        raise Exception("Follicle with name \"%s\" Already Exists" % f_name)
    pm.connectAttr('%s.outMesh' % shape.name(), '%s.inputMesh' % follicle, f=True)
    pm.connectAttr('%s.worldMatrix[0]' % shape.name(), '%s.inputWorldMatrix' % follicle, f=True)
    pm.connectAttr('%s.outRotate' % follicle, '%s.rotate' % f_name, f=True)
    pm.connectAttr('%s.outTranslate' % follicle, '%s.translate' % f_name, f=True)
    pm.setAttr("%s.parameterU" % follicle, u)
    pm.setAttr("%s.parameterV" % follicle, v)

    _ob_to_hiden_group(f_name)

    return f_name, follicle


def make_on_surf_locator(mesh_name, e, lists, normal=False, type='locator'):
    """
    Parameters
    -----------
    mesh_name : str
        имя сетки
    e : int
        номер эджа
    lists : Object optional
        некий объект с параметрами
    normal : bool optional
        привязка к нормали
    type : str optional
        тип создаваемого объекта
    """
    mesh = pm.PyNode(mesh_name)
    ww = '%s_e_%s_' % (mesh.name(), str(e))
    try:
        pm.select(mesh.e[e], r = True)
    except Exception as e:
        print(e)
        pm.warning(e)
        return False
    curve = pm.duplicateCurve(n = '%scurveOnSurface' % ww)
    curve_ob = pm.PyNode(curve[0])
    curveShape = curve_ob.getShape()
    point = pm.createNode('pointOnCurveInfo', n = '%spointOnCurve'  % ww)
    point.turnOnPercentage.set(1)
    point.parameter.set(0.5)

    curveShape.worldSpace[0] >> point.inputCurve
    locator = pm.spaceLocator(n = '%slocOnSurface' % ww)
    point.result.position >> locator.translate

    # -- normal constraint
    if normal:
        pm.normalConstraint(mesh_name, locator, n = '%snormalConstraint' % ww)

    # -- parent locator
    # if hasattr(lists, "HIDDEN_GROUP_OF_SURF"):
    #     name_of_hiden_grp = lists.HIDDEN_GROUP_OF_SURF
    # else:
    #     name_of_hiden_grp = "HIDDEN_GROUP_OF_SURF"

    # if pm.objExists(name_of_hiden_grp):
    #     pm.parent(curve_ob, name_of_hiden_grp)
    # else:
    #     pm.select(curve_ob, r=True)
    #     pm.group(n = name_of_hiden_grp)
    _ob_to_hiden_group(curve_ob, lists=lists)

    pm.select(locator, r=True)
    return(locator, locator.getShape())


def make_surf_locator_from_selected_edje(lists=[], normal=False):
    """
    * создаёт локатор по выделенному эджу, с привязкой к еджу
    * локатор размеается в мире, кривая в группе ``HIDDEN_GROUP_OF_SURF``.
    """
    # selected edje
    edj = pm.ls(sl=1)[0]
    e = edj.index()
    mesh_shape_name = edj.name().split('.')[0]
    mesh_name = pm.listRelatives(mesh_shape_name, parent=1)[0].name()
    if lists:
        return(make_on_surf_locator(mesh_name, e, lists, normal=normal))
    else:
        return(make_on_surf_locator(mesh_name, e, [], normal=normal))


def make_surf_object_from_selected_vertex(type):
    """Создаёт кость привязанную к сетке, ориентированную по нормали

    Returns
    --------
    str
        имя созданного объекта
    """
    follicle = _create_follicle_from_selected_vertex()
    print(follicle)
    ob_name = follicle[0].replace('_follicle', '_%s' % type)

    if type=='locator':
        object = mc.spaceLocator(n=ob_name)[0]
    else:
        object = mc.createNode(type, n = ob_name)
    mc.parentConstraint(follicle[0], object, mo=False)
    _ob_to_hiden_group(object, group='TMP_SURF_%sS' % type.upper())

    mc.select(object, r=True)
    return object


def print_blend_shapes(lists, side='left'):
    """
    Печать в терминале имена и значения срабатывания блендов, атрибуты ноды :attr:`bs_face_simple.lists.ACTION_OB`.
    """
    ob=pm.PyNode(lists.ACTION_OB)
    print('\n')
    print('*'*25)
    for attr in ob.listAttr(k=1):
        bra=['fix_01',
            'visibility',
            'translateX',
            'translateY',
            'translateZ',
            'rotateX',
            'rotateY',
            'rotateZ',
            'scaleX',
            'scaleY',
            'scaleZ',
        ]
        name=attr.name()
        attr_name=name.split('.')[1]
        if side.lower()=='left':
            if attr_name in bra or attr_name.endswith('Right'):
                continue
        elif side.lower()=='right':
            if attr_name in bra or attr_name.endswith('Left'):
                continue
        if attr.get() > 0.00001:
            print('%s : %s' % (attr_name, attr.get()))


def eye_ik_start_position():
    pass
    return
    m = ['root_ik_eyes_ctrl', (0, 71.349, 126.108), (0, 0, 0)]
    l = ['grp_l_eyeAim_ctrl', (31.133, 71.349, 126.108), (90, 0, 0)]
    r = ['grp_r_eyeAim_ctrl', (-31.133, 71.349, 126.108), (90, 0, 0)]

    loc = pm.spaceLocator(name='eye_ik_positioner')

    for item in [m, l, r]:
        pass
        if not pm.objExists(item[0]):
            continue

        loc.t.set(item[1])

        for attr in ['tx','ty','tz','rx','ry','rz']:
            pm.setAttr('%s.%s' % (item[0], attr), lock=0)

        pc = pm.parentConstraint(loc, item[0])
        pm.delete(pc)

        ob = pm.PyNode(item[0])
        ob.r.set(item[2])

        for attr in ['tx','ty','tz','rx','ry','rz', 'sx', 'sy', 'sz']:
            if item[0] == 'root_ik_eyes_ctrl':
                continue
            pm.setAttr('%s.%s' % (item[0], attr), lock=1)

    pm.delete(loc)

    for name in ['l_eyeAim_ctrl', 'r_eyeAim_ctrl']:
        if not pm.objExists(name):
            continue
        ob = pm.PyNode(name)
        pm.setAttr(ob.ty, lock=True, keyable=False, channelBox=False)


def save_surf_ctrl_data(orig_ob, orig_root, surf_ob):
    """Запись в метадату данных создаваемого сурф контрола."""
    data = pipeline_connect.get_metadata("facial_ctrls_on_mesh")
    # --
    data[surf_ob.name()] = {}
    data[surf_ob.name()]['orig_ob'] = orig_ob.name()
    data[surf_ob.name()]['orig_root'] = orig_root.name()
    data[surf_ob.name()]['t'] = (orig_root.tx.get(), orig_root.ty.get(), orig_root.tz.get())
    data[surf_ob.name()]['r'] = (orig_root.rx.get(), orig_root.ry.get(), orig_root.rz.get())
    data[surf_ob.name()]['s'] = (orig_root.sx.get(), orig_root.sy.get(), orig_root.sz.get())

    # -- mesh data
    p = ''
    for i in range(0, 5):
        if not p:
            p = pm.listRelatives(surf_ob, p=True)[0]
        else:
            p = pm.listRelatives(p, p=True)[0]
        if p.name().endswith('_locOnSurface'):
            break
    print(p)
    p = p.replace('_locOnSurface', '')
    mesh, e = p.split('_e_')

    data[surf_ob.name()]['mesh'] = mesh
    data[surf_ob.name()]['e'] = e

    pipeline_connect.patch_metadata(data, "facial_ctrls_on_mesh")

    return True


def _edje_vertex_data_from_loc(loc):
    """
    возвращает кортеж для записи в метадату ('vertex'/'edje', num) по переданному локатору
    """
    if '_v_' in loc and loc.endswith('_locator'):
        ob = loc.split('_v_')[0]
        surf_ob = 'vertex'
        num = int(loc.split('_v_')[1].replace('_locator', ''))
    elif '_e_' in loc and loc.endswith('_locOnSurface'):
        ob = loc.split('_e_')[0]
        surf_ob = 'edje'
        num = int(loc.split('_e_')[1].replace('_locOnSurface', ''))
    return (ob, surf_ob, num)


def ctrl_to_locator(lists=[], message=True):
    """
    * Превращает контрол в сурфейс-контрол (привязанный к сетке). Выделение сначала локатора, потом контрола, старый контрол будет спрятан, новый со всей атрибутикой будет создан на месте сратого, используя шейп старого контрола. имя нового контрола получит постфикс ``_surf``.
    * Созданный конрол со всей иерархией попадает в группу ``os.environ['SURF_ROOT']``. На эту группу надо создавать *ParentConstraint* от кости головы.
    """
    sel = mc.ls(sl=1)
    if len(sel) !=2:
        mc.warning('!!!!')
        return
    # loc = pm.PyNode(sel[0])
    loc_name = sel[0]
    print('+'*100)
    print(loc_name)
    print('+'*100)

    ctrl_source = pm.PyNode(sel[1])
    shapes = ctrl_source.getShapes()

    # save metadata
    # -- edje/vertex
    # save_data = metadat.get(None, key="facial_ctrls_on_mesh")
    save_data = pipeline_connect.get_metadata("facial_ctrls_on_mesh", {})
    save_data[ctrl_source.name()] = _edje_vertex_data_from_loc(loc_name)
    # metadat.patch(None, facial_ctrls_on_mesh=save_data)
    pipeline_connect.patch_metadata(save_data, "facial_ctrls_on_mesh")
    # -- ctrl root position
    ctrl_root = mc.listRelatives(ctrl_source.name(), p=True)
    if ctrl_root:
        # data = metadat.get(None, key='facial_ctrls_positions')
        data = pipeline_connect.get_metadata("facial_ctrls_positions")
        data[ctrl_root[0]] = _get_position(ctrl_root[0])
        # metadat.patch(None, facial_ctrls_positions=data)
        pipeline_connect.patch_metadata(data, "facial_ctrls_positions")

    # if '_connect' in ctrl_source.name():
    #     ctrl_name = ctrl_source.name().replace('_connect', '_ctrl')
    # else:
    #     ctrl_name = '%s_surf' % ctrl_source.name()

    # new
    source_ctrl_name = '%s_surf' % ctrl_source.name()
    ctrl_name = ctrl_source.name()
    pm.rename(ctrl_source, source_ctrl_name)

    duplicates = pm.duplicate(ctrl_source, name = ctrl_name, ic=True, po=True)
    ctrl = duplicates[0]
    for shape in shapes:
        #print(shape
        #new_shape = pm.duplicate(shape, addShape=True)
        #print(new_shape)
        #pm.parent(new_shape[0], ctrl, s=True, add=True, r=True)
        pm.parent(shape, ctrl, s=True, add=True)
    # reverse
    reverse_name = '%s_reverse' % ctrl_source.name()
    pm.select(ctrl, r=True)
    reverse = pm.group(n = reverse_name)
    # offset
    offset_name = '%s_offset' % ctrl_source.name()
    pm.select(reverse, r=True)
    offset = mc.group(n = offset_name)
    mc.parent(offset, loc_name)
    # hide source loc
    attr = pm.Attribute(ctrl_source.v);attr.unlock();attr.disconnect();attr.set(0);attr.lock();
    # connect reverse
    mp = pm.shadingNode('multiplyDivide', au=True, n = '%s_mpd' % ctrl_source.name())
    mp.input2X.set(-1)
    mp.input2Y.set(-1)
    mp.input2Z.set(-1)
    #x
    ctrl.tx >> mp.input1X
    mp.outputX >> reverse.tx
    #y
    ctrl.ty >> mp.input1Y
    mp.outputY >> reverse.ty
    #z
    ctrl.tz >> mp.input1Z
    mp.outputZ >> reverse.tz
    # connect attr
    keyable_attrs = ctrl_source.listAttr(k=1)
    for key in keyable_attrs:
        '''
        if 'rotate' in key.name():
            key.lock()
            continue

        else:
        '''
        attr_name = key.name(includeNode=False)
        if attr_name=='visibility':
            continue
        eval('ctrl.%s >> ctrl_source.%s' % (attr_name, attr_name))
        cattr = pm.PyNode('%s.%s' % (ctrl_source, attr_name))
        cattr.lock()

    # рут для локатора
    mc.select(loc_name, r=True)
    loc_root = mc.group(n = '%s_locsurfroot' % loc_name)
    # рут для всех локаторов
    superrootname = os.environ['SURF_ROOT']
    if not mc.objExists(superrootname):
        mc.select(cl=True)
        superroot = mc.group(empty=True, n=superrootname)
    else:
        # superroot = pm.PyNode(superrootname)
        superroot = superrootname
    pm.parent(loc_root, superroot)

    # select control
    pm.select(ctrl, r=True)
    if message:
        pm.confirmDialog(t = 'message', m = 'Surface Control was created!')

    # save data
    try:
        save_surf_ctrl_data(ctrl_source, pm.listRelatives(ctrl_source, p=True)[0], ctrl)
    except:
        print(ctrl_source, ctrl)
        print(traceback.format_exc())

    # hide loc
    print('-'*100)
    print(loc_name)
    print('-'*100)
    # loc_shape = loc.getShapes()[0]
    loc_shape = mc.listRelatives(loc_name, c=True, type='shape')[0]
    # loc_shape.v.set(0)
    mc.setAttr('%s.v' % loc_shape, 0)


def make_surf_ctrls_from_file_data(lists, position_only=False):
    pass
    # 1 - чтение файла
    # 2 - расстановка контролов
    # 3 - создание локаторов

    # (1)
    if not get_path('USER_WORKDIR'):
        pm.warning('Undefined "utils.get_path(\'USER_WORKDIR\')"')
        return False
    path = os.path.join(get_path('USER_WORKDIR'), lists.LOCAL_DIR, lists.SURF_CTRL_DATA_JSON)
    if os.path.exists(path):
        with open(path, 'r') as f:
            data = json.load(f)

        for key in data:
            if pm.objExists("%s_surf" % key):
                print('*** "%s" - already Exists' % key)
                continue

            root = pm.PyNode(data[key]['orig_root'])
            # (2)
            for i in [('tx',0),('ty',1),('tz',2),('rx',0),('ry',1),('rz',2),('sx',0),('sy',1),('sz',2)]:
                try:
                    exec('root.%s.set(data[key]["%s"][%s])' % (i[0], i[0][0], str(i[1])))
                except:
                    pass

        if position_only:
                return True

        for key in data:
            if pm.objExists("%s_surf" % key):
                print('*** "%s" - already Exists' % key)
                continue

            # (3)
            locator, locatorShape = make_on_surf_locator(data[key]['mesh'], int(data[key]['e']), lists)
            locatorShape.v.set(0)


            # (4)
            pm.select(locator, r=True)
            pm.select(data[key]['orig_ob'].replace('_surf', ''), add=True)
            ctrl_to_locator(message=False)

    return True

# префикс рута оригинального контрола  = 'neutralPose_'
# постфикс сурф локатора = '_locOnSurface'
# структура имени сурф локатора - имя-сетки_e_номер-ребра__locOnSurface
def save_surf_ctrls_data(lists):
    pass
    # SURF
    data = {}
    for surf_ob in pm.ls('*_surf'):
        # orig_ob_name = surf_ob.replace('_surf', '')
        orig_ob_name = surf_ob.name()
        orig_root_name = 'neutralPose_%s' % orig_ob_name.replace('_surf', '')
        if pm.objExists(orig_ob_name) and pm.objExists(orig_root_name):
            orig_ob = pm.PyNode(orig_ob_name)
            orig_root = pm.PyNode(orig_root_name)
        else:
            continue
        if pm.objExists(orig_ob_name.replace('_surf', '')):
            surf_ob=pm.PyNode(orig_ob_name.replace('_surf', ''))
        else:
            continue
        data[surf_ob.name()] = {}
        data[surf_ob.name()]['orig_ob'] = orig_ob_name
        data[surf_ob.name()]['orig_root'] = orig_root_name
        data[surf_ob.name()]['t'] = (orig_root.tx.get(), orig_root.ty.get(), orig_root.tz.get())
        data[surf_ob.name()]['r'] = (orig_root.rx.get(), orig_root.ry.get(), orig_root.rz.get())
        data[surf_ob.name()]['s'] = (orig_root.sx.get(), orig_root.sy.get(), orig_root.sz.get())

        # mesh data
        p = ''
        for i in range(0, 5):
            if not p:
                p = pm.listRelatives(surf_ob, p=True)[0]
            else:
                p = pm.listRelatives(p, p=True)[0]
            if p.name().endswith('_locOnSurface'):
                break
        #print p
        p = p.replace('_locOnSurface', '')
        mesh, e = p.split('_e_')

        data[surf_ob.name()]['mesh'] = mesh
        data[surf_ob.name()]['e'] = e

    # save data
    if not get_path('USER_WORKDIR'):
        pm.warning('Undefined "os.environ[\'USER_WORKDIR\']"')
        return False

    path = os.path.join(get_path('USER_WORKDIR'), lists.LOCAL_DIR, lists.SURF_CTRL_DATA_JSON)
    with open(path, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4)

    # OTHER
    # get data
    ctrls = [
    'neutralPose_CTRL_eye_L',
    'neutralPose_CTRL_eye_R',
    'root_eyes',
    'neutralPose_CTRL_squint_L',
    'neutralPose_CTRL_squint_R',
    'neutralPose_CTRL_up_eye_lid_L',
    'neutralPose_CTRL_up_eye_lid_R',
    ]
    data = {}
    for name in ctrls:
        if not pm.objExists(name):
            continue
        ob = pm.PyNode(name)
        data[name] = {}
        data[name]['t'] = (ob.tx.get(), ob.ty.get(), ob.tz.get())
        data[name]['r'] = (ob.rx.get(), ob.ry.get(), ob.rz.get())
        data[name]['s'] = (ob.sx.get(), ob.sy.get(), ob.sz.get())
    # save data
    path = os.path.join(get_path('USER_WORKDIR'), lists.LOCAL_DIR, lists.OTHER_CTRL_DATA_JSON)
    with open(path, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4)

    return True

    #print(json.dumps(data, sort_keys=True, indent=1))
    #print(data.keys()


def ctrls_set(ctrls, ctrl_set=settings.FACIAL_MOCAP_RIG_CTRLS_SET, replace=True):
    """
    Создание, перезапись или дополнение сета лицевых контролов.

    Parameters
    ------------
    ctrls : list
        список контролов добавляемых в сет
    set : str optional
        имя сета
    replace : bool optional
        Если ``True`` (по умолчанию) сет будет полностью перезаписан, иначе будет добавление новых элементов.

    Returns
    --------
    str
        имя созданного сета
    """
    print('-'*50 + 'create ctrl set' + '-'*50)
    try:
        if mc.objExists(ctrl_set):
            if replace:
                mc.sets(cl=ctrl_set)
        else:
            ctrl_set = mc.sets(name=ctrl_set)
        exists_ctls = list()
        for ob in ctrls:
            if mc.objExists(ob):
                exists_ctls.append(ob)
                print('Exists ctrl: %s' % ob)
            else:
                print('No Found ctrl: %s' % ob)
        return mc.sets(exists_ctls, include=ctrl_set)
    except:
        print(traceback.format_exc())


def _get_position(ob):
    """
    Возвращает список кортежей координат объекта **ob** - [(tx,ty,tz), (rx,ry,rz), (sx,sy,sz)]
    """
    return (mc.getAttr('%s.t' % ob)[0], mc.getAttr('%s.r' % ob)[0], mc.getAttr('%s.s' % ob)[0])


def _set_position(ob, position):
    """
    Выставляет значения объекту ob из position

    Parameters
    ----------
    ob: str
        объект
    position: list
        список или кортеж кортежей координат [(tx,ty,tz), (rx,ry,rz), (sx,sy,sz)]
    """
    if not mc.objExists(ob):
        print("Object: \"%s\" Not Found" % ob)
        return

    attrs = ("t","r","s")
    for i,attr in enumerate(attrs):
        try:
            mc.setAttr('%s.%s' % (ob, attr), *position[i])
        except:
            pass


def save_facial_ctrl_position(config):
    """Запись в метадату **facial_ctrls_positions** персонажа координат расположения контролов рамки и самой рамки

    Используются **config.CTRL_ROOT_OB**, **config.CTRL_SUB_ROOT_OB**

    Перечень контролов из дочерних элементов **config.CTRL_SUB_ROOT_OB**

    Parameters
    -----------
    config : object
        импортированный модуль конфигурации персонажа.

    Returns
    --------
    None
    """
    SUBROOT = 'RAMKA'
    """str: верятное имя для ``CTRL_SUB_ROOT_OB`` """

    confirm = mc.confirmDialog(
        title='Save Control Position',
        message='Are you sure?',
        button=['Yes','No'],
        defaultButton='Yes',
        cancelButton='No',
        dismissString='No'
        )
    if confirm != "Yes":
        return

    if not hasattr(config, "CTRL_ROOT_OB"):
        raise Exception('Parameter "CTRL_ROOT_OB" is missing in the config')
    if not mc.objExists(config.CTRL_ROOT_OB):
        raise Exception('No Found "%s"' % config.CTRL_ROOT_OB)

    data = dict()

    roots = [config.CTRL_ROOT_OB]
    if hasattr(config, "CTRL_SUB_ROOT_OB") and mc.objExists(config.CTRL_SUB_ROOT_OB):
        subroot = config.CTRL_SUB_ROOT_OB
        roots.append(subroot)
    else:
        subroot = config.CTRL_ROOT_OB

    for ob in roots:
        data[ob] = _get_position(ob)

    for ob in mc.listRelatives(subroot, c=True, type='transform', ad=True):
        # for ch in mc.listRelatives(ob, c=True, type='transform'):
        #     if not ch.endswith('_frame'):
                # data[ch] = _get_position(ch)
        if ob.startswith('Root_') or ob==SUBROOT:
            data[ob] = _get_position(ob)

    print(json.dumps(data, sort_keys=True, indent=4))

    # return

    # metadat.patch(None, facial_ctrls_positions=data)
    pipeline_connect.patch_metadata(data, "facial_ctrls_positions")

    parent_objs = mc.listRelatives(config.CTRL_ROOT_OB, p=True)
    if parent_objs:
        parent_ob = parent_objs[0]
        # metadat.patch(None, facial_ctrls_parent=parent_ob)
        pipeline_connect.patch_metadata(parent_ob, "facial_ctrls_parent")


def set_facial_ctrl_position(config):
    """
    Расстановка контролов по данным из метадаты **facial_ctrls_positions**.
    """
    print('*'*20 + "set_facial_ctrl_position()")
    if not hasattr(config, "CTRL_ROOT_OB"):
        raise Exception('Parameter "CTRL_ROOT_OB" is missing in the config')
    if not mc.objExists(config.CTRL_ROOT_OB):
        raise Exception('No Found "%s"' % config.CTRL_ROOT_OB)

    # parent_ob = metadat.get(None, key="facial_ctrls_parent")
    parent_ob = pipeline_connect.get_metadata("facial_ctrls_parent")
    if parent_ob:
        try:
            mc.parent(config.CTRL_ROOT_OB, parent_ob)
        except:
            pass

    # data = metadat.get(None, key="facial_ctrls_positions")
    data = pipeline_connect.get_metadata("facial_ctrls_positions")
    print(json.dumps(data, sort_keys=True, indent=4))
    for ob,position in data.items():
        _set_position(ob, position)


def _one_surf_ctrl_from_metadata(ctrl, data):
    """
    Создаёт сурфейс контрол из кортежа полученного из **metadata.facial_ctrls_on_mesh**

    Parameters
    -----------
    ctrl : str
        имя контрола
    data : tuple
        данные крепления контрола из метадаты: **('vertex'/'edje', num)**
    """
    try:
        if data[1] == 'vertex':
            mc.select('%s.vtx[%s]' % (data[0], data[2]), r=True)
        elif data[1] == 'edje':
            mc.select('%s.e[%s]' % (data[0], data[2]), r=True)
        loc = make_surf_object_from_selected_vertex('locator')
        mc.select(ctrl, r=False, add=True)
        ctrl_to_locator()
    except:
        print(traceback.format_exc())


def make_surf_ctrls_from_metadata():
    """
    Создаёт все сурф контролы по записи из метадаты **metadata.facial_ctrls_on_mesh**
    """
    confirm = mc.confirmDialog(
        title='Surf Controls Create',
        message='Are you sure?',
        button=['Yes','No'],
        defaultButton='Yes',
        cancelButton='No',
        dismissString='No'
        )
    if confirm != "Yes":
        return

    # data = metadat.get(None, key="facial_ctrls_on_mesh")
    data = pipeline_connect.get_metadata("facial_ctrls_on_mesh")
    for key,value in data.items():
        _one_surf_ctrl_from_metadata(key, value)