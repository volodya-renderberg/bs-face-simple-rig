#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Maya функции
"""

import os
import logging

import pymel.core as pm
import maya.cmds as cmds

import facial_utils as utils
import pipeline_connect
import livelink

logger = logging.getLogger(__name__)


def get_namespace_from_selected_object():
    """Возвращает ``namecpase`` выделенного объекта, или None. """
    selecteds = cmds.ls(sl=True)
    if selecteds:
        ob = selecteds[0]
        if len(ob.split(':')) > 1:
            ns = ob.split(':')[0]
            return ns


def import_livelink_animation_to_selected_from_file(config):
    """
    Запекание ``livelink`` анимации на выделенного персонажа из ``.csv`` файла
    """
    ns = get_namespace_from_selected_object()
    csv_path = pm.fileDialog(m=0)
    codes = livelink.get_csv(csv_path)
    livelink.bake_animation(ns, codes, config, start_frame=1)


# def import_file(t_info, ext=".ma"):
#     """
#     Импорт файла по ууказанному расширению из переданной задачи.

#     :param t_info: таск инфо
#     :param ext: расширение ``.ma``, ``.fbx``
#     :return:
#     """
#     # logger.info(t_info)

#     fvers=c_functions.task_versions_paths(t_info)
#     path=fvers.publish_path()

#     # logger.info("*** versions %s" % fvers.versions)
#     # logger.info("*** local_path %s" % fvers.local_path())
#     logger.info("*** publish_path %s" % path)
#     # logger.info("*** next_version_path %s" % fvers.next_version_path())
#     # logger.info("*** current_version_path %s" % fvers.current_version_path())

#     if not path:
#         pm.warning("Not publish path of task: %s" % t_info)
#         return

#     if not os.path.splitext(path)[1] == ext:
#         path="%s%s" % (os.path.splitext(path)[0], ext)
#     if os.path.exists(path):
#         pm.importFile(path)
#     else:
#         pm.warning("File path not found: %s" % path)


# def open_publish_folder(t_info):
#     """
#     Запуск в проводнике папки паблиш версии задачи.

#     :param t_info: словарь задачи
#     :type t_info: dict
#     :return: None
#     """
#     fvers = c_functions.task_versions_paths(t_info)
#     path = fvers.publish_path()
#     os.system("start %s" % os.path.dirname(path))


def checking_blend_shapes(config):
    """
    Чекинг блендов с принтингом. Чекаются сеитки выбранной группы.

    :param config: конфиг персонажа
    :type config: module
    :return:
    """
    lower_names = dict()
    for n in config.DRIVER_CONFIG.keys():
        if n.endswith("_driver"):
            continue
        lower_names[n.lower()] = n
    no_checking_names = list()
    # no_checking_names=["jawOpen", "jawRight", "jawLeft", "jawForward"]
    exists_blends_names = list()
    for ob in cmds.listRelatives(c=True):
        exists_blends_names.append(ob.lower())

    print('-' * 200)
    print(u"*** Есть в сцене, нет в конфиге ***")

    for ob in cmds.listRelatives(c=True):
        if ob in no_checking_names:
            continue
        if not ob in config.DRIVER_CONFIG.keys():
            if ob.lower() in lower_names:
                cmds.rename(ob, lower_names[ob.lower()])
                print("rename %s -> %s" % (ob, lower_names[ob.lower()]))
            else:
                print("%s" % ob)

    print(u"\n*** Есть в конфиге, нет в сцене ***")
    for n in lower_names:
        if not n in exists_blends_names:
            print("%s" % lower_names[n])

    print('-' * 200)


def append_blends(mesh, bs, config):
    """
    Создание блендшейп ноды на указанную сетку. Бленды берутся из выбранной группы.

    :param mesh:  имя сетки на которую применяются бленды
    :type config: str
    :param bs: имя бленд ноды.
    :type bs: str
    :return: None
    """
    print("BLENDS %s - %s" % (mesh, bs))

    # test exists
    if cmds.objExists(bs):
        cmds.delete(bs)

    # make BlendShapes
    try:
        group = cmds.ls(sl=True)[0]
    except IndexError:
        cmds.warning(u"Группа блендов не выбрана")
        return
    meshes = list()
    for ob in cmds.listRelatives(c=True, f=True):
        if not ob.split("|")[-1:][0] in config.DRIVER_CONFIG.keys():
            continue
        else:
            meshes.append(ob)

    if not meshes:
        cmds.warning(u"Бленды не выбраны")
        return

    cmds.select(deselect=True)
    cmds.select(meshes, tgl=True)
    cmds.select(mesh, add=True)

    cmds.blendShape(name=bs, tc=False)


def save_facial_ctrl_set(ask=False):
    """
    Записывает в метадату персонажа ``facial_ctrls`` - выделенные контролы.
    При этом переопределяет сет :attr:`Tentaculo_processors.settings.FACIAL_MOCAP_RIG_CTRLS_SET`

    Returns
    ----------
    None
    """
    # -- confirm
    if ask:
        if cmds.confirmDialog(
            m="Write set data?\nThis is to delete previously saved data!",
            t="Confirm",
            b=['Ok', 'Cansel'],
            db="Ok",
            cb='Cansel') != 'Ok':
            return
    ctrls = cmds.ls(sl=True)
    # asset_info = c_functions.task_info_by_task_id(int(os.environ["MF_TASK_PARENT_ID"]))
    # meta_controls  = metadat.patch(asset_info, facial_ctrls=ctrls)
    pipeline_connect.patch_metadata(ctrls, "facial_ctrls")
    utils.ctrls_set(ctrls)


def set_facial_ctrl_set(config):
    """
    Создаёт лицевой сет из записи в метадате ассета.
    При отсутствии записи в метадате, пробует читать из конфига и в случае удачи перезаписывает метадату.

    Parameters
    -----------
    config : object
        конфиг персонажа

    Returns
    --------
    None
    """
    # asset_info = c_functions.task_info_by_task_id(int(os.environ["MF_TASK_PARENT_ID"]))
    # ctrls  = metadat.get(asset_info, key="facial_ctrls")
    ctrls  = pipeline_connect.get_metadata("facial_ctrls")
    if not ctrls:
        if hasattr(config, "FACIAL_MOCAP_RIG_CTRLS"):
            ctrls = config.FACIAL_MOCAP_RIG_CTRLS
            cmds.select(ctrls, r=True)
            save_facial_ctrl_set()
        else:
            raise Exception("Нет записи сета лицевых контролов! Выполните прежде \"Save Control Set\"")
    else:
        utils.ctrls_set(ctrls)