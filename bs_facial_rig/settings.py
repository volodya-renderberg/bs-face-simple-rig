# -*- coding: utf-8 -*-

"""
Общие настройки
"""

import os


USE_PIPELINE = False
"""bool: Использовать Pipeline или локальные пути."""


FASE_META_ROOT = os.path.normpath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'local_data'))
"""str: Путь до родительской директории встроенного ``face_meta``, для :attr:`USE_PIPELINE` = ``False``"""


METADATA_FILE = os.path.join(os.path.expanduser('~'), '.bs-face-simple-rig', 'facial_metadata.json')
"""str: Путь до файла записи рабочих метаданных сборки лицевого рига, для :attr:`USE_PIPELINE` = ``False`` """


HTML_DIR = os.path.normpath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'html'))
"""str: Путь до директории ``html`` (где лежит сгенерённая документация)"""


class HELP:
    """Класс энум содержащий все ссылки на доку. """
    # main_help = "https://bs-face-simple.readthedocs.io/en/latest/index.html"
    main_help = "%s/index.html" % HTML_DIR
    convert_u_fix_help = ""


FACIAL_MOCAP_RIG_CTRLS_SET = "rig_bs_facial_ctrls_grp"
"""str: имя сета контролов лицевого рига, используемых при запекании мимики с мокапа. """
