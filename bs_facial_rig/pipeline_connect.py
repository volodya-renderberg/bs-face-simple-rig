# -*- coding: utf-8 -*-
import os
import json

from bs_facial_rig  import settings


def get_global_face_meta():
    """Возвращает путь до глобальной директории ``face_meta``"""
    if not settings.USE_PIPELINE:
        return settings.FASE_META_ROOT


def get_local_face_meta():
    """Возвращает путь до локальной директории ``face_meta``"""
    if not settings.USE_PIPELINE:
        return settings.FASE_META_ROOT


def patch_metadata(metadata, key):
    """Запись метадаты сборки лицевого рига
    
    Parameters
    -----------
    metadata : dict list str int float optional
        сохраняемые данные
    key : str
        записываемый ключ.
    """
    if not settings.USE_PIPELINE:
        mfile = settings.METADATA_FILE
        data = dict()
        if os.path.exists(mfile):
            with open(mfile, 'r') as fp:
                data = json.load(fp)
        else:
            os.makedirs(os.path.dirname(mfile))
    
        k_data = data.get(key, metadata.__class__())
        if isinstance(k_data, dict) and isinstance(metadata, dict):
            k_data = dict(list(k_data.items()) + list(metadata.items()))
        elif isinstance(k_data, list) and isinstance(metadata, list):
            k_data = k_data + metadata
        elif isinstance(k_data, str) or isinstance(k_data, int) or isinstance(k_data, float):
            k_data = metadata
        save_data = data
        save_data[key] = k_data
        
        with open(mfile, 'w') as fp:
            json.dump(save_data, fp, sort_keys=True, indent=4)
    else:
        return


def get_metadata(key, empty_ob = None):
    """Чтение метадаты сборки лицевого рига
    
    Parameters
    ------------
    key : str
        ключ по которому идёт чтение метадаты, если None - вернёт всю метадату
    empty_class : class instance optional
        объект, который будет возвращён при отсутствии значения по ключу.
    """
    if not settings.USE_PIPELINE:
        mfile = settings.METADATA_FILE
        data = dict()
        if os.path.exists(mfile):
            with open(mfile, 'r') as fp:
                data = json.load(fp)
        if key:
            return data.get(key, empty_ob)
        else:
            return data
    else:
        return