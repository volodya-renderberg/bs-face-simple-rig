#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Главна панель лицевого рига
"""
import sys
import os
import webbrowser
import logging
import traceback

try:
    from importlib import reload
except:
    pass

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

file_handler = logging.FileHandler(os.path.join(os.path.expanduser('~'), 'facial_rig_logs.log'))
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

import pymel.core as pm

import pipeline_connect

import facial_utils as utils, hook, build, funcs
reload(utils)
reload(hook)
reload(build)
reload(funcs)


def append_blends_dialog(config):
    """
    Диалог выбора сетки для которой будут создаваться блкенды.
    Бленды - дочерние сетки выбранной группы.

    :param config: Конфиг персонажа
    :type config: module
    :return: None
    """
    try:
        # window
        window = pm.window(t='Import data from Incoming tasks:')
        # layout
        layout = pm.columnLayout(adj=True)

        for mesh, bs in config.MESHES.items():
            b = pm.button(l="%s - %s" % (mesh, bs))
            b.setCommand(pm.Callback(funcs.append_blends, mesh, bs, config))

        pm.setParent('..')
        window.show()
    except:
        logger.error(traceback.format_exc())


def help_tutorial(*args): # help button
    '''help '''
    path = pipeline_connect.settings.HELP.main_help
    webbrowser.open(path)


def run():
    """Gui"""
    try:
        face_meta_dir = pipeline_connect.get_local_face_meta()
        sys.path.append(face_meta_dir)
        sys.path.insert(0, face_meta_dir)

        from face_meta import config
        reload(config)
        from face_meta import hook as hook_local
        reload(hook_local)

        from split_blend_shapes import ui as split_ui
        reload(split_ui)

        from from_meta_human_blends import ui as meta_bs_ui
        reload(meta_bs_ui)

        from edit_blend_shapes import ui as edit_bs_ui
        reload(edit_bs_ui)

        # window
        window = pm.window(t='Facial Rig')
        # menu
        menuBar = pm.menuBarLayout()
        pm.menu(l='Utilities')
        pm.menuItem(l='--- Surf Controls ----', en=False)
        make_surf_locator = pm.menuItem('Make Locator from Edge', \
                                    c=pm.Callback(utils.make_surf_locator_from_selected_edje))
        make_surf_locator = pm.menuItem('Make Locator from Vetrtex', \
                                    c=pm.Callback(utils.make_surf_object_from_selected_vertex, 'locator'))
        make_surf_locator = pm.menuItem('Make Joint from Vetrtex', \
                                    c=pm.Callback(utils.make_surf_object_from_selected_vertex, 'joint'))
        make_surf_control = pm.menuItem('Make Surface Control from locator', c=pm.Callback(utils.ctrl_to_locator))

        pm.menuItem(l='--- Split meshes ----', en=False)
        pm.menuItem('L/R Split Blend Shape', c=pm.Callback(split_ui.run))

        pm.menuItem(l='--- Blend Shapes ----', en=False)
        pm.menuItem('Blends From Metahuman', c=pm.Callback(meta_bs_ui.run))
        pm.menuItem('_fix <-> _u', c=pm.Callback(edit_bs_ui.run, config))

        pm.menuItem(l='--- Controls Position ----', en=False)
        pm.menuItem('Connect root of Ctrls', c=pm.Callback(utils.connect_ctrls))
        # pm.menuItem('Save Control Position', c=pm.Callback(utils.save_facial_ctrl_position, config))
        # pm.menuItem('Set Control Position', c=pm.Callback(utils.set_facial_ctrl_position, config))

        pm.menuItem(l='--- Control sets ----', en=False)
        pm.menuItem('Save Control Set', c=pm.Callback(funcs.save_facial_ctrl_set, True))
        pm.menuItem('Create Facial Control Set', c=pm.Callback(funcs.set_facial_ctrl_set, config))

        pm.menuItem(l='--- Live Link ----', en=False)
        pm.menuItem('Import Animation From .csv', c=pm.Callback(funcs.import_livelink_animation_to_selected_from_file, config))

        pm.menu(l='Help')
        pm.menuItem(l='Manual', c=help_tutorial)

        # layout
        layout = pm.columnLayout(adj=True)

        # buttons
        # -- blends
        pm.text(u'Бленды')
        # import_blends_btn = pm.button(l='Import Blend Shapes')
        checking_blends_btn = pm.button(l='Cheking Blend Shapes')
        append_blends_btn = pm.button(l='Appends Blend Shapes')

        # -- build
        pm.separator()
        pm.text(u'Сборка')
        pm.text(u'Контролы:', align='left')
        # pm.rowLayout(nc=4, adj=True)
        import_rig_tmp_btn = pm.button(l='Import Rig Template')
        pm.rowLayout(nc=3, adj=True)
        pm.button(l='Set Position', c=pm.Callback(utils.set_facial_ctrl_position, config))
        pm.button(l='Save Position', c=pm.Callback(utils.save_facial_ctrl_position, config))
        pm.button(l='Surf ctrls', c=pm.Callback(utils.make_surf_ctrls_from_metadata))
        pm.setParent('..')
        pm.text(u'Управление:', align='left')
        pre_bild_btn = pm.button(l='Pre Bild')
        pm.rowLayout(nc=2, adj=True)
        clear_bs_btn = pm.button(l='Clear Driving')
        pm.button(l='Full Clear', c=pm.Callback(build.clear_driving_blend_shaps, config, True, True))
        pm.setParent('..')
        dr_bs_btn = pm.button(l='Driving Blend Shapes')

        pm.setParent('..')

        def dr_bs_btn_func(config):
            # build
            build.connect_blend_shapes(config)
            # post_bild
            hook_local.post_bild(config)

        # def import_blends_ui(config, task_info):
        #     """
        #     Запуск диалога импорта файлов по входящим связям.

        #     :param config: модуль конфиг из ``face_meta`` config
        #     :type  config: module
        #     :return: None
        #     """
        #     incoming_tasks=c_functions.incoming_tasks(task_info, orig_info=True)
        #     incoming_tasks_dialog(incoming_tasks)


        # connect buttons
        # import_blends_btn.setCommand(pm.Callback(import_blends_ui, config, task_info))
        checking_blends_btn.setCommand(pm.Callback(funcs.checking_blend_shapes, config))
        append_blends_btn.setCommand(pm.Callback(append_blends_dialog, config))

        import_rig_tmp_btn.setCommand(pm.Callback(build.import_template_of_facial_controls, config))
        pre_bild_btn.setCommand(pm.Callback(hook_local.pre_bild, config))
        clear_bs_btn.setCommand(pm.Callback(build.clear_driving_blend_shaps, config))
        dr_bs_btn.setCommand(pm.Callback(dr_bs_btn_func, config))

        window.show()
        pm.window(window, edit=True, wh=[400, 300])
    except:
        logger.error(traceback.format_exc())


def run_checking_blend_shapes():
    funcs.checking_blend_shapes(config) #откуда берётся config???

