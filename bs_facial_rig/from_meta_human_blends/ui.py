#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import tempfile
import webbrowser
import logging
import traceback
import subprocess
import json

# -- logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
file_handler = logging.FileHandler(os.path.join(os.path.expanduser('~'), 'face_split_bs.log'))
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

import pymel.core as pm
import maya.cmds as mc
import maya.mel as mel

def help_tutorial(*args):
    '''help '''
    # path="https://wiki.magicfactory.ru/index.php/Взятие_блендов_из_MetaHuman"
    path="http://192.168.33.252/wiki/wiki-manuals/modellers/bs-from-metahuman-export-edit/"
    webbrowser.open(path)

GROUP="META_HUMAN_EXPORTS_BLENDS"


ZERO_POSITION = [
    ["CTRL_R_mouth_dimple.translateY", 0.0],
    ["CTRL_L_mouth_sharpCornerPull.translateY", 0.0],
    ["CTRL_R_mouth_funnelD.translateY", 0.0],
    ["CTRL_C_tongue.translateX", 0.0],
    ["CTRL_C_tongue.translateY", 0.0],
    ["CTRL_L_nose_wrinkleUpper.translateY", 0.0],
    ["CTRL_R_mouth_stickyInnerD.translateY", 0.0],
    ["CTRL_R_mouth_stickyInnerU.translateY", 0.0],
    ["CTRL_L_neck_mastoidContract.translateY", 0.0],
    ["CTRL_L_brow_lateral.translateY", 0.0],
    ["CTRL_C_tongue_press.translateY", 0.0],
    ["CTRL_L_mouth_stretchLipsClose.translateY", 0.0],
    ["CTRL_R_eye_squintInner.translateY", 0.0],
    ["CTRL_L_mouth_lipSticky.translateY", 0.0],
    ["CTRL_L_jaw_chinCompress.translateY", 0.0],
    ["CTRL_R_mouth_lowerLipDepress.translateY", 0.0],
    ["CTRL_L_eye_blink.translateY", 0.0],
    ["CTRL_R_mouth_lipBiteD.translateY", 0.0],
    ["CTRL_C_tongue_roll.translateX", 0.0],
    ["CTRL_C_tongue_roll.translateY", 0.0],
    ["CTRL_R_brow_raiseIn.translateY", 0.0],
    ["CTRL_L_brow_raiseOut.translateY", 0.0],
    ["CTRL_R_eye_cheekRaise.translateY", 0.0],
    ["CTRL_R_mouth_stickyOuterU.translateY", 0.0],
    ["CTRL_R_mouth_lipsBlow.translateY", 0.0],
    ["CTRL_L_mouth_lipBiteD.translateY", 0.0],
    ["CTRL_R_jaw_chinCompress.translateY", 0.0],
    ["CTRL_R_mouth_tightenU.translateY", 0.0],
    ["CTRL_R_mouth_towardsU.translateY", 0.0],
    ["CTRL_R_eye_pupil.translateY", 0.0],
    ["CTRL_L_mouth_purseU.translateY", 0.0],
    ["CTRL_R_neck_mastoidContract.translateY", 0.0],
    ["CTRL_R_mouth_pressU.translateY", 0.0],
    ["CTRL_rigLogicSwitch.translateY", 1.0],
    ["CTRL_L_jaw_clench.translateY", 0.0],
    ["CTRL_C_tongue_narrowWide.translateY", 0.0],
    ["CTRL_R_eye_blink.translateY", 0.0],
    ["CTRL_L_mouth_purseD.translateY", 0.0],
    ["CTRL_C_mouth_stickyD.translateY", 0.0],
    ["CTRL_L_mouth_lipsTogetherD.translateY", 0.0],
    ["CTRL_R_mouth_suckBlow.translateY", 0.0],
    ["CTRL_R_mouth_lipsTogetherD.translateY", 0.0],
    ["CTRL_C_jaw.translateX", 0.0],
    ["CTRL_C_jaw.translateY", 0.0],
    ["CTRL_R_jaw_ChinRaiseD.translateY", 0.0],
    ["CTRL_C_eye_parallelLook.translateY", 0.0],
    ["CTRL_L_mouth_stickyInnerD.translateY", 0.0],
    ["CTRL_L_eye_squintInner.translateY", 0.0],
    ["CTRL_L_mouth_pressD.translateY", 0.0],
    ["CTRL_R_mouth_stretch.translateY", 0.0],
    ["CTRL_R_mouth_lipBiteU.translateY", 0.0],
    ["CTRL_R_mouth_purseU.translateY", 0.0],
    ["CTRL_L_eye_lidPress.translateY", 0.0],
    ["CTRL_L_mouth_cornerPull.translateY", 0.0],
    ["CTRL_L_mouth_tightenU.translateY", 0.0],
    ["CTRL_L_mouth_upperLipRaise.translateY", 0.0],
    ["CTRL_R_mouth_cornerDepress.translateY", 0.0],
    ["CTRL_C_tongue_inOut.translateY", 0.0],
    ["CTRL_L_mouth_lowerLipDepress.translateY", 0.0],
    ["CTRL_R_eye.translateX", 0.0],
    ["CTRL_R_eye.translateY", 0.0],
    ["CTRL_L_mouth_lipsPressU.translateY", 0.0],
    ["CTRL_C_jaw_openExtreme.translateY", 0.0],
    ["CTRL_R_mouth_stretchLipsClose.translateY", 0.0],
    ["CTRL_L_mouth_pressU.translateY", 0.0],
    ["CTRL_R_mouth_tightenD.translateY", 0.0],
    ["CTRL_L_mouth_tightenD.translateY", 0.0],
    ["CTRL_C_eye.translateX", 0.0],
    ["CTRL_C_eye.translateY", 0.0],
    ["CTRL_L_mouth_lipsBlow.translateY", 0.0],
    ["CTRL_L_jaw_ChinRaiseU.translateY", 0.0],
    ["CTRL_L_mouth_dimple.translateY", 0.0],
    ["CTRL_R_eye_lidPress.translateY", 0.0],
    ["CTRL_L_mouth_stickyOuterU.translateY", 0.0],
    ["CTRL_lookAtSwitch.translateY", 0.0],
    ["CTRL_R_nose.translateX", 0.0],
    ["CTRL_R_nose.translateY", 0.0],
    ["CTRL_R_neck_stretch.translateY", 0.0],
    ["CTRL_L_mouth_lipsTogetherU.translateY", 0.0],
    ["CTRL_R_jaw_ChinRaiseU.translateY", 0.0],
    ["CTRL_L_mouth_towardsU.translateY", 0.0],
    ["CTRL_L_mouth_funnelD.translateY", 0.0],
    ["CTRL_neck_digastricUpDown.translateY", 0.0],
    ["CTRL_R_mouth_funnelU.translateY", 0.0],
    ["CTRL_neck_throatUpDown.translateY", 0.0],
    ["CTRL_C_mouth_stickyU.translateY", 0.0],
    ["CTRL_R_mouth_stickyOuterD.translateY", 0.0],
    ["CTRL_R_brow_down.translateY", 0.0],
    ["CTRL_R_mouth_lipSticky.translateY", 0.0],
    ["CTRL_R_brow_raiseOut.translateY", 0.0],
    ["CTRL_L_mouth_funnelU.translateY", 0.0],
    ["CTRL_L_eye_pupil.translateY", 0.0],
    ["CTRL_L_nose.translateX", 0.0],
    ["CTRL_L_nose.translateY", 0.0],
    ["CTRL_R_mouth_lipsPressU.translateY", 0.0],
    ["CTRL_C_neck_swallow.translateY", 0.0],
    ["CTRL_R_mouth_lipsTogetherU.translateY", 0.0],
    ["CTRL_R_mouth_upperLipRaise.translateY", 0.0],
    ["CTRL_L_mouth_cornerDepress.translateY", 0.0],
    ["CTRL_R_mouth_pressD.translateY", 0.0],
    ["CTRL_L_mouth_stretch.translateY", 0.0],
    ["CTRL_L_jaw_ChinRaiseD.translateY", 0.0],
    ["CTRL_L_ear_up.translateY", 0.0],
    ["CTRL_R_mouth_towardsD.translateY", 0.0],
    ["CTRL_R_mouth_purseD.translateY", 0.0],
    ["CTRL_L_neck_stretch.translateY", 0.0],
    ["CTRL_R_mouth_sharpCornerPull.translateY", 0.0],
    ["CTRL_R_brow_lateral.translateY", 0.0],
    ["CTRL_L_mouth_lipBiteU.translateY", 0.0],
    ["CTRL_R_jaw_clench.translateY", 0.0],
    ["CTRL_L_mouth_stickyInnerU.translateY", 0.0],
    ["CTRL_L_mouth_suckBlow.translateY", 0.0],
    ["CTRL_C_tongue_tip.translateX", 0.0],
    ["CTRL_C_tongue_tip.translateY", 0.0],
    ["CTRL_L_eye_cheekRaise.translateY", 0.0],
    ["CTRL_L_brow_raiseIn.translateY", 0.0],
    ["CTRL_R_mouth_cornerPull.translateY", 0.0],
    ["CTRL_L_eye.translateX", 0.0],
    ["CTRL_L_eye.translateY", 0.0],
    ["CTRL_L_mouth_towardsD.translateY", 0.0],
    ["CTRL_R_nose_wrinkleUpper.translateY", 0.0],
    ["CTRL_L_brow_down.translateY", 0.0],
    ["CTRL_C_mouth.translateX", 0.0],
    ["CTRL_C_mouth.translateY", 0.0],
    ["CTRL_C_jaw_fwdBack.translateY", 0.0],
    ["CTRL_R_ear_up.translateY", 0.0],
    ["CTRL_neck_throatExhaleInhale.translateY", 0.0],
    ["CTRL_L_mouth_stickyOuterD.translateY", 0.0],
    ["CTRL_R_mouth_pushPullU.translateY", 0.0],
    ["CTRL_R_mouth_lipsTowardsTeethD.translateY", 0.0],
    ["CTRL_C_teeth_fwdBackD.translateY", 0.0],
    ["CTRL_C_mouth_lipShiftU.translateY", 0.0],
    ["CTRL_C_teethU.translateX", 0.0],
    ["CTRL_C_teethU.translateY", 0.0],
    ["CTRL_L_mouth_cornerSharpnessD.translateY", 0.0],
    ["CTRL_R_eye_faceScrunch.translateY", 0.0],
    ["CTRL_R_eyelashes_tweakerIn.translateY", 0.0],
    ["CTRL_L_mouth_thicknessD.translateY", 0.0],
    ["CTRL_R_mouth_pushPullD.translateY", 0.0],
    ["CTRL_R_mouth_cornerSharpnessU.translateY", 0.0],
    ["CTRL_L_mouth_corner.translateX", 0.0],
    ["CTRL_L_mouth_corner.translateY", 0.0],
    ["CTRL_L_mouth_lipsTowardsTeethD.translateY", 0.0],
    ["CTRL_C_teethD.translateX", 0.0],
    ["CTRL_C_teethD.translateY", 0.0],
    ["CTRL_L_eyelashes_tweakerIn.translateY", 0.0],
    ["CTRL_L_mouth_lipsRollU.translateY", 0.0],
    ["CTRL_R_eye_eyelidU.translateY", 0.0],
    ["CTRL_R_mouth_thicknessU.translateY", 0.0],
    ["CTRL_R_mouth_lipsTowardsTeethU.translateY", 0.0],
    ["CTRL_L_eye_faceScrunch.translateY", 0.0],
    ["CTRL_R_mouth_cornerSharpnessD.translateY", 0.0],
    ["CTRL_R_nose_nasolabialDeepen.translateY", 0.0],
    ["CTRL_L_mouth_thicknessU.translateY", 0.0],
    ["CTRL_R_eye_eyelidD.translateY", 0.0],
    ["CTRL_L_mouth_pushPullD.translateY", 0.0],
    ["CTRL_L_eye_eyelidD.translateY", 0.0],
    ["CTRL_R_mouth_thicknessD.translateY", 0.0],
    ["CTRL_R_eyelashes_tweakerOut.translateY", 0.0],
    ["CTRL_L_mouth_cornerSharpnessU.translateY", 0.0],
    ["CTRL_L_eye_eyelidU.translateY", 0.0],
    ["CTRL_R_mouth_lipsRollU.translateY", 0.0],
    ["CTRL_L_nose_nasolabialDeepen.translateY", 0.0],
    ["CTRL_C_mouth_lipShiftD.translateY", 0.0],
    ["CTRL_L_mouth_lipsTowardsTeethU.translateY", 0.0],
    ["CTRL_C_teeth_fwdBackU.translateY", 0.0],
    ["CTRL_L_eyelashes_tweakerOut.translateY", 0.0],
    ["CTRL_R_mouth_lipsRollD.translateY", 0.0],
    ["CTRL_R_mouth_corner.translateX", 0.0],
    ["CTRL_R_mouth_corner.translateY", 0.0],
    ["CTRL_L_mouth_lipsRollD.translateY", 0.0],
    ["CTRL_L_mouth_pushPullU.translateY", 0.0],
    ["CTRL_faceGUIfollowHead.translateY", 0.0],
    ["CTRL_eyesAimFollowHead.translateY", 0.0],
]
"""list: Дефолтовые значения атрибутов контролов"""


BLENDS={
    "EyeBlinkLeft":[("CTRL_L_eye_blink", "ty", 1)],
    "EyeBlinkRight":[("CTRL_R_eye_blink", "ty", 1)],
    "EyeLookDownLeft":[("CTRL_L_eye", "ty", -1)],
    "EyeLookDownRight":[("CTRL_R_eye", "ty", -1)],
    "EyeLookInLeft":[("CTRL_L_eye", "tx", -1)],
    "EyeLookInRight":[("CTRL_R_eye", "tx", 1)],
    "EyeLookOutLeft":[("CTRL_L_eye", "tx", 1)],
    "EyeLookOutRight":[("CTRL_R_eye", "tx", -1)],
    "EyeLookUpLeft":[("CTRL_L_eye", "ty", 1)],
    "EyeLookUpRight":[("CTRL_R_eye", "ty", 1)],
    "EyeSquintLeft":[("CTRL_L_eye_squintInner", "ty", 1)],
    "EyeSquintRight":[("CTRL_R_eye_squintInner", "ty", 1)],
    "EyeWideLeft":[("CTRL_L_eye_blink", "ty", -1)],
    "EyeWideRight":[("CTRL_R_eye_blink", "ty", -1)],
    ###
    "EyeCheekSquintLeft_u":[
        ("CTRL_L_eye_squintInner", "ty", 1),
        ("CTRL_L_eye_cheekRaise", "ty", 1)
        ],
    "EyeCheekSquintRight_u":[
        ("CTRL_R_eye_squintInner", "ty", 1),
        ("CTRL_R_eye_cheekRaise", "ty", 1)
        ],
    ###
    "EyeBlinkCSquintLeft_u":[
        ("CTRL_L_eye_blink", "ty", 1),
        ("CTRL_L_eye_cheekRaise", "ty", 1)
        ],
    "EyeBlinkCSquintRight_u":[
        ("CTRL_R_eye_blink", "ty", 1),
        ("CTRL_R_eye_cheekRaise", "ty", 1)
        ],
    ###
    "EyeBlinkESquintLeft_u":[
        ("CTRL_L_eye_blink", "ty", 1),
        ("CTRL_L_eye_squintInner", "ty", 1)
        ],
    "EyeBlinkESquintRight_u":[
        ("CTRL_R_eye_blink", "ty", 1),
        ("CTRL_R_eye_squintInner", "ty", 1)
        ],
    ###
    "EyeBlinkFSquintLeft_u":[
        ("CTRL_L_eye_blink", "ty", 1),
        ("CTRL_L_eye_squintInner", "ty", 1),
        ("CTRL_L_eye_cheekRaise", "ty", 1)
        ],
    "EyeBlinkFSquintRight_u":[
        ("CTRL_R_eye_blink", "ty", 1),
        ("CTRL_R_eye_squintInner", "ty", 1),
        ("CTRL_R_eye_cheekRaise", "ty", 1)
        ],
    "JawForward":[("CTRL_C_jaw_fwdBack", "ty", -1)],
    "JawBack":[("CTRL_C_jaw_fwdBack", "ty", 1)],
    "JawLeft":[("CTRL_C_jaw", "tx", -1)],
    "JawRight":[("CTRL_C_jaw", "tx", 1)],
    "JawOpen":[("CTRL_C_jaw", "ty", 1)],
    "JawOpenPucker_u":[
        # open
        ("CTRL_C_jaw", "ty", 1),
        # pucker
        ("CTRL_R_mouth_purseU", "ty", 1),
        ("CTRL_L_mouth_purseU", "ty", 1),
        ("CTRL_R_mouth_purseD", "ty", 1),
        ("CTRL_L_mouth_purseD", "ty", 1),
    ],
    "JawOpenFunnel_u":[
        # open
        ("CTRL_C_jaw", "ty", 1),
        # funnel
        ("CTRL_R_mouth_funnelU", "ty", 1),
        ("CTRL_L_mouth_funnelU", "ty", 1),
        ("CTRL_R_mouth_funnelD", "ty", 1),
        ("CTRL_L_mouth_funnelD", "ty", 1),
    ],
    "JawOpenPuckerFunnel_u":[
        # open
        ("CTRL_C_jaw", "ty", 1),
        # funnel
        ("CTRL_R_mouth_funnelU", "ty", 1),
        ("CTRL_L_mouth_funnelU", "ty", 1),
        ("CTRL_R_mouth_funnelD", "ty", 1),
        ("CTRL_L_mouth_funnelD", "ty", 1),
        # pucker
        ("CTRL_R_mouth_purseU", "ty", 1),
        ("CTRL_L_mouth_purseU", "ty", 1),
        ("CTRL_R_mouth_purseD", "ty", 1),
        ("CTRL_L_mouth_purseD", "ty", 1),
    ],
    "JawOpenMouthCloseFunnel_u":[
        # open
        ("CTRL_C_jaw", "ty", 1),
        # clothe
        ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
        ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
        ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
        ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
        # funnel
        ("CTRL_R_mouth_funnelU", "ty", 1),
        ("CTRL_L_mouth_funnelU", "ty", 1),
        ("CTRL_R_mouth_funnelD", "ty", 1),
        ("CTRL_L_mouth_funnelD", "ty", 1),
    ],
    "JawOpenMouthClosePucker_u":[
        # open
        ("CTRL_C_jaw", "ty", 1),
        # clothe
        ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
        ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
        ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
        ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
        # pucker
        ("CTRL_R_mouth_purseU", "ty", 1),
        ("CTRL_L_mouth_purseU", "ty", 1),
        ("CTRL_R_mouth_purseD", "ty", 1),
        ("CTRL_L_mouth_purseD", "ty", 1),
    ],
    "JawOpenMouthClosePuckerFunnel_u":[
        # open
        ("CTRL_C_jaw", "ty", 1),
        # clothe
        ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
        ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
        ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
        ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
        # pucker
        ("CTRL_R_mouth_purseU", "ty", 1),
        ("CTRL_L_mouth_purseU", "ty", 1),
        ("CTRL_R_mouth_purseD", "ty", 1),
        ("CTRL_L_mouth_purseD", "ty", 1),
        # funnel
        ("CTRL_R_mouth_funnelU", "ty", 1),
        ("CTRL_L_mouth_funnelU", "ty", 1),
        ("CTRL_R_mouth_funnelD", "ty", 1),
        ("CTRL_L_mouth_funnelD", "ty", 1),
    ],
    "MouthClose_u":[("CTRL_C_jaw", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
                ],
    "MouthCloseJawOpenLeft_u":[("CTRL_C_jaw", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_C_jaw", "tx", -1),
                ],
    "MouthCloseJawOpenRight_u":[("CTRL_C_jaw", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_C_jaw", "tx", 1),
                ],
    "MouthCloseJawLeft_u":[
                   ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_C_jaw", "tx", -1),
                ],
    "MouthCloseJawRight_u":[
                   ("CTRL_R_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherU", "ty", 1),
                   ("CTRL_R_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_L_mouth_lipsTogetherD", "ty", 1),
                   ("CTRL_C_jaw", "tx", 1),
                ],
    "MouthFunnel":[("CTRL_R_mouth_funnelU", "ty", 1),
                   ("CTRL_L_mouth_funnelU", "ty", 1),
                   ("CTRL_R_mouth_funnelD", "ty", 1),
                   ("CTRL_L_mouth_funnelD", "ty", 1),
                ],
    "MouthPucker":[("CTRL_R_mouth_purseU", "ty", 1),
                   ("CTRL_L_mouth_purseU", "ty", 1),
                   ("CTRL_R_mouth_purseD", "ty", 1),
                   ("CTRL_L_mouth_purseD", "ty", 1),
                ],
    "MouthLeftPucker_u":[
        ("CTRL_R_mouth_purseU", "ty", 1),
        ("CTRL_L_mouth_purseU", "ty", 1),
        ("CTRL_R_mouth_purseD", "ty", 1),
        ("CTRL_L_mouth_purseD", "ty", 1),
        ("CTRL_C_mouth", "tx", 1)
    ],
    "MouthRightPucker_u":[
        ("CTRL_R_mouth_purseU", "ty", 1),
        ("CTRL_L_mouth_purseU", "ty", 1),
        ("CTRL_R_mouth_purseD", "ty", 1),
        ("CTRL_L_mouth_purseD", "ty", 1),
        ("CTRL_C_mouth", "tx", -1)
    ],
    "LipsTowards":[("CTRL_L_mouth_towardsU", "ty", 1),
                   ("CTRL_L_mouth_towardsD", "ty", 1),
                   ("CTRL_R_mouth_towardsD", "ty", 1),
                   ("CTRL_R_mouth_towardsU", "ty", 1),
                ],
    "MouthPuckerFunnel_u":[
                   ("CTRL_R_mouth_funnelU", "ty", 1),
                   ("CTRL_L_mouth_funnelU", "ty", 1),
                   ("CTRL_R_mouth_funnelD", "ty", 1),
                   ("CTRL_L_mouth_funnelD", "ty", 1),
                   ("CTRL_R_mouth_purseU", "ty", 1),
                   ("CTRL_L_mouth_purseU", "ty", 1),
                   ("CTRL_R_mouth_purseD", "ty", 1),
                   ("CTRL_L_mouth_purseD", "ty", 1),
                ],
    "MouthPuckerTowards_u":[
                   ("CTRL_L_mouth_towardsU", "ty", 1),
                   ("CTRL_L_mouth_towardsD", "ty", 1),
                   ("CTRL_R_mouth_towardsD", "ty", 1),
                   ("CTRL_R_mouth_towardsU", "ty", 1),
                   ("CTRL_R_mouth_purseU", "ty", 1),
                   ("CTRL_L_mouth_purseU", "ty", 1),
                   ("CTRL_R_mouth_purseD", "ty", 1),
                   ("CTRL_L_mouth_purseD", "ty", 1),
                ],
    "MouthLeft":[("CTRL_C_mouth", "tx", 1)],
    "MouthRight":[("CTRL_C_mouth", "tx", -1)],
    "MouthUp":[("CTRL_C_mouth", "ty", 1)],
    "MouthDown":[("CTRL_C_mouth", "ty", -1)],
    "MouthSmile":[
        ("CTRL_L_mouth_cornerPull", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
        ],
    "MouthSmileLeft":[("CTRL_L_mouth_cornerPull", "ty", 1)],
    "MouthSmileRight":[("CTRL_R_mouth_cornerPull", "ty", 1)],
    "MouthSharpPullLeft":[("CTRL_L_mouth_sharpCornerPull", "ty", 1)],
    "MouthSharpPullRight":[("CTRL_R_mouth_sharpCornerPull", "ty", 1)],
    "MouthFrownLeft":[("CTRL_L_mouth_cornerDepress", "ty", 1)],
    "MouthFrownRight":[("CTRL_R_mouth_cornerDepress", "ty", 1)],
    "MouthDimple":[
        ("CTRL_L_mouth_dimple", "ty", 1),
        ("CTRL_R_mouth_dimple", "ty", 1)
        ],
    "MouthDimpleLeft":[("CTRL_L_mouth_dimple", "ty", 1)],
    "MouthDimpleRight":[("CTRL_R_mouth_dimple", "ty", 1)],
    # "MouthDimpleLeft":[("CTRL_L_mouth_stretch", "ty", 1)],
    # "MouthDimpleRight":[("CTRL_R_mouth_stretch", "ty", 1)],
    "MouthStretch":[
        ("CTRL_L_mouth_stretch", "ty", 1),
        ("CTRL_R_mouth_stretch", "ty", 1)
        ],
    "MouthStretchLeft":[("CTRL_L_mouth_stretch", "ty", 1)],
    "MouthStretchRight":[("CTRL_R_mouth_stretch", "ty", 1)],
    # "MouthStretchLeft":[("CTRL_L_mouth_dimple", "ty", 1)],
    # "MouthStretchRight":[("CTRL_R_mouth_dimple", "ty", 1)],
    "MouthRollUpperLeft":[("CTRL_L_mouth_lipsRollU", "ty", 1)],
    "MouthRollUpperRight":[("CTRL_R_mouth_lipsRollU", "ty", 1)],
    "MouthRollUpperLeftJawOpen_u":[
        ("CTRL_L_mouth_lipsRollU", "ty", 1),
        ("CTRL_C_jaw", "ty", 1)
    ],
    "MouthRollUpperRightJawOpen_u":[
        ("CTRL_R_mouth_lipsRollU", "ty", 1),
        ("CTRL_C_jaw", "ty", 1)
    ],
    "MouthShrugUpperLeft":[("CTRL_L_mouth_lipsRollU", "ty", -1)],
    "MouthShrugUpperRight":[("CTRL_R_mouth_lipsRollU", "ty", -1)],
    "MouthRollLowerLeft":[("CTRL_L_mouth_lipsRollD", "ty", 1)],
    "MouthRollLowerRight":[("CTRL_R_mouth_lipsRollD", "ty", 1)],
    "MouthRollLowerLeftJawOpen_u":[
        ("CTRL_L_mouth_lipsRollD", "ty", 1),
        ("CTRL_C_jaw", "ty", 1)
    ],
    "MouthRollLowerRightJawOpen_u":[
        ("CTRL_R_mouth_lipsRollD", "ty", 1),
        ("CTRL_C_jaw", "ty", 1)
    ],
    "MouthRollJawOpen_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_lipsRollD", "ty", 1),
        ("CTRL_L_mouth_lipsRollD", "ty", 1),
        ("CTRL_R_mouth_lipsRollU", "ty", 1),
        ("CTRL_L_mouth_lipsRollU", "ty", 1),
    ],
    "MouthShrugLowerLeft":[("CTRL_L_mouth_lipsRollD", "ty", -1)],
    "MouthShrugLowerRight":[("CTRL_R_mouth_lipsRollD", "ty", -1)],
    "MouthPressLeft":[("CTRL_L_mouth_pressU", "ty", 1), ("CTRL_L_mouth_pressD", "ty", 1)],
    "MouthPressRight":[("CTRL_R_mouth_pressU", "ty", 1), ("CTRL_R_mouth_pressD", "ty", 1)],
    "LipPressLeft":[("CTRL_L_mouth_lipsPressU", "ty", 1)],
    "LipPressRight":[("CTRL_R_mouth_lipsPressU", "ty", 1)],
    "LipCornerSharpenULeft":[("CTRL_L_mouth_cornerSharpnessU", "ty", 1)],
    "LipCornerSharpenURight":[("CTRL_R_mouth_cornerSharpnessU", "ty", 1)],
    "LipCornerSharpenDLeft":[("CTRL_L_mouth_cornerSharpnessD", "ty", 1)],
    "LipCornerSharpenDRight":[("CTRL_R_mouth_cornerSharpnessD", "ty", 1)],
    "MouthLowerDown":[
        ("CTRL_L_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_R_mouth_lowerLipDepress", "ty", 1)
        ],
    "MouthLowerDownLeft":[("CTRL_L_mouth_lowerLipDepress", "ty", 1)],
    "MouthLowerDownRight":[("CTRL_R_mouth_lowerLipDepress", "ty", 1)],
    "MouthUpperUp":[
        ("CTRL_L_mouth_upperLipRaise", "ty", 1),
        ("CTRL_R_mouth_upperLipRaise", "ty", 1),
        ],
    "MouthUpperUpLeft":[("CTRL_L_mouth_upperLipRaise", "ty", 1)],
    "MouthUpperUpRight":[("CTRL_R_mouth_upperLipRaise", "ty", 1)],
    "NasolabialLeft":[("CTRL_L_nose_nasolabialDeepen", "ty", 1)],
    "NasolabialRight":[("CTRL_R_nose_nasolabialDeepen", "ty", 1)],
    "ChinCompressLeft":[("CTRL_L_jaw_chinCompress", "ty", 1)],
    "ChinCompressRight":[("CTRL_R_jaw_chinCompress", "ty", 1)],
    "ChinRaiseLeft":[("CTRL_L_jaw_ChinRaiseU", "ty", 1), ("CTRL_L_jaw_ChinRaiseD", "ty", 1)],
    "ChinRaiseRight":[("CTRL_R_jaw_ChinRaiseU", "ty", 1), ("CTRL_R_jaw_ChinRaiseD", "ty", 1)],
    "BrowLateralLeft":[("CTRL_L_brow_lateral", "ty", 1)],
    "BrowLateralRight":[("CTRL_R_brow_lateral", "ty", 1)],
    # "BrowDownLeftInner":[("CTRL_L_brow_down", "ty", 1)],
    # "BrowDownRightInner":[("CTRL_R_brow_down", "ty", 1)],
    "BrowDownLeft":[("CTRL_L_brow_down", "ty", 1)],
    "BrowDownRight":[("CTRL_R_brow_down", "ty", 1)],
    "BrowUpLeft":[("CTRL_L_brow_raiseIn", "ty", 1), ("CTRL_L_brow_raiseOut", "ty", 1)],
    "BrowUpRight":[("CTRL_R_brow_raiseIn", "ty", 1), ("CTRL_R_brow_raiseOut", "ty", 1)],
    "BrowUpLeftInner":[("CTRL_L_brow_raiseIn", "ty", 1)],
    "BrowUpRightInner":[("CTRL_R_brow_raiseIn", "ty", 1)],
    "BrowUpLeftOuter":[("CTRL_L_brow_raiseOut", "ty", 1)],
    "BrowUpRightOuter":[("CTRL_R_brow_raiseOut", "ty", 1)],
    "CheekPuffLeft":[("CTRL_L_mouth_suckBlow", "ty", 1)],
    "CheekPuffRight":[("CTRL_R_mouth_suckBlow", "ty", 1)],
    "CheekPuff":[("CTRL_R_mouth_suckBlow", "ty", 1), ("CTRL_L_mouth_suckBlow", "ty", 1)],
    "CheekSuckLeft":[("CTRL_L_mouth_suckBlow", "ty", -1)],
    "CheekSuckRight":[("CTRL_R_mouth_suckBlow", "ty", -1)],
    "CheekSuck":[("CTRL_R_mouth_suckBlow", "ty", -1), ("CTRL_L_mouth_suckBlow", "ty", -1)],
    "CheekSquintLeft":[("CTRL_L_eye_cheekRaise", "ty", 1)],
    "CheekSquintRight":[("CTRL_R_eye_cheekRaise", "ty", 1)],
    "NoseSneerLeft":[("CTRL_L_nose", "ty", 1)],
    "NoseSneerRight":[("CTRL_R_nose", "ty", 1)],
    "NoseDepressLeft":[("CTRL_L_nose", "ty", -1)],
    "NoseDepressRight":[("CTRL_R_nose", "ty", -1)],
    "NostrilDilateLeft":[("CTRL_L_nose", "tx", 1)],
    "NostrilDilateRight":[("CTRL_R_nose", "tx", 1)],
    "NostrilCompressLeft":[("CTRL_L_nose", "tx", -1)],
    "NostrilCompressRight":[("CTRL_R_nose", "tx", -1)],
    # JawOpen
    "JawOpenSmileLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "JawOpenSmileRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    "JawOpenSharpPullLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1)
    ],
    "JawOpenSharpPullRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1)
    ],
    "JawOpenDimpleLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_dimple", "ty", 1)
    ],
    "JawOpenDimpleRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_dimple", "ty", 1)
    ],
    "JawOpenStretchLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_stretch", "ty", 1)
    ],
    "JawOpenStretchRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_stretch", "ty", 1)
    ],
    "JawOpenFrownLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_cornerDepress", "ty", 1)
    ],
    "JawOpenFrownRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_cornerDepress", "ty", 1)
    ],
    "JawOpenMouthLowerDownLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_lowerLipDepress", "ty", 1)
    ],
    "JawOpenMouthLowerDownRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_lowerLipDepress", "ty", 1)
    ],
    "JawOpenSmileMLowerDownLeft_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_L_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "JawOpenSmileMLowerDownRight_u":[
        ("CTRL_C_jaw", "ty", 1),
        ("CTRL_R_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    # Nasolabial
    "NasolabialSmileLeft_u":[
        ("CTRL_L_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "NasolabialSmileRight_u":[
        ("CTRL_R_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    "NasolabialSharpPullLeft_u":[
        ("CTRL_L_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1)
    ],
    "NasolabialSharpPullRight_u":[
        ("CTRL_R_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1)
    ],
    "NasolabialDimpleLeft_u":[
        ("CTRL_L_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_L_mouth_dimple", "ty", 1)
    ],
    "NasolabialDimpleRight_u":[
        ("CTRL_R_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_R_mouth_dimple", "ty", 1)
    ],
    "NasolabialStretchLeft_u":[
        ("CTRL_L_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_L_mouth_stretch", "ty", 1)
    ],
    "NasolabialStretchRight_u":[
        ("CTRL_R_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_R_mouth_stretch", "ty", 1)
    ],
    "NasolabialFrownLeft_u":[
        ("CTRL_L_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_L_mouth_cornerDepress", "ty", 1)
    ],
    "NasolabialFrownRight_u":[
        ("CTRL_R_nose_nasolabialDeepen", "ty", 1),
        ("CTRL_R_mouth_cornerDepress", "ty", 1)
    ],
    # MouthUpperUpSneer
    "MouthUpperUpSneerLeft_u":[
        ("CTRL_L_mouth_upperLipRaise", "ty", 1),
        ("CTRL_L_nose", "ty", 1)
    ],
    "MouthUpperUpSneerRight_u":[
        ("CTRL_R_mouth_upperLipRaise", "ty", 1),
        ("CTRL_R_nose", "ty", 1)
    ],
    # MouthUpperUpSmile
    "MouthUpperUpSmileLeft_u":[
        ("CTRL_L_mouth_upperLipRaise", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "MouthUpperUpSmileRight_u":[
        ("CTRL_R_mouth_upperLipRaise", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    # MouthUpperUpSharpPull
    "MouthUpperUpSharpPullLeft_u":[
        ("CTRL_L_mouth_upperLipRaise", "ty", 1),
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1)
    ],
    "MouthUpperUpSharpPullRight_u":[
        ("CTRL_R_mouth_upperLipRaise", "ty", 1),
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1)
    ],
    # MouthLowerDownSmile
    "MouthLowerDownSmileLeft_u":[
        ("CTRL_L_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "MouthLowerDownSmileRight_u":[
        ("CTRL_R_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    # MouthLowerDownSharpPull
    "MouthLowerDownSharpPullLeft_u":[
        ("CTRL_L_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1)
    ],
    "MouthLowerDownSharpPullRight_u":[
        ("CTRL_R_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1)
    ],
    # MouthLowerDownStretch
    "MouthLowerDownStretchLeft_u":[
        ("CTRL_L_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_L_mouth_stretch", "ty", 1)
    ],
    "MouthLowerDownStretchRight_u":[
        ("CTRL_R_mouth_lowerLipDepress", "ty", 1),
        ("CTRL_R_mouth_stretch", "ty", 1)
    ],
    # MouthStretchSmile
    "MouthStretchSmileLeft_u":[
        ("CTRL_L_mouth_stretch", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "MouthStretchSmileRight_u":[
        ("CTRL_R_mouth_stretch", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    # MouthStretchSharpPull
    "MouthStretchSharpPullLeft_u":[
        ("CTRL_L_mouth_stretch", "ty", 1),
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1)
    ],
    "MouthStretchSharpPullRight_u":[
        ("CTRL_R_mouth_stretch", "ty", 1),
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1)
    ],
    # SmileSneer
    "SmileSneerLeft_u":[
        ("CTRL_L_mouth_cornerPull", "ty", 1),
        ("CTRL_L_nose", "ty", 1)
    ],
    "SmileSneerRight_u":[
        ("CTRL_R_mouth_cornerPull", "ty", 1),
        ("CTRL_R_nose", "ty", 1)
    ],
    # SharpSneer
    "SharpPullSneerLeft_u":[
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1),
        ("CTRL_L_nose", "ty", 1)
    ],
    "SharpPullSneerRight_u":[
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1),
        ("CTRL_R_nose", "ty", 1)
    ],
    # CheekSquint Smile
    "CheekSquintSmileLeft_u":[
        ("CTRL_L_eye_cheekRaise", "ty", 1),
        ("CTRL_L_mouth_cornerPull", "ty", 1)
    ],
    "CheekSquintSmileRight_u":[
        ("CTRL_R_eye_cheekRaise", "ty", 1),
        ("CTRL_R_mouth_cornerPull", "ty", 1)
    ],
    # CheekSquint SharpPull
    "CheekSquintSharpPullLeft_u":[
        ("CTRL_L_eye_cheekRaise", "ty", 1),
        ("CTRL_L_mouth_sharpCornerPull", "ty", 1)
    ],
    "CheekSquintSharpPullRight_u":[
        ("CTRL_R_eye_cheekRaise", "ty", 1),
        ("CTRL_R_mouth_sharpCornerPull", "ty", 1)
    ],
    "EyeCompress_u":[
        ("CTRL_L_nose", "ty", 1),
        ("CTRL_L_eye_blink", "ty", 1),
        ("CTRL_L_eye_squintInner", "ty", 1),
        ("CTRL_L_eye_cheekRaise", "ty", 1),
        ("CTRL_L_brow_down", "ty", 1),
        # ("CTRL_L_brow_lateral", "ty", 1),
        ("CTRL_R_nose", "ty", 1),
        ("CTRL_R_eye_blink", "ty", 1),
        ("CTRL_R_eye_squintInner", "ty", 1),
        ("CTRL_R_eye_cheekRaise", "ty", 1),
        ("CTRL_R_brow_down", "ty", 1),
        # ("CTRL_R_brow_lateral", "ty", 1)
    ],
    "EyeCompressLeft_u":[
        ("CTRL_L_nose", "ty", 1),
        ("CTRL_L_eye_blink", "ty", 1),
        ("CTRL_L_eye_squintInner", "ty", 1),
        ("CTRL_L_eye_cheekRaise", "ty", 1),
        ("CTRL_L_brow_down", "ty", 1),
        # ("CTRL_L_brow_lateral", "ty", 1),
    ],
    "EyeCompressRight_u":[
        ("CTRL_R_nose", "ty", 1),
        ("CTRL_R_eye_blink", "ty", 1),
        ("CTRL_R_eye_squintInner", "ty", 1),
        ("CTRL_R_eye_cheekRaise", "ty", 1),
        ("CTRL_R_brow_down", "ty", 1),
        # ("CTRL_R_brow_lateral", "ty", 1)
    ],
}


CORRECTS_POST={
    # "EyeCompressLeft_fix":("EyeCompress_uLeft", "EyeBlinkLeft", "EyeSquintLeft", "CheekSquintLeft", "BrowDownLeftInner", "BrowDownLeftOuter", "BrowLateralLeft", "NoseSneerLeft"),
    "EyeCompressLeft_fix":(
        "EyeCompress_uLeft",
        # "EyeBlinkLeft",
        "EyeBlinkFSquintLeft_u",
        # "EyeSquintLeft",
        # "CheekSquintLeft",
        "BrowDownLeftInner",
        "BrowDownLeftOuter",
        "NoseSneerLeft",
    ),
    # "EyeCompressRight_fix":("EyeCompress_uRight", "EyeBlinkRight", "EyeSquintRight", "CheekSquintRight", "BrowDownRightInner", "BrowDownRightOuter", "BrowLateralRight", "NoseSneerRight"),
    "EyeCompressRight_fix":(
        "EyeCompress_uRight",
        # "EyeBlinkRight",
        "EyeBlinkFSquintRight_u",
        # "EyeSquintRight",
        # "CheekSquintRight",
        "BrowDownRightInner",
        "BrowDownRightOuter",
        "NoseSneerRight",
    ),
}


CORRECTS = {
    # JawOpen
    "JawOpenSmileLeft_fix":("JawOpenSmileLeft_u", "JawOpen", "MouthSmileLeft"),
    "JawOpenSmileRight_fix":("JawOpenSmileRight_u", "JawOpen", "MouthSmileRight"),
    "JawOpenSharpPullLeft_fix":("JawOpenSharpPullLeft_u", "JawOpen", "MouthSharpPullLeft"),
    "JawOpenSharpPullRight_fix":("JawOpenSharpPullRight_u", "JawOpen", "MouthSharpPullRight"),
    "JawOpenDimpleLeft_fix":("JawOpenDimpleLeft_u", "JawOpen", "MouthDimpleLeft"),
    "JawOpenDimpleRight_fix":("JawOpenDimpleRight_u", "JawOpen", "MouthDimpleRight"),
    "JawOpenStretchLeft_fix":("JawOpenStretchLeft_u", "JawOpen", "MouthStretchLeft"),
    "JawOpenStretchRight_fix":("JawOpenStretchRight_u", "JawOpen", "MouthStretchRight"),
    "JawOpenFrownLeft_fix":("JawOpenFrownLeft_u", "JawOpen", "MouthFrownLeft"),
    "JawOpenFrownRight_fix":("JawOpenFrownRight_u", "JawOpen", "MouthFrownRight"),
    "JawOpenMouthLowerDownLeft_fix":("JawOpenMouthLowerDownLeft_u", "JawOpen", "MouthLowerDownLeft"),
    "JawOpenMouthLowerDownRight_fix":("JawOpenMouthLowerDownRight_u", "JawOpen", "MouthLowerDownRight"),
    # Nasolabial
    "NasolabialSmileLeft_fix":("NasolabialSmileLeft_u", "NasolabialLeft", "MouthSmileLeft"),
    "NasolabialSmileRight_fix":("NasolabialSmileRight_u", "NasolabialRight", "MouthSmileRight"),
    "NasolabialSharpPullLeft_fix":("NasolabialSharpPullLeft_u", "NasolabialLeft", "MouthSharpPullLeft"),
    "NasolabialSharpPullRight_fix":("NasolabialSharpPullRight_u", "NasolabialRight", "MouthSharpPullRight"),
    "NasolabialDimpleLeft_fix":("NasolabialDimpleLeft_u", "NasolabialLeft", "MouthDimpleLeft"),
    "NasolabialDimpleRight_fix":("NasolabialDimpleRight_u", "NasolabialRight", "MouthDimpleRight"),
    "NasolabialStretchLeft_fix":("NasolabialStretchLeft_u", "NasolabialLeft", "MouthStretchLeft"),
    "NasolabialStretchRight_fix":("NasolabialStretchRight_u", "NasolabialRight", "MouthStretchRight"),
    "NasolabialFrownLeft_fix":("NasolabialFrownLeft_u", "NasolabialLeft", "MouthFrownLeft"),
    "NasolabialFrownRight_fix":("NasolabialFrownRight_u", "NasolabialRight", "MouthFrownRight"),
    # MouthUpperUpSneer
    "MouthUpperUpSneerLeft_fix":("MouthUpperUpSneerLeft_u", "MouthUpperUpLeft", "NoseSneerLeft"),
    "MouthUpperUpSneerRight_fix":("MouthUpperUpSneerRight_u", "MouthUpperUpRight", "NoseSneerRight"),
    # MouthUpperUpSmile
    "MouthUpperUpSmileLeft_fix":("MouthUpperUpSmileLeft_u", "MouthUpperUpLeft", "MouthSmileLeft"),
    "MouthUpperUpSmileRight_fix":("MouthUpperUpSmileRight_u", "MouthUpperUpRight", "MouthSmileRight"),
    # MouthUpperUpSharpPull
    "MouthUpperUpSharpPullLeft_fix":("MouthUpperUpSharpPullLeft_u", "MouthUpperUpLeft", "MouthSharpPullLeft"),
    "MouthUpperUpSharpPullRight_fix":("MouthUpperUpSharpPullRight_u", "MouthUpperUpRight", "MouthSharpPullRight"),
    # MouthLowerDownSmile
    "MouthLowerDownSmileLeft_fix":("MouthLowerDownSmileLeft_u", "MouthLowerDownLeft", "MouthSmileLeft"),
    "MouthLowerDownSmileRight_fix":("MouthLowerDownSmileRight_u", "MouthLowerDownRight", "MouthSmileRight"),
    # MouthLowerDownSharpPull
    "MouthLowerDownSharpPullLeft_fix":("MouthLowerDownSharpPullLeft_u", "MouthLowerDownLeft", "MouthSharpPullLeft"),
    "MouthLowerDownSharpPullRight_fix":("MouthLowerDownSharpPullRight_u", "MouthLowerDownRight", "MouthSharpPullRight"),
    # MouthStretchSmile
    "MouthStretchSmileLeft_fix":("MouthStretchSmileLeft_u", "MouthStretchLeft", "MouthSmileLeft"),
    "MouthStretchSmileRight_fix":("MouthStretchSmileRight_u", "MouthStretchRight", "MouthSmileRight"),
    # MouthStretchSharpPull
    "MouthStretchSharpPullLeft_fix":("MouthStretchSharpPullLeft_u", "MouthStretchLeft", "MouthSharpPullLeft"),
    "MouthStretchSharpPullRight_fix":("MouthStretchSharpPullRight_u", "MouthStretchRight", "MouthSharpPullRight"),
    # MouthLowerDownStretch
    "MouthLowerDownStretchLeft_fix":("MouthLowerDownStretchLeft_u", "MouthStretchLeft", "MouthLowerDownLeft"),
    "MouthLowerDownStretchRight_fix":("MouthLowerDownStretchRight_u", "MouthStretchRight", "MouthLowerDownRight"),
    #
    "MouthClose":("MouthClose_u", "JawOpen"),
    "MouthCloseJawOpenLeft_fix":("MouthCloseJawOpenLeft_u", "JawOpen", "JawLeft"),
    "MouthCloseJawOpenRight_fix":("MouthCloseJawOpenRight_u", "JawOpen", "JawRight"),
    "MouthCloseJawLeft_fix":("MouthCloseJawLeft_u", "JawLeft"),
    "MouthCloseJawRight_fix":("MouthCloseJawRight_u", "JawRight"),
    # SmileSneer
    "SmileSneerLeft_fix":("SmileSneerLeft_u", "MouthSmileLeft", "NoseSneerLeft"),
    "SmileSneerRight_fix":("SmileSneerRight_u", "MouthSmileRight", "NoseSneerRight"),
    # SharpSneer
    "SharpPullSneerLeft_fix":("SharpPullSneerLeft_u", "MouthSharpPullLeft", "NoseSneerLeft"),
    "SharpPullSneerRight_fix":("SharpPullSneerRight_u", "MouthSharpPullRight", "NoseSneerRight"),
    #
    "MouthPuckerFunnel_fix":("MouthPuckerFunnel_u", "MouthFunnel", "MouthPucker"),
    "MouthPuckerTowards_fix":("MouthPuckerTowards_u", "LipsTowards", "MouthPucker"),
    # cheek eye squints blinks
    # "EyeCheekSquintLeft_fix":("EyeCheekSquintLeft_u", "EyeSquintLeft", "CheekSquintLeft"),
    # "EyeCheekSquintRight_fix":("EyeCheekSquintRight_u", "EyeSquintRight", "CheekSquintRight"),
    #
    "CheekSquintSmileLeft_fix":("CheekSquintSmileLeft_u", "MouthSmileLeft", "CheekSquintLeft"),
    "CheekSquintSmileRight_fix":("CheekSquintSmileRight_u", "MouthSmileRight", "CheekSquintRight"),
    "CheekSquintSharpPullLeft_fix":("CheekSquintSharpPullLeft_u", "MouthSharpPullLeft", "CheekSquintLeft"),
    "CheekSquintSharpPullRight_fix":("CheekSquintSharpPullRight_u", "MouthSharpPullRight", "CheekSquintRight"),
}
"""dict: key: str - имя создаваемого корректа, value: tuple - [0] имя перерабатываемого комплексного бленда _u, [1], [2], ... имена простых составляющих блендов. """


def _get_meta_controls():
    """
    Возвращает список всех контролов метахумана, которые использованы при генерации блендов, исползует :attr:`BLENDS`
    """
    controls = set()
    for v in BLENDS.values():
        for c in v:
            controls.add(c[0])
    return list(controls)


def print_controls(*args):
    """
    Печатает в терминале все контролы используемые при генерации блендов.
    """
    print('-'*100)
    for c in sorted(_get_meta_controls()):
        print(c)


def make_fix(n, fix, blends):
    """
    создание корректов

    Parameters
    ----------
    n : str
        нейтральная сетка
    fix : str
        имя создаваемого корректа
    blends : list
        список исходных блендов для которых делается фикс, нулевой элемент - объединённая корректная форма _u.
    """
    if not mc.objExists(GROUP):
        mc.group(name=GROUP, empty=True)

    fix = mc.duplicate(n, n=fix)[0]

    # --
    mc.select(blends[0], r=True)
    for m in blends[1:]:
        mc.select(m, add=True)
    mc.select(fix, add=True)
    # -- bs
    bs = pm.blendShape()[0]
    eval('bs.%s.set(%s)' % (blends[0], 1))
    for m in blends[1:]:
        eval('bs.%s.set(%s)' % (m, -1))

    mc.delete(fix, ch=True)
    mc.parent(fix, GROUP)

    mc.select(blends[0], r=True)
    mc.delete()


def make_fix_by_selected_group(neutral, fix, blends):
    """
    Создание корректов из оригинальных сеток внутри выделенной группы

    Parameters
    ----------
    neutral : str
        нейтральная сетка
    fix : str
        имя создаваемого корректа
    blends : list
        список исходных блендов для которых делается фикс, нулевой элемент - объединённая корректная форма _u.

    Returns
    --------
    str
        имя созданного корректа
    """
    group_objects = dict()
    grp = mc.ls(sl=True)[0]
    for n in mc.listRelatives(c=True, type='transform', shapes=False, fullPath=True):
        group_objects[n.split("|")[-1:][0]] = n

    fix = mc.duplicate(neutral, n=fix)[0]

    # --
    for bn in blends:
        if not bn in group_objects:
            return "%s: NO FOUND" % bn
    mc.select(group_objects[blends[0]], r=True)
    for m in blends[1:]:
        mc.select(group_objects[m], add=True)
    mc.select(fix, add=True)

    # -- bs
    bs = pm.blendShape()[0]
    eval('bs.%s.set(%s)' % (blends[0], 1))
    for m in blends[1:]:
        eval('bs.%s.set(%s)' % (m, -1))

    mc.delete(fix, ch=True)

    # -- clean
    mc.select(group_objects[blends[0]], r=True)
    mc.delete()

    try:
        mc.parent(fix, grp)
    except:
        pass

    mc.select(grp, r=True)
    return fix


def make_all_bs(mesh_field, fix=True):
    """
    Авто создание всех блендов. И базовые и корректы  вычитанием.

    Parameters
    -----------
    mesh_field: str
        имя поля ввода нейтральной сетки
    fix: bool
        генерить или нет корректы
    """
    ctrls_to_zero()
    for bs, item in BLENDS.items():
        make_single_bs(bs, item, mesh_field)

    if fix:
        for k, v in CORRECTS.items():
            make_fix(mc.textField(mesh_field, q=True, text=True), k, v)


def make_all_corrects_bs(mesh_field, datas):
    """
    Создание корректов на блендах

    Parameters
    ----------
    mesh_field : cmds.textField
        ткекстовое поле содержащее имя нейтральной сетки
    datas: list
        список словарей по созданию корректов
    """
    ctrls_to_zero()
    for data in datas:
        for k, v in data.items():
            make_fix(mc.textField(mesh_field, q=True, text=True), k, v)


def make_all_fix_by_selected_group(mesh_field, datas):
    """
    Создание корректов на блендах, из сеток выделенной группы.

    Parameters
    ----------
    mesh_field : cmds.textField
        ткекстовое поле содержащее имя нейтральной сетки
    datas: list
        список словарей по созданию корректов
    """

    print('-'*20 + ' convert_all_corrects_to_original ' + '-'*20)

    for data in datas:
        for k,v in data.items():
            print(make_fix_by_selected_group(mc.textField(mesh_field, q=True, text=True), k, v))


def _convert_correct_to_original(neutral, correct_name, orig_name, blends):
    """
    Превращает коррект в оригинальный бленд. В выбранной группе.

    Parameters
    -----------
    neutral: str
        имя нейтральной сетки головы
    correct_name : str
        имя корректа который необходимо превратить в оригинальный бленд.
    orig_name : str
        имя оригинального блендшейпа в который будет переименован коррект после преобразования.
    blends: list
        список имён блендов, которые вычитались из оригинального для создания корректа.

    Returns
    --------
    str
        имя созданного бленда
    """
    group_objects = dict()
    grp = mc.ls(sl=True)[0]

    for n in mc.listRelatives(c=True, type='transform', shapes=False, fullPath=True):
        group_objects[n.split("|")[-1:][0]] = n
    # print('.'*50)
    # print(group_objects)

    if not correct_name in group_objects:
        r_str = "%s: no found" % correct_name
        return r_str

    orig = mc.duplicate(neutral, name=orig_name)[0]
    try:
        mc.parent(orig, grp)
    except:
        pass

    mc.select(group_objects[blends[0]], r=True)
    for m in blends[1:]:
        mc.select(group_objects[m], add=True)
    mc.select(correct_name, add=True)
    mc.select(orig, add=True)
    # -- bs
    bs = pm.blendShape()[0]
    keys = blends + (correct_name,)
    for m in keys:
        eval('bs.%s.set(%s)' % (m, 1))

    mc.delete(orig, ch=True)
    if orig_name in group_objects:
        mc.delete(group_objects[orig_name])

    mc.delete(group_objects[correct_name])

    mc.select(grp, r=True)
    return orig


def convert_all_corrects_to_original(mesh_field):
    """
    Превращение корректов в бленды финального вида. Из констант: :attr:`CORRECTS`, :attr:`CORRECTS_POST`
    """

    print('-'*20 + ' convert_all_corrects_to_original ' + '-'*20)

    for DATA in [CORRECTS, CORRECTS_POST]:
        for k,v in DATA.items():
            print(_convert_correct_to_original(mc.textField(mesh_field, q=True, text=True), k, v[0], v[1:]))


def convert_selected_corrects_to_original():
    """
    Превращение выделенных корректов в бленды финального вида. Используются константы: :attr:`CORRECTS`, :attr:`CORRECTS_POST`
    """


def make_single_bs(name, data, mesh_field):
    mesh = mc.textField(mesh_field, q=True, text=True)
    print(name)
    print(data)
    print(mesh)

    if not mc.objExists(GROUP):
        mc.group(name=GROUP, empty=True)

    for item in data:
        mc.setAttr('%s.%s' % (item[0], item[1]), item[2])
    copy = mc.duplicate(mesh, name=name)
    mc.parent(copy, GROUP)
    # -- to zero
    for item in data:
        mc.setAttr('%s.%s' % (item[0], item[1]), 0)
    # ctrls_to_zero()
    mc.select(d=True)


def select_to_field(mesh_field):
    """
    Добавление имени выбранного объекта в текстовое поле.
    """
    ob = mc.ls(sl=True)[0]
    mc.textField(mesh_field, e=True, text=ob)


def export_group_content_to_obj():
    """Экспортирует все сетки выбранной группы в формат ``obj`` каждую сетку отдельным файлом в указанную директорию. """
    dir = mc.fileDialog2(fm=3)
    if dir is None:
        return
    if not os.path.isdir(dir[0]):
        mc.confirmDialog(m="Please select the Directory!", icon="warning")
        return
    dir = dir[0]
    print(dir)

    root = mc.ls(sl=True, type="transform")
    if not root:
        mc.confirmDialog(m="Group is not selected!", icon="warning")
        return
    root = root[0]

    for ob in mc.listRelatives(root, c=True, type='transform', shapes=False, fullPath=True):
        name = ob.split("|")[-1:][0]
        export_path = os.path.join(dir, "%s.obj" % name)
        print("%s - %s" % (ob, name))
        mc.select(ob, r=True)
        mc.file(export_path, pr=1, es=1, force=1, options="groups=0;ptgroups=1;materials=0;smoothing=1;normals=1",type="OBJexport")


def run():
    """Gui"""
    try:
        # window
        window = pm.window(t='From Meta Human Blends')

        menuBar = pm.menuBarLayout()
        pm.menu(l="Print Controls")
        pm.menuItem(l="Nino Scheme", c=print_controls)
        pm.menu(l='Help')
        pm.menuItem(l='Manual', c=help_tutorial)

        pm.columnLayout(adj=True)

        pm.rowLayout(nc=3, adj=True)
        pm.text('Mesh:', align='right')
        mesh_field = pm.textField()
        pm.button(l='<<', c=pm.Callback(select_to_field, mesh_field))
        pm.setParent('..')

        pm.text('Generate:', align='left')
        pm.rowLayout(nc=2, adj=True)
        pm.button(l='Make All _u', c=pm.Callback(make_all_bs, mesh_field, False))
        pm.button(l='Make All _fix', enable=False, c=pm.Callback(make_all_bs, mesh_field))
        pm.setParent('..')
        pm.button(l='Make All Post Corrects', enable=False, c=pm.Callback(make_all_corrects_bs, mesh_field, [CORRECTS_POST]))

        pm.text('Edit:', align='left')
        # pm.rowLayout(nc=2, adj=True)
        pm.button(l='Corrects to Originals', enable=False, c=pm.Callback(convert_all_corrects_to_original, mesh_field))
        pm.button(l='Originals to Corrects', enable=False, c=pm.Callback(make_all_fix_by_selected_group, mesh_field, [CORRECTS, CORRECTS_POST]))
        # pm.setParent('..')

        pm.text('Export:', align='left')
        pm.button(l='Export to OBJ', c=pm.Callback(export_group_content_to_obj))

        # pm.scrollLayout()
        # for bs in sorted(list(BLENDS.keys())):
        #     pm.rowLayout(nc=2, adj=True)
        #     pm.text(bs)
        #     btn = pm.button(l='Make', c=pm.Callback(make_single_bs, bs, BLENDS[bs], mesh_field))
        #     pm.setParent('..')
        # pm.setParent('..')

        pm.setParent('..')

        # mc.showWindow(window)
        window.show()
        pm.window(window, edit=True, wh=[300, 300])
    except:
        logger.error(traceback.format_exc())


def ctrls_to_zero():
    """Выставляет все контролы по дефолту"""
    for a, v in ZERO_POSITION:
        try:
            mc.setAttr(a, v)
        except:
            pass


if __name__ == '__main__':
    run()
